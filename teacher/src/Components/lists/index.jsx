import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core'

import { NavLink, Link, withRouter, Switch, Route } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    justifyContent: "space-between",
    border: `1px solid ${theme.colors.grey3}`,
    borderRadius: 20,
    padding: '24px 32px',
    margin: 8,
    cursor: "pointer",
  },
}))

export const ListItem = props => {
  const c = useStyles()
  return (
    <div className={c.container + " " + (props.className || "")} onClick={props.onClick} to={props.to}>
      {props.children}
    </div>
  )
}
