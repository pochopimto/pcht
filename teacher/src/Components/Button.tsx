import React from 'react'
import {
  Button as MUIButton,
  CircularProgress,
  makeStyles,
} from '@material-ui/core'

const useStyles = makeStyles({
  button: {
    position: 'relative',
  },
  progress: {
    position: 'absolute',
    top: 'calc(50% - 12px)',
    left: 'calc(50% - 12px)',
  },
})

type Props = {
  loading?: boolean
}

const Button = ({
  loading = false,
  disabled,
  className,
  children,
  ...props
}: Props & React.ComponentProps<typeof MUIButton>) => {
  const c = useStyles()
  const color = props.color === 'default' ? undefined : props.color

  return (
    <MUIButton
      className={`${className} ${c.button}`}
      disabled={disabled || loading}
      {...props}
    >
      {children}
      {loading && (
        <CircularProgress size={24} className={c.progress} color={color} />
      )}
    </MUIButton>
  )
}

export default Button
