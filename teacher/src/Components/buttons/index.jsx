import React from 'react'
import { Button as MUIButton, makeStyles } from '@material-ui/core'

import { Link as RouterLink, withRouter } from 'react-router-dom'

import l from 'Lib'
import * as hooks from 'Hooks'

import { Loader } from 'Components/loaders'
import * as icons from '../icons'
import Button from './Button'

export { default as Button } from './Button'

const useIconButtonStyles = makeStyles(theme => ({
  button: {
    position: 'relative',
    '&.enabled g': {
      fill: theme.colors.grey3,
      color: theme.colors.grey3,
    },
    '&.enabled:hover:not(:focus) g': {
      fill: theme.colors.grey2,
      color: theme.colors.grey2,
    },
    '&.enabled:focus g': {
      fill: theme.colors.grey4,
      color: theme.colors.grey4,
    },
    '&.disabled g': {
      fill: theme.colors.grey1,
      color: theme.colors.grey1,
    },
  },
  icon: {
    opacity: p => (p.isLoading ? 0 : 1),
    transition: `opacity ${theme.transitions.easing.easeInOut} ${theme.transitions.duration.shortest}`,
  },
  loadingCircle: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
}))

const gen_button = Icon =>
  function IconButton({ className, ...props }) {
    const c = useIconButtonStyles(props)

    return (
      <Button {...props} className={`${c.button} ${className}`}>
        <Icon size={props.size} className={c.icon} />
        {props.isLoading && <Loader className={c.loadingCircle} />}
      </Button>
    )
  }

export const ButtonDelete = gen_button(icons.IconDelete)
export const ButtonEdit = gen_button(icons.IconEdit)
export const ButtonClose = gen_button(icons.IconClose)
export const ButtonPlus = gen_button(icons.IconPlus)
export const ButtonHistory = gen_button(icons.IconHistory)
export const ButtonSemafor = gen_button(icons.IconSemafor)
export const ButtonSettings = gen_button(icons.IconSettings)
export const ButtonLogOut = gen_button(icons.IconLogOut)
export const ButtonBug = gen_button(icons.IconBug)

const useFancyLinkStartStyles = makeStyles(theme => ({
  link: {
    borderRadius: '100%',
    padding: '10px 7px 10px 11px',
    margin: '-10px 0px -10px 76px',
    backgroundColor: theme.colors.barbie,
    '& g': {
      fill: theme.colors.white,
    },
  },
}))

export const FancyLinkStart = props => {
  const c = useFancyLinkStartStyles()
  return (
    <RouterLink className={c.link} to={props.to}>
      <icons.IconStart size={props.size} />
    </RouterLink>
  )
}

export const useTextButtonBorderStyles = makeStyles(theme => ({
  button: {
    fontSize: p => [13, 16][p.level || 0],
    fontWeight: 600,
    padding: p => ['6px 14px', '12px 48px'][p.level || 0],
    minWidth: p => [82, 124][p.level || 0],

    transition: theme.transition('all'),
    border: '2px solid ' + theme.colors.grey3,
    borderRadius: 6,
    textAlign: 'center',
    display: 'inline-block',
    '&.enabled': {
      border: p =>
        '2px solid ' +
        (p.onClick ? l.getColor(theme, p.color || 0) : theme.colors.grey3),
      color: p => l.getColor(theme, p.color || 0),
    },
  },
}))

export const TextButtonBorder = props => {
  const c = useTextButtonBorderStyles(props)
  return (
    <Button
      className={c.button + ' ' + (props.className || '')}
      onClick={props.onClick}
      disabled={props.disabled}
    >
      {props.children}
    </Button>
  )
}

const useTextButtonFilledStyles = makeStyles(theme => ({
  button: {
    backgroundColor: theme.colors.grey3,
    '&.enabled': {
      backgroundColor: p => l.getColor(theme, p.color || 0),
    },
    '&:not(#id)': {
      color: theme.colors.white,
    },
  },
}))

export const TextButtonFilled = props => {
  const c = useTextButtonFilledStyles(props)
  return (
    <TextButtonBorder
      level={props.level}
      className={c.button + ' ' + (props.className || '')}
      onClick={props.onClick}
      color={props.color}
      disabled={props.disabled}
    >
      {props.children}
    </TextButtonBorder>
  )
}

const getTextColor = theme => p => {
  return {
    true: {
      select: theme.colors.white,
      scale: theme.colors.white,
      text: theme.colors.white,
    },
    false: {
      select: theme.colors.yellow,
      scale: theme.colors.purple,
      text: theme.colors.barbie,
    },
  }[p.active][p.type]
}

const useSaveAsTemplateButtonStyles = makeStyles(theme => ({
  button: {
    '& g': {
      fill: theme.colors.purple,
    },
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
}))

export const SaveAsTemplateButton = withRouter(props => {
  const c = useSaveAsTemplateButtonStyles()
  const t = hooks.usePolyglot()

  return (
    <MUIButton
      className={c.button + ' ' + (props.className || '')}
      onClick={async () => {
        props.onClick instanceof Function && (await props.onClick())
        props.history.push('/templates')
      }}
      disabled={props.disabled}
      color="primary"
      variant="outlined"
      endIcon={<icons.BookmarkIcon />}
    >
      <span>{l.firstUpper(t('editor.save_as_template'))}</span>
    </MUIButton>
  )
})

const useChipButtonStyles = makeStyles(theme => ({
  chip: {
    padding: '8px 16px',
    borderRadius: 6,
    display: 'flex',
    alignItems: 'center',
    width: 110,
    backgroundColor: theme.colors.grey2,
    transition: theme.transition('all'),
    '&.enabled': {
      backgroundColor: p => {
        return {
          true: {
            select: theme.colors.yellow,
            scale: theme.colors.purple,
            text: theme.colors.barbie,
          },
          false: {
            select: theme.colors.lightyellow,
            scale: theme.colors.lightpurple,
            text: theme.colors.lightbarbie,
          },
        }[p.active][p.type]
      },
      '& g': {
        fill: getTextColor(theme),
      },
      '& > span': {
        color: getTextColor(theme),
      },
    },
    '& > g': {
      color: theme.colors.grey3,
      transition: theme.transition('all'),
    },
    '& > span': {
      ...theme.h4,
      color: theme.colors.grey3,
      transition: theme.transition('all'),
      marginLeft: 7,
    },
  },
}))

export const ChipButton = props => {
  const c = useChipButtonStyles(props)
  const t = hooks.usePolyglot()
  return (
    <Button
      className={c.chip}
      onClick={props.onClick}
      disabled={props.disabled}
    >
      {props.type === 'select' && <icons.IconSelect />}
      {props.type === 'scale' && <icons.IconSlider />}
      {props.type === 'text' && <icons.IconText />}
      <span>
        {t(
          {
            select: 'select',
            scale: 'slider',
            text: 'text',
          }[props.type]
        ).toLowerCase()}
      </span>
    </Button>
  )
}

const useLinkStyles = makeStyles(theme => ({
  link: {
    color: p =>
      p.color === undefined ? 'inherit' : l.getColor(theme, p.color),
    fontWeight: 600,
  },
}))

export const Link = ({ className, ...props }) => {
  const c = useLinkStyles(props)

  return (
    <Button className={`${c.link} ${className || ''}`} {...props}>
      {props.children}
    </Button>
  )
}
