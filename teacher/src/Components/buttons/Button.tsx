import React from 'react'
import { makeStyles } from '@material-ui/core'
import { Link } from 'react-router-dom'

const useDivStyles = makeStyles({
  div: {
    outline: 'none',
  },
})

const Div = ({
  children,
  className,
  ...props
}: React.ComponentProps<'div'>) => {
  const c = useDivStyles({})
  return (
    <div tabIndex={-1} className={c.div + ' ' + (className || '')} {...props}>
      {children}
    </div>
  )
}

type AProps = {
  newTab?: boolean
}

const A = ({
  children,
  className,
  newTab = false,
  ...props
}: AProps & React.ComponentProps<'a'>) => (
  <a className={className} target={newTab ? '__blank' : undefined} {...props}>
    {children}
  </a>
)

const useStyles = makeStyles({
  button: {
    boxSizing: 'border-box',
    '&.enabled': {
      cursor: 'pointer',
    },
  },
})

type AButtonProps = {
  href: string
  to: never
  onClick: never
}

type LinkButtonProps = {
  href: never
  to: string
  onClick: never
}

type ButtonButtonProps = {
  href: never
  to: never
  onClick: string
}

type CommonProps = {
  disabled?: boolean
  className?: string
  children: React.ReactChild
}

type Props = CommonProps & (LinkButtonProps | AButtonProps | ButtonButtonProps)

export const Button = ({
  to,
  onClick,
  href,
  disabled = false,
  className = '',
  children,
  ...props
}: Props) => {
  const c = useStyles({})
  const hasLink = to !== undefined
  const hasClick = onClick !== undefined
  const hasHref = href !== undefined
  const enabled = (hasLink || hasClick || hasHref) && !disabled

  const Component = hasLink ? Link : hasClick ? Div : A

  const componentProps = {
    ...(enabled && to && { to }),
    ...(enabled && href && { href }),
    ...(enabled && onClick && { onClick }),
    ...props,
  }

  return (
    <Component
      className={`${c.button} ${enabled ? 'enabled' : 'disabled'} ${className}`}
      {...(componentProps as any)}
    >
      {children}
    </Component>
  )
}

export default Button
