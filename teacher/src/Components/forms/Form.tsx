import React, { useCallback } from 'react'
type Props = {
  onSubmit: (e?: any) => void
  children: JSX.Element
  className?: string
}
const Form = ({ onSubmit, children, className = '' }: Props) => {
  const handleSubmit = useCallback(
    (e: any) => {
      onSubmit(e)
      if (!e.defaultPrevented) {
        e.preventDefault()
      }
    },
    [onSubmit]
  )

  return (
    <form onSubmit={handleSubmit} className={className}>
      <input type="submit" style={{ display: 'none' }} />
      {children}
    </form>
  )
}

export default Form
