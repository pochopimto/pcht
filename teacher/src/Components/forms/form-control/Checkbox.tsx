import React, { useCallback, useState } from 'react'
import uuid from 'uuid'

import {
  FormControl,
  Checkbox as MuiCheckbox,
  FormHelperText,
  FormControlLabel,
  FormGroup,
  makeStyles,
} from '@material-ui/core'

const useStyles = makeStyles({
  errorText: {
    marginLeft: 33,
    marginTop: -8,
  },
})

type Props = {
  value: boolean
  onChange: (value: boolean) => void
  label?: string | JSX.Element
  error?: string[]
  className?: string
}

const Checkbox = ({
  value,
  onChange,
  label = '',
  error = [],
  className = '',
}: Props) => {
  const c = useStyles()
  const [id] = useState(() => uuid())

  const isError = error.length > 0

  const handleChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>, isChecked: boolean) =>
      onChange(isChecked),
    [onChange]
  )
  return (
    <FormControl error={isError} className={`${className}`}>
      <FormGroup>
        <FormControlLabel
          control={
            <MuiCheckbox
              id={`${id}`}
              checked={value}
              onChange={handleChange}
              aria-describedby={`${id}-text`}
            />
          }
          label={<>{label}</>}
        />
      </FormGroup>
      {isError && (
        <FormHelperText className={c.errorText} id={`${id}-text`}>
          {error[0]}
        </FormHelperText>
      )}
    </FormControl>
  )
}
export default Checkbox
