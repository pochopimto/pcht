import React from 'react'
import { makeStyles, TextField, InputAdornment } from '@material-ui/core'
import Fuse from 'fuse.js'

import * as hooks from 'Hooks'
import { buttons } from 'Components'
import l from 'Lib'

export const Input = ({ className = '', autocomplete, ...props }) => {
  return (
    <TextField
      className={className}
      disabled={Boolean(props.disabled)}
      error={Boolean(props.error)}
      onChange={e =>
        props.onChange instanceof Function && props.onChange(e.target.value)
      }
      placeholder={props.placeholder}
      value={props.value}
      type={props.type}
      variant={'outlined'}
      label={props.label}
      helperText={props.message}
      margin={'normal'}
      autoComplete={autocomplete}
      autoFocus={props.autoFocus}
      required={props.required}
      InputProps={{
        ...(props.onDelete && {
          endAdornment: (
            <InputAdornment position={'end'}>
              <buttons.ButtonDelete onClick={props.onDelete} />
            </InputAdornment>
          ),
        }),
        inputProps: {
          onFocus: props.onFocus,
          onBlur: props.onBlur,
        },
      }}
    />
  )
}

const useSelectOptionStyles = makeStyles(theme => ({
  option: {
    cursor: 'pointer',
    padding: '7px 8px',
    margin: '2px 0px',
    color: theme.colors.grey4,
    fontSize: 14,
    borderRadius: 6,
    backgroundColor: p => (p.active ? theme.colors.grey2 : 'initial'),
    transition: 'all .25s ease-in-out',
    '&:hover': {
      backgroundColor: p =>
        p.active ? theme.colors.grey2 : theme.colors.grey1,
      transition: 'all .1s ease-out',
    },
  },
}))

export const SelectOption = ({ label, onClick, active }) => {
  const c = useSelectOptionStyles({ active })
  return (
    <div className={c.option} onClick={onClick}>
      {label}
    </div>
  )
}

const useSelectStyles = makeStyles(theme => ({
  select: {
    position: 'relative',
  },
  input: {
    width: '100%',
    marginBottom: 0,
  },
  options: {
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: 100,
    background: theme.colors.white,
    maxHeight: 400,
    display: 'flex',
    flexDirection: 'column',
    border: `1px solid ${theme.colors.grey2}`,
    borderRadius: 6,
    backgroundColor: theme.colors.white,
    boxShadow: '0 6px 16px 0 rgba(221,221,221,0.5)',
    transition: 'all .25s ease-in-out',
    overflow: 'hidden',
    '&.hidden': {
      maxHeight: 0,
      opacity: 0,
    },
  },
  blank: {
    textAlign: 'center',
    color: theme.colors.grey3,
  },
  optionsWrapper: {
    padding: 8,
    display: 'flex',
    flexDirection: 'column',
    overflow: 'auto',
    flexShrink: 1,
  },
}))

export function Select({
  className = '',
  options = [],
  alwaysOptions = [],
  value = undefined,
  label = '',
  placeholder = '',
  onChange = () => {},
  error = false,
  message = '',
  autoFocus = false,
}) {
  const c = useSelectStyles()
  const t = hooks.usePolyglot()
  const [__text, setText] = React.useState('')
  const [focused, __setFocused] = React.useState(false)

  const setFocused = focus => {
    __setFocused(focus)
    if (!focus) {
      setText('')
    }
  }

  const text = focused
    ? __text
    : value
    ? [...options, ...alwaysOptions].find(o => o.id === value).label
    : ''

  const fuseOptions = {
    shouldSort: true,
    threshold: 0.6,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: ['label'],
  }

  const fuse = new Fuse(options, fuseOptions)
  const reducedOptions = text
    ? [...fuse.search(text), ...alwaysOptions]
    : [...alwaysOptions, ...options]

  return (
    <div
      className={c.select + ' ' + className}
      value={value}
      name={label}
      placeholder={placeholder}
    >
      <Input
        type={'text'}
        label={label}
        className={c.input}
        value={text}
        onChange={setText}
        autocomplete={'school'}
        onFocus={() => setFocused(true)}
        onBlur={() => setTimeout(() => setFocused(false), 200)}
        error={error}
        message={message}
        autoFocus
      />
      <div className={c.options + ' ' + (focused ? '' : 'hidden')}>
        <div className={c.optionsWrapper}>
          {reducedOptions.map(o => (
            <SelectOption
              key={o.id}
              label={o.label}
              active={o.id === value}
              onClick={() => {
                setText(o.label)
                onChange(o.id)
                setFocused(false)
              }}
            />
          ))}
          {reducedOptions.length === 0 && (
            <div className={c.blank}> {l.firstUpper(t('inputs.empty'))} </div>
          )}
        </div>
      </div>
    </div>
  )
}
