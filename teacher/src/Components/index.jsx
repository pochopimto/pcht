import * as __buttons from './buttons';
import * as __inputs from './inputs';
import * as __popups from './popups';
import * as __sliders from './sliders';
import * as __lists from './lists';
import * as __icons from './icons';

export const buttons = __buttons
export const inputs = __inputs
export const popups = __popups
export const sliders = __sliders
export const lists = __lists
export const icons = __icons
