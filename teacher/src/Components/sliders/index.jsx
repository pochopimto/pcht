import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core'

const useSliderStyles = makeStyles(theme => ({
  slider: {
    flex: '1 1 auto',
    height: 4,
    borderRadius: 3,
    backgroundColor: theme.colors.grey1,
    position: 'relative',
    "&:after": {
      position: 'absolute',
      content: '" "',
      top: 0,
      bottom: 0,
      left: 0,
      borderRadius: 3,
      minWidth: 4,
      width: p => (100*p.value)+"%",
      backgroundColor: p => [theme.colors.barbie, theme.colors.purple, theme.colors.yellow][p.color],
    },
  },
}))

export const Slider = props => {
  const c = useSliderStyles(props)

  return (
    <div className={c.slider}/>
  )
}

const useNumberedSliderStyles = makeStyles(theme => ({
  numberedSlider: {
    flex: '1 1 auto',
    display: 'flex',
    flexDirection: 'column',
  },
  bar: {
    height: 4,
    borderRadius: 3,
    backgroundColor: theme.colors.grey1,
    position: 'relative',
    "&:after":{
      position: 'absolute',
      content: '" "',
      top: 0,
      bottom: 0,
      left: 0,
      borderRadius: 3,
      width: p => (100*p.value)+"%",
      backgroundColor: theme.colors.purple,
    }
  },
  numbers: {
    ...theme.h5,
    color: theme.colors.grey4,
    marginTop: 8,
    display: 'flex',
    justifyContent: 'space-between',

  }
}))

export const NumberedSlider = props => {
  const c = useNumberedSliderStyles(props)

  return (
    <div className={c.numberedSlider + " " + (props.className || "")}>
      <Slider value={props.value} color={1}/>
      <div className={c.numbers}>
        { 
          [...Array(11).keys()].map(i => (
            <span key={i}>{i}</span>
          ))
        }
      </div>
    </div>
  )
}


