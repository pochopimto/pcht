import React, { useState, useRef } from 'react';
import { makeStyles } from '@material-ui/core'


import { NavLink, Link, withRouter, Switch, Route } from "react-router-dom";

import l from '../../Lib';


const usePopupStyles = makeStyles(theme => ({
  container: {
    zIndex: 10000,
  },
  popup: {
    zIndex: 10000,
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    backgroundColor: theme.colors.white,
    padding: '40px 48px',
    borderRadius: 6,
  },
  popupBackground: {
    zIndex: 1000,
    position: "fixed",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    background: theme.colors.bat,
    opacity: .9,
  },
}))
export const Popup = props => {
  const c = usePopupStyles()

  return (
    <div className={c.container}>
      <div className={c.popup + " " + (props.className || "")}>
        <div className={c.header}>
          { props.header }
        </div>
        {props.children}
      </div>
      <div className={c.popupBackground}/>
    </div>
  )
}

const useHeaderStyles = makeStyles(theme => ({
  header: {
    margin: '-16px -24ox 0 0',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    "& > *:not(:first-child)": {
      marginLeft: 17,
    }
  },
}))

export const Header = props => {
  const c = useHeaderStyles()

  return (
    <div className={c.header}>
      { props.children }
    </div>
  )
}
