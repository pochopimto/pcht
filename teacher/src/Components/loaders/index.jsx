import React from 'react'
import { CircularProgress } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles({
  loader: {
    height: '100%',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export const Loader = ({ className = '' }) => {
  const c = useStyles()

  return (
    <div className={`${c.loader} ${className}`}>
      <CircularProgress />
    </div>
  )
}
