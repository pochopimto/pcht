import React from 'react'
import { SvgIcon } from '@material-ui/core'

const SemaforIcon = (props: React.ComponentProps<typeof SvgIcon>) => {
  return (
    <SvgIcon {...props}>
      <rect
        width="10"
        height="20"
        x="7"
        y="2"
        strokeWidth="2"
        stroke="currentColor"
        rx="5"
        fill="none"
      />
      <circle cx="12" cy="12" r="2" />
      <circle cx="12" cy="7" r="2" />
      <circle cx="12" cy="17" r="2" />
    </SvgIcon>
  )
}

export default SemaforIcon
