import React from 'react'
import ReactSVG from 'react-svg'
import { makeStyles, Box } from '@material-ui/core'

import * as hooks from 'Hooks'

import IconLogoSrc from 'Assets/logo/pcht.svg'
import IconUserSrc from 'Assets/icons/icon-user32x32.svg'
import IconPlusSrc from 'Assets/icons/icon-plus32x32.svg'
import IconHistorySrc from 'Assets/icons/icon-history32x32.svg'
import IconSettingsSrc from 'Assets/icons/icon-settings32x32.svg'
import IconLogOutSrc from 'Assets/icons/icon-log-out32x32.svg'
import IconBugSrc from 'Assets/icons/icon-bug32x32.svg'
import IconDeleteSrc from 'Assets/icons/icon-trash32x32.svg'
import IconEditSrc from 'Assets/icons/icon-edit32x32.svg'
import IconStartSrc from 'Assets/icons/icon-play32x32.svg'
import IconTextSrc from 'Assets/icons/icon-message32x32.svg'
import IconSelectSrc from 'Assets/icons/icon-abc32x32.svg'
import IconSliderSrc from 'Assets/icons/icon-skala32x32.svg'
import IconCloseSrc from 'Assets/icons/icon-cancel32x32.svg'
import IconArrowLeftSrc from 'Assets/icons/icon-arrow_left32x32.svg'
import IconBookmarkSrc from 'Assets/icons/icon-bookmark32x32.svg'
import IconSemaforSrc from 'Assets/icons/icon-semafor32x32.svg'

const useStyles = makeStyles(theme => ({
  container: {
    height: p => p.size || 32,
    width: p => p.size || 32,
    position: 'relative',
    overflow: 'hidden',
    '& > div': {
      transform: p => `translate(50%, 50%) scale(${(p.size || 32) / 32})`,
      position: 'absolute',
      right: '50%',
      bottom: '50%',
    },
    '& svg': {
      display: 'block',
      pointerEvents: 'none',
      '& g': {
        transition: 'fill .25s ease-in-out, color .25s ease-in-out',
      },
    },
  },
}))

const gen_icon = src =>
  function Icon({ className = '', ...props }) {
    const c = useStyles()
    return (
      <div className={c.container + ' ' + className} {...props}>
        <ReactSVG src={src} />
      </div>
    )
  }

export const IconLogo = () => (
  <ReactSVG
    src={IconLogoSrc}
    loading={props => <Box width={26} height={52} {...props} />}
  />
)
export const IconUser = gen_icon(IconUserSrc)
export const IconPlus = gen_icon(IconPlusSrc)
export const IconHistory = gen_icon(IconHistorySrc)
export const IconSettings = gen_icon(IconSettingsSrc)
export const IconLogOut = gen_icon(IconLogOutSrc)
export const IconBug = gen_icon(IconBugSrc)
export const IconDelete = gen_icon(IconDeleteSrc)
export const IconEdit = gen_icon(IconEditSrc)
export const IconStart = gen_icon(IconStartSrc)
export const IconText = gen_icon(IconTextSrc)
export const IconSelect = gen_icon(IconSelectSrc)
export const IconSlider = gen_icon(IconSliderSrc)
export const IconClose = gen_icon(IconCloseSrc)
export const IconArrowLeft = gen_icon(IconArrowLeftSrc)
export const IconBookmark = gen_icon(IconBookmarkSrc)
export const IconSemafor = gen_icon(IconSemaforSrc)

export const IconAnswerType = props => {
  switch (props.type) {
    case 'select':
      return <IconSelect size={props.size} />
    case 'scale':
      return <IconSlider size={props.size} />
    case 'text':
      return <IconText size={props.size} />
    default:
      console.error('Unknown icon type.')
  }
}

const useIconBackgroundStyles = makeStyles(theme => ({
  background: {
    padding: 8,
    borderRadius: '100%',
    display: 'inline-block',
    backgroundColor: p =>
      [theme.colors.barbie, theme.colors.purple, theme.colors.yellow][
        p.color || 0
      ],
    '& g': {
      fill: theme.colors.white,
      color: theme.colors.white,
    },
  },
}))
export const IconBackground = props => {
  const c = useIconBackgroundStyles(props)
  return (
    <div className={c.background}>
      <props.icon size={props.size} />
    </div>
  )
}

export const IconBackgroundAnswerType = props => (
  <IconBackground
    color={{ text: 0, scale: 1, select: 2 }[props.type]}
    icon={p => <IconAnswerType {...p} type={props.type} />}
    {...props}
  />
)

const useBadgeAnswerTypeStyles = makeStyles(theme => ({
  badge: {
    display: 'flex',
    alignItems: 'center',
    '& > span': {
      ...theme.h4,
      marginLeft: 12,
    },
  },
}))

export const BadgeAnswerType = props => {
  const c = useBadgeAnswerTypeStyles()
  const t = hooks.usePolyglot()
  return (
    <div className={c.badge + ' ' + (props.className || '')}>
      <IconBackgroundAnswerType type={props.type} size={16} />
      <span>
        {t(
          { text: 'text', scale: 'slider', select: 'select' }[props.type]
        ).toLowerCase()}
      </span>
    </div>
  )
}

export { default as PlusIcon } from './PlusIcon'
export { default as HistoryIcon } from './HistoryIcon'
export { default as SemaforIcon } from './SemaforIcon'
export { default as SettingsIcon } from './SettingsIcon'
export { default as LogOutIcon } from './LogOutIcon'
export { default as DebugIcon } from './DebugIcon'
export { default as UserIcon } from './UserIcon'
export { default as PlayIcon } from './PlayIcon'
export { default as BookmarkIcon } from './BookmarkIcon'
