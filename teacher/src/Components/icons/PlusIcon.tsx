import React from 'react'
import { SvgIcon } from '@material-ui/core'

const PlusIcon = (props: React.ComponentProps<typeof SvgIcon>) => {
  return (
    <SvgIcon viewBox="0 0 24 24" {...props}>
      <path d="M12 2.99999996c.5522848 0 1 .44771525 1 1V11h7c.5522848 0 1 .4477152 1 1s-.4477152 1-1 1h-7v7c0 .5128359-.3860402.9355071-.8833789.9932722L12 21c-.5522848 0-1-.4477152-1-1v-7H3.99999996c-.55228475 0-1-.4477152-1-1s.44771525-1 1-1H11V3.99999996c0-.55228475.4477152-1 1-1z" />
    </SvgIcon>
  )
}

export default PlusIcon
