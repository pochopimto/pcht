import React from 'react'
import { Typography, Grid, makeStyles, Box } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2, 1, 1, 1),
  },
}))

type Props = {
  text?: string
  actions: React.ReactNode[]
}

const TooltipContent = ({ text = '', actions = [] }: Props) => {
  const c = useStyles({})

  return (
    <>
      <Box mb={4} mt={2}>
        <Typography variant="body1">{text}</Typography>
      </Box>
      <Grid container justify="flex-end" spacing={2}>
        {actions.map((a, i) => (
          <Grid item key={i}>
            {a}
          </Grid>
        ))}
      </Grid>
    </>
  )
}

export default TooltipContent
