import React, { useState } from 'react'
import { Typography, Grid, Button } from '@material-ui/core'
import Joyride, { STATUS } from 'react-joyride'

import { useCurrentUser, usePolyglot } from 'Hooks'
import agents from 'Agents'

import TooltipComponent from './TooltipComponent'
import TooltipContent from './TooltipContent'

const Intro = () => {
  const [helpers, setHelpers] = useState<any>()
  const t = usePolyglot()
  const currentUser = useCurrentUser()

  const next = () => {
    if (helpers !== undefined) helpers.next()
  }

  const skip = () => {
    if (helpers !== undefined) helpers.skip()
  }

  const handleJoyrideChange = (data: { status: any }) => {
    const { status } = data

    switch (status) {
      case STATUS.FINISHED:
      case STATUS.SKIPPED:
        agents.currentTeacher.patch({
          completed_intro: true,
        })
        break
      default:
        console.log(status)
    }
  }

  const getButton = (
    text = '',
    action = next,
    key = 1,
    color: React.ComponentProps<typeof Button>['color'] = 'primary',
    variant: React.ComponentProps<typeof Button>['variant'] = 'contained'
  ) => (
    <Button onClick={action} key={key} color={color} variant={variant}>
      {text}
    </Button>
  )

  const steps = [
    {
      target: 'body',
      placement: 'center' as 'center',
      content: (
        <TooltipContent
          text={t('intro.intro')}
          actions={[
            getButton(t('intro.skip'), skip, 2, 'default', 'outlined'),
            getButton(t('intro.next'), next),
          ]}
        />
      ),
    },
    {
      target: '.sidebar.templates',
      content: (
        <TooltipContent
          text={t('intro.templates-button')}
          actions={[getButton(t('intro.next'), next)]}
        />
      ),
    },
    {
      target: '.sidebar.history',
      content: (
        <TooltipContent
          text={t('intro.history-button')}
          actions={[getButton(t('intro.next'), next)]}
        />
      ),
    },
    {
      target: '.sidebar.semafor',
      content: (
        <TooltipContent
          text={t('intro.semafor-button')}
          actions={[getButton(t('intro.next'), next)]}
        />
      ),
    },
    {
      target: 'body',
      placement: 'center' as 'center',
      content: (
        <TooltipContent
          text={t('intro.last')}
          actions={[getButton(t('intro.finish'), next)]}
        />
      ),
    },
  ].map(s => ({ disableBeacon: true, ...s }))

  if (currentUser.lastRetrieved && !currentUser.data.completed_intro)
    return (
      <Joyride
        steps={steps}
        getHelpers={setHelpers}
        callback={handleJoyrideChange}
        tooltipComponent={TooltipComponent}
        disableOverlayClose
      />
    )
  return null
}

export default Intro
