import React from 'react'
import { Paper, makeStyles } from '@material-ui/core'
import { TooltipRenderProps } from 'react-joyride'

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    minWidth: theme.breakpoints.width('sm') / 2,
    maxWidth: theme.breakpoints.width('sm'),
  },
}))

const TooltipComponent = ({ step, tooltipProps }: TooltipRenderProps) => {
  const c = useStyles({})

  return (
    <div {...tooltipProps}>
      <Paper className={c.paper}>{step.content}</Paper>
    </div>
  )
}

export default TooltipComponent
