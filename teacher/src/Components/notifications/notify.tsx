import uuid from 'uuid'
import { createNotification } from './integration'

const notify = (message: string, type: INotificationType = 'default') => {
  return createNotification({ message, type, id: uuid() })
}
export default notify
