import React from 'react'
import { IconButton } from '@material-ui/core'
import { Close } from '@material-ui/icons'

export const notificationProvider = {
  enqueueSnackbar: (null as unknown) as ((
    message: string,
    options: object
  ) => {} | null),
  closeSnackbar: (null as unknown) as ((key: IUUID) => {} | null),
}

const closeAction = (key: IUUID) => (
  <IconButton onClick={() => notificationProvider.closeSnackbar(key)}>
    <Close />
  </IconButton>
)

const combineActions = (actions: ((key: IUUID) => JSX.Element)[]) => (
  key: IUUID
) => {
  return <>{actions.map(action => action(key))}</>
}

export const createNotification = ({
  message,
  actions = [],
  id,
  type,
  ...rest
}: INotification) => {
  if (notificationProvider.enqueueSnackbar !== null) {
    notificationProvider.enqueueSnackbar(message, {
      autoHideDuration: 3000,
      anchorOrigin: {
        vertical: 'top',
        horizontal: 'right',
      },
      action: combineActions([...actions, closeAction]),
      key: id,
      variant: type,
      ...rest,
    })
  } else {
    alert(message)
  }
}
