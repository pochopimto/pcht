import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { makeStyles } from '@material-ui/core'

import { buttons, icons, inputs, sliders } from 'Components'
import * as hooks from 'Hooks'
import l from 'Lib'

import HelperPopup from './helper-popup'

const useQuestionListItemStyles = makeStyles((theme) => ({
  question: {
    padding: '26px 64px 26px 32px',
    margin: '-2px 0',
    backgroundColor: (p) =>
      p.active ? theme.colors.grey3 : theme.colors.white + '00',
    transition: 'all .25s ease-in-out',
    cursor: 'pointer',
    '&:first-child': {
      marginTop: 0,
    },
    '&:last-child': {
      marginBottom: 0,
    },
    position: 'relative',
    '&:not(:first-child) > div:first-child': {
      opacity: 0,
    },
    overflowX: 'hidden',
    '&:hover': {
      backgroundColor: theme.colors.grey3,
    },
  },
  actions: {
    position: 'absolute',
    right: 16,
    top: 16,
    display: 'flex',
    flexDirection: 'column',
  },
  type: {},
  content: {
    ...theme.p,
    paddingTop: 8,
    overflowWrap: 'break-word',
  },
  border: {
    position: 'absolute',
    left: 22,
    right: 22,
    borderTop: `1px solid ${theme.colors.grey3}`,
  },
  borderTop: {
    top: 0,
  },
  borderBottom: {
    bottom: 0,
  },
}))

const QuestionListItem = ({ active, ...props }) => {
  const c = useQuestionListItemStyles({ active, id: props.question.id })
  return (
    <div className={c.question} onClick={props.onClick}>
      <div className={c.borderTop + ' ' + c.border} />
      <icons.BadgeAnswerType className={c.type} type={props.question.type} />
      <div className={c.content}>{props.question.question}</div>
      <div className={c.actions}>
        {/* TODO: removing questions from surveys <buttons.ButtonDelete size={24}/> */}
      </div>
      <div className={c.borderBottom + ' ' + c.border} />
    </div>
  )
}

const useAnswerDivStyles = makeStyles((theme) => ({
  fancyDiv: {
    '&:not(:first-child)': {
      marginTop: 8,
    },
    '&:not(:last-child)': {
      marginBottom: 8,
    },
    borderRadius: 20,
    border: `1px solid ${theme.colors.grey2}`,
    backgroundColor: theme.colors.white,
    boxShadow: '0 6px 16px 0 rgba(221,221,221,0.5)',
  },
}))

const AnswerDiv = (props) => {
  const c = useAnswerDivStyles()
  return (
    <div className={c.fancyDiv + ' ' + (props.className || '')}>
      {props.children}
    </div>
  )
}

const useStyles = makeStyles((theme) => ({
  container: {
    flex: '1 1 auto',
    display: 'flex',
  },
  leftColumn: {
    flex: '0 0 auto',
    width: 296,
    borderRight: `1px solid ${theme.colors.grey3}`,
    display: 'flex',
    flexDirection: 'column',
  },
  header: {
    padding: 32,
  },
  surveyCode: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    '& > .hint': {
      ...theme.hint,
    },
    '& > .code': {
      ...theme.h3,
      color: theme.colors.barbie,
      fontWeight: 500,
    },
  },
  surveyName: {
    marginTop: 21,
  },
  questionList: {
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  questionView: {
    flex: '1 1 auto',
    display: 'flex',
    justifyContent: 'center',
    padding: '80px 64px',
  },
  questionContainer: {
    flex: '0 1 668px',
    display: 'flex',
    flexDirection: 'column',
  },
  questionQuestion: {
    ...theme.h3,
  },
  questionAnswers: {
    marginTop: 33,
    flex: '0 1 auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    overflow: 'hidden',
  },
  questionAnswersCount: {
    padding: '7px 24px',
    backgroundColor: theme.colors.grey1,
    fontSize: 15,
    fontWeight: 500,
    color: theme.colors.grey4,
    borderRadius: 12,
  },
  questionResults: {
    flex: '0 1 auto',
    marginTop: 48,
    width: 'calc(100% - 20px)',
    display: 'flex',
    flexDirection: 'column',
    overflowY: 'auto',
    overflowX: 'hidden',
    padding: '10px 10px',
  },
  sliderResult: {
    padding: '62px 40px 42px 40px',
    display: 'flex',
  },
  textResult: {
    ...theme.p,
    fontSize: 16,
    fontWeight: 500,
    padding: '30px 14px 26px 40px',
    display: 'flex',
    justifyContent: 'space-between',
  },
  buttonDelete: {
    paddingLeft: 5,
  },
  selectResult: {
    padding: '30px 73px 10px 40px',
    '& > div': {
      marginBottom: 34,
      '& > span': {
        ...theme.p,
        fontWeight: 500,
        marginBottom: -3,
      },
      '& > div': {
        marginTop: -3,
        display: 'flex',
        alignItems: 'center',
        '& > span': {
          ...theme.h5,
          fontWeight: 600,
          marginLeft: 63,
        },
      },
    },
  },
  saveAsSurvey: {
    marginTop: 24,
  },
}))

const SurveyView = (props) => {
  // Tohle jsem přesunul do toho hooku -> useTextAnswerDelete
  // const [showLoaderForButtonIds, setShowLoaderForButtonIds] = useState([])

  const c = useStyles()

  const { surveyId } = props.match.params
  const survey = hooks.useSurvey(surveyId)

  const createTemplate = hooks.useCreateTemplate()

  const t = hooks.usePolyglot()

  const { showHelp } = props.location.state || { showHelp: false }
  const [showHelperPopup, setShowHelperPopup] = useState(showHelp)

  const [deleteTextAnswer, showLoaderForButtonIds] = hooks.useTextAnswerDelete()

  const onTextAnswerDelete = (e, id) => {
    e.stopPropagation()
    deleteTextAnswer(id)
  }

  const [activeQuestionId, setActiveQuestionId] = useState(undefined)
  const activeQuestion =
    survey.data.questions.find((q) => q.id === activeQuestionId) || undefined
  const activeQuestionIndex =
    activeQuestion === undefined
      ? undefined
      : survey.data.questions.indexOf(activeQuestion)
  const activeAnswer =
    activeQuestionIndex === undefined
      ? undefined
      : survey.data.answers
          .map((a) => a.answers[activeQuestionIndex])
          .filter((a) => a !== undefined)
          .filter((a) => activeQuestion.type !== 'text' || a.text.length > 0)

  useEffect(() => {
    if (
      activeQuestionId === undefined &&
      survey.data.questions &&
      survey.data.questions.length > 0
    ) {
      setActiveQuestionId(survey.data.questions[0].id)
    }
  }, [activeQuestionId, survey])

  return (
    <div className={c.container}>
      <div className={c.leftColumn}>
        <div className={c.header}>
          <div className={c.surveyCode}>
            <span className="hint">
              {t('results.survey_code').toLowerCase()}
            </span>
            <span className="code">{survey.data.id.toUpperCase()}</span>
          </div>
          <inputs.Input
            className={c.surveyName}
            value={survey.data.name}
            disabled={true}
            label={l.firstUpper(t('results.survey_name'))}
          />
          <buttons.SaveAsTemplateButton
            className={c.saveAsSurvey}
            onClick={() => createTemplate(survey.data)}
          />
        </div>
        <div className={c.questionList}>
          {survey.data.questions.map((q) => (
            <QuestionListItem
              question={q}
              key={q.id}
              active={q.id === activeQuestionId}
              onClick={() => setActiveQuestionId(q.id)}
            />
          ))}
        </div>
      </div>
      {activeQuestion !== undefined && (
        <div className={c.questionView}>
          <div className={c.questionContainer}>
            <div className={c.questionQuestion}>
              {
                survey.data.questions.find((q) => q.id === activeQuestionId)
                  .question
              }
            </div>
            <div className={c.questionAnswers}>
              <div className={c.questionAnswersCount}>
                {l.firstUpper(t('results.answer_count'))}:{' '}
                {(activeAnswer || []).length}
              </div>
              <div className={c.questionResults}>
                {(() => {
                  switch (activeQuestion.type) {
                    case 'text':
                      return activeAnswer.map((a) => (
                        <AnswerDiv key={a.id} className={c.textResult}>
                          {a.text}
                          <buttons.ButtonDelete
                            className={c.buttonDelete}
                            size={24}
                            onClick={(e) => onTextAnswerDelete(e, a.id)}
                            isLoading={showLoaderForButtonIds.includes(a.id)}
                          />
                        </AnswerDiv>
                      ))
                    case 'scale':
                      return (
                        <AnswerDiv className={c.sliderResult}>
                          <sliders.NumberedSlider
                            value={
                              activeAnswer.reduce(
                                (c, a) => c + a.number / 10,
                                0
                              ) / (activeAnswer.length || 1)
                            }
                          />
                        </AnswerDiv>
                      )
                    case 'select':
                      return (
                        <AnswerDiv className={c.selectResult}>
                          {activeQuestion.options.map((o) => (
                            <div key={o.id}>
                              <span> {o.text} </span>
                              <div>
                                <sliders.Slider
                                  color={2}
                                  value={
                                    activeAnswer.filter(
                                      (a) => a.option.id === o.id
                                    ).length / activeAnswer.length
                                  }
                                />
                                <span>
                                  {' '}
                                  {
                                    activeAnswer.filter(
                                      (a) => a.option.id === o.id
                                    ).length
                                  }
                                  ×{' '}
                                </span>
                              </div>
                            </div>
                          ))}
                        </AnswerDiv>
                      )
                  }
                })()}
              </div>
            </div>
          </div>
        </div>
      )}

      {showHelperPopup && (
        <HelperPopup
          surveyId={surveyId}
          onBack={() => props.history.push('/')}
          onContinue={() => setShowHelperPopup(false)}
        />
      )}
    </div>
  )
}

export default withRouter(SurveyView)
