import React from 'react'
import { Typography, makeStyles } from '@material-ui/core'
import dayjs from 'dayjs'
import { withRouter } from 'react-router-dom'

import * as hooks from 'Hooks'
import l from 'Lib'

import { buttons, lists } from 'Components'

const useSurveyItemStyles = makeStyles(theme => ({
  survey: {
    display: 'flex',
    flex: '0 0 auto',
    '& > span': {
      ...theme.h4,
    },
    '& > div': {
      margin: [-9, -8, -9, 0],
      display: 'flex',
      alignItems: 'center',
    },
  },
  date: {
    ...theme.h4,
    marginRight: 48,
    color: theme.colors.grey4,
  },
  deleteButton: {
    marginRight: 24,
  },
}))

const SurveyItem = withRouter(function SurveyItem(props) {
  const c = useSurveyItemStyles()
  const deleteSurvey = hooks.useDeleteSurvey()
  const t = hooks.usePolyglot()

  const onDelete = e => {
    e.stopPropagation()
    deleteSurvey(props.survey.id)
  }

  const onOpen = e => {
    e.stopPropagation()
    props.history.push(`${props.match.path}/${props.survey.id}`)
  }

  return (
    <lists.ListItem className={c.survey} onClick={onOpen}>
      <span>{props.survey.name}</span>
      <div>
        <span className={c.date + ' ' + c.cursorDefault}>
          {dayjs(props.survey.date).format('DD.MM.YYYY')}
        </span>
        <buttons.ButtonDelete
          className={c.deleteButton}
          size={24}
          onClick={onDelete}
        />
        <buttons.TextButtonFilled className={c.openButton} onClick={onOpen}>
          {l.firstUpper(t('results.open'))}
        </buttons.TextButtonFilled>
      </div>
    </lists.ListItem>
  )
})

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flex: '1 1 auto',
    margin: '93px 64px 50px 78px',
  },
  header: {
    '& > span': {
      ...theme.h2,
    },
  },
  surveyLists: {
    overflowY: 'auto',
    marginTop: 83,
    display: 'flex',
    flexDirection: 'column',
    flex: '1 1 auto',
  },
  surveyList: {
    display: 'flex',
    flexDirection: 'column',
    flex: '1 1 auto',
  },
  noResults: {
    ...theme.h3,
    marginTop: 60,
  },
}))

const Selection = () => {
  const c = useStyles()
  const surveys = hooks.useSurveys()
  const t = hooks.usePolyglot()

  return (
    <div className={c.container}>
      <div className={c.header}>
        <span>{l.firstUpper(t('results.history'))}</span>
      </div>
      {Object.keys(surveys.data).length > 0 ? (
        <div className={c.surveyLists}>
          <div className={c.surveyList}>
            {Object.values(surveys.data)
              //.filter(s => /*check if it is finished */ true)
              .sort((a, b) => b.date - a.date)
              .map(s => (
                <SurveyItem key={s.id} surveyId={s.id} survey={s} />
              ))}
          </div>
        </div>
      ) : (
        <Typography>{t('results.no_results')}</Typography>
      )}
    </div>
  )
}

export default withRouter(Selection)
