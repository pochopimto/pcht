import React from 'react'
import { withRouter, Switch, Route } from 'react-router-dom'

import Selection from './selection'
import SurveyView from './survey-view'

const Results = props => {
  return (
    <Switch>
      <Route exact path={props.match.path} component={Selection} />
      <Route path={props.match.path + '/:surveyId'} component={SurveyView} />
    </Switch>
  )
}

export default withRouter(Results)
