import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core'
import {
  NavLink,
  Link,
  withRouter,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom'

import l from 'Lib'
import actions from 'Actions'
import * as hooks from 'Hooks'
import { icons, buttons, popups } from 'Components'

import DemoPicture from 'Assets/images/incognito.png'

const useStyles = makeStyles(theme => ({
  container: {
    padding: 0,
  },
  back: {
    position: 'absolute',
    top: 36,
    left: 0,
    transform: 'translateX(-50%)',
    padding: '8px 9px 8px 7px',
    borderRadius: '100%',
    backgroundColor: theme.colors.purple,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
  },
  textContainer: {
    marginTop: 63,
    marginRight: 384,
    marginBottom: 48,
    marginLeft: 66,
    width: 270,
    '& > .header': {
      ...theme.h3,
      margin: 0,
    },
    '& > .code': {
      marginTop: 20,
      color: theme.colors.purple,
      '& > span:first-child': {
        fontSize: 13,
        fontWeight: 500,
      },
      '& > span:last-child': {
        fontSize: theme.h3.fontSize,
        fontWeight: theme.h3.fontWeight,
      },
    },
    '& > .info': {
      ...theme.hint,
      fontWeight: 500,
      color: theme.colors.darkpurple,
      margin: '13px 0 0 0',
      overflowWrap: 'break-word',
      '&.info2': {
        marginTop: 0,
      },
      '& > a': {
        fontWeight: 700,
      },
    },
  },
  button: {
    marginTop: 28,
  },
  demoPicture: {
    height: 240,
    width: 228,
    position: 'absolute',
    right: 51,
    bottom: 0,
  },
}))

const Page = props => {
  const c = useStyles(props)
  const t = hooks.usePolyglot()

  return (
    <popups.Popup className={c.container}>
      {props.onBack && (
        <div className={c.back} onClick={props.onBack}>
          <icons.IconArrowLeft size={16} />
        </div>
      )}
      <div className={c.textContainer}>
        <h3 className="header">{l.firstUpper(t('results.survey_started'))}</h3>
        <div className="code">
          <span>{t('results.survey_code').toLowerCase()}</span> <br />
          <span>{props.surveyId.toUpperCase()}</span>
        </div>
        <p className={'info'}>
          {l.firstUpper(t('results.go_to_student_app_located1'))}
        </p>
        <p className={'info info2'}>
          {l.firstUpper(t('results.go_to_student_app_located2'))}{' '}
          <a href={'http://pcht.eu/student/' + props.surveyId}>pcht.eu</a>.
        </p>
        <buttons.TextButtonFilled
          className={c.button}
          level={1}
          onClick={props.onContinue}
        >
          {l.firstUpper(t('results.show_survey'))}
        </buttons.TextButtonFilled>
      </div>

      <img className={c.demoPicture} src={DemoPicture} />
    </popups.Popup>
  )
}

export default withRouter(Page)
