import React from 'react'
import { makeStyles } from '@material-ui/styles'
import { CircularProgressbar } from 'react-circular-progressbar'
import 'react-circular-progressbar/dist/styles.css'

import * as hooks from 'Hooks'
import l from 'Lib'

const lightColors = theme => ({
  ok: theme.colors.lightpurple,
  mid: theme.colors.lightyellow,
  bad: theme.colors.lightbarbie,
})

const colors = theme => ({
  ok: theme.colors.purple,
  mid: theme.colors.yellow,
  bad: theme.colors.barbie,
})

const useStyles = makeStyles(theme => ({
  widget: {
    padding: '96px 86px',
    backgroundColor: theme.colors.white,
    borderRadius: 20,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  widget2: {
    boxShadow: `0 6px 16px 0 ${theme.colors.grey4}}`,
    border: `1px solid ${theme.colors.grey2}`,
  },
  legend: {
    flex: '0 0 auto',
  },
  hint: {
    display: 'flex',
    alignItems: 'center',
    margin: '8px 0',
  },
  hintText: {
    ...theme.h5,
    whiteSpace: 'nowrap',
  },
  circle: {
    width: 8,
    minWidth: 8,
    height: 8,
    minHeight: 8,
    borderRadius: '100%',
    marginRight: 16,
  },
  circles: {
    height: 96,
    display: 'flex',
  },
  ...(types => {
    let styles = {}
    types.forEach(type => {
      styles[`progressbar_root_${type}`] = {
        width: '100%',
        verticalAlign: 'middle',
      }
      styles[`progressbar_trail_${type}`] = {
        stroke: lightColors(theme)[type],
        strokeLinecap: 'round',
      }
      styles[`progressbar_text_${type}`] = {
        fill: theme.colors.darkpurple,
        stroke: theme.colors.darkpurple,
        fontSize: 20,
        dominantBaseline: 'middle',
        textAnchor: 'middle',
      }
      styles[`progressbar_path_${type}`] = {
        stroke: colors(theme)[type],
        strokeLinecap: 'round',
        transition: 'stroke-dashoffset .25s ease',
      }
      styles[`background_${type}`] = {
        backgroundColor: colors(theme)[type],
      }
    })
    return styles
  })(['ok', 'mid', 'bad']),
}))
const TrafficLightsWidget = ({ counts, className = '', ...props }) => {
  const c = useStyles()
  const t = hooks.usePolyglot()

  return (
    <div className={`${c.widget} ${c.widget2} ${className}`}>
      <div className={c.legend}>
        {['ok', 'mid', 'bad'].map(type => (
          <div className={c.hint} key={type}>
            <div className={`${c.circle} ${c[`background_${type}`]}`} />
            <div className={c.hintText}>
              {l.firstUpper(t(`traffic_lights.${type}`))}
            </div>
          </div>
        ))}
      </div>
      <div className={c.circles}>
        {['ok', 'mid', 'bad'].map((type, i) => (
          <CircularProgressbar
            key={type}
            text={`${counts[i]}×`}
            value={counts[i]}
            maxValue={counts.reduce((a, b) => a + b, 0)}
            classes={{
              root: c[`progressbar_root_${type}`],
              trail: c[`progressbar_trail_${type}`],
              path: c[`progressbar_path_${type}`],
              text: c[`progressbar_text_${type}`],
            }}
          />
        ))}
      </div>
    </div>
  )
}

export default TrafficLightsWidget
