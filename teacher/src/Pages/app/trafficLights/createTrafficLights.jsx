import React from 'react'
import { withRouter, Redirect } from 'react-router'

import { Loader } from 'Components/loaders'
import * as hooks from 'Hooks'

const CreateTrafficLights = props => {
  const trafficLights = hooks.useCreateTrafficLights()

  if (trafficLights === undefined) return <Loader />
  else return <Redirect to={props.match.path + '/' + trafficLights.id} />
}

export default withRouter(CreateTrafficLights)
