import React from 'react'
import { makeStyles } from '@material-ui/core'

import { Loader } from 'Components/loaders'
import * as hooks from 'Hooks'
import l from 'Lib'

import TrafficLightsWidget from './trafficLightsWidget'

const useStyles = makeStyles(theme => ({
  trafficLights: {
    width: '100%',
    minHeight: '100%',
    padding: '80px 125px',
  },
  heading: {
    ...theme.h3,
    '& > span': {
      marginLeft: 20,
      ...theme.p,
    },
  },
  widget: {
    marginTop: 112,
  },
}))

const TrafficLights = props => {
  const c = useStyles()
  const trafficLights = hooks.useTrafficLights(props.match.params.id)
  const t = hooks.usePolyglot()

  if (trafficLights === undefined) return <Loader />
  const counts = ['ok', 'mid', 'bad'].map(
    type => trafficLights.entries.filter(e => e.state === type).length
  )

  return (
    <div className={c.trafficLights}>
      <div className={c.heading}>
        {l.firstUpper(t('traffic_lights.traffic_lights'))}
        <span>
          {l.firstUpper(t('traffic_lights.code'))}: {trafficLights.id}
        </span>
      </div>
      <TrafficLightsWidget className={c.widget} counts={counts} />
    </div>
  )
}

export default TrafficLights
