import React from 'react'
import { makeStyles } from '@material-ui/core'
import { Switch, Route, withRouter } from 'react-router'

import CreateTrafficLights from './createTrafficLights'
import TrafficLightsView from './trafficLightsView'

import * as hooks from 'Hooks'
import l from 'Lib'

const useStyles = makeStyles(theme => ({}))

const TrafficLights = props => {
  const c = useStyles()

  return (
    <Switch>
      <Route path={props.match.path} exact component={CreateTrafficLights} />
      <Route
        path={props.match.path + '/:id'}
        exact
        component={TrafficLightsView}
      />
    </Switch>
  )
}

export default withRouter(TrafficLights)
