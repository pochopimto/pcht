import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core'

import * as hooks from 'Hooks'
import l from 'Lib'
import { icons, buttons, popups } from 'Components'

const useUserInfoPopupStyles = makeStyles(theme => ({
  popup: {
    minWidth: 400,
  },
  userInfo: {
    marginTop: 45,
    display: 'flex',
  },
  userPicture: {
    padding: 16,
    border: `1.3px solid  ${theme.colors.grey2}`,
    borderRadius: '100%',
  },
  userText: {
    marginLeft: 24,
  },
  name: {
    ...theme.h3,
  },
  stats: {
    marginTop: 32,
    '& > div': {
      padding: '16px 40px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      border: `1px solid ${theme.colors.grey2}`,
      borderRadius: 20,
      '& > .name': {
        ...theme.h4,
      },
      '& > .value': {
        ...theme.h3,
      },
      '&:not(:first-child)': {
        marginTop: 8,
      },
    },
  },
}))

export const UserInfoPopup = props => {
  const c = useUserInfoPopupStyles()
  const currentUser = hooks.useCurrentUser()
  const surveys = hooks.useSurveys()
  const t = hooks.usePolyglot()

  return (
    <popups.Popup className={c.popup}>
      <popups.Header>
        <buttons.TextButtonFilled>
          {l.firstUpper(t('sidebar.pay'))}
        </buttons.TextButtonFilled>
        <buttons.ButtonClose size={24} onClick={props.onClose} />
      </popups.Header>
      <div className={c.userInfo}>
        <div className={c.userPicture}>
          <icons.IconUser />
        </div>
        <div className={c.userText}>
          <div className={c.name}>{currentUser.data.name}</div>
        </div>
      </div>
      <div className={c.stats}>
        <div>
          <div className="name">{l.firstUpper(t('sidebar.survey_count'))}</div>
          <div className="value">
            {surveys.data && Object.keys(surveys.data).length}
          </div>
        </div>
        <div>
          <div className="name">{l.firstUpper(t('sidebar.answer_count'))}</div>
          <div className="value">
            {surveys.data &&
              Object.values(surveys.data).reduce(
                (s, v) => s + v.answers.length,
                0
              )}
          </div>
        </div>
      </div>
    </popups.Popup>
  )
}

export default UserInfoPopup
