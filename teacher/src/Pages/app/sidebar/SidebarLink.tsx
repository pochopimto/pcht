import React from 'react'
import { Tooltip, IconButton, Typography, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  button: {
    transition: theme.transitions.create('color'),
    color: theme.colors.grey4,
    '&:hover': {
      color: theme.colors.grey3,
    },
  },
}))

type Props = {
  children: React.ReactChildren
  className?: string
  onClick?: () => void
  tooltip?: string
}

const SidebarLink = ({
  children,
  className = '',
  onClick,
  tooltip = '',
}: Props) => {
  const c = useStyles()
  return (
    <Tooltip
      title={<Typography variant="body2">{tooltip}</Typography>}
      placement="right"
      enterDelay={300}
    >
      <IconButton className={`${className} ${c.button}`} onClick={onClick}>
        {children}
      </IconButton>
    </Tooltip>
  )
}

export default SidebarLink
