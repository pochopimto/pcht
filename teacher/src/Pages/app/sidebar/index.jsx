import React, { useState } from 'react'
import { makeStyles, Grid, Box, IconButton } from '@material-ui/core'
import { withRouter } from 'react-router-dom'

import { icons, buttons } from 'Components'
import {
  PlusIcon,
  HistoryIcon,
  SemaforIcon,
  SettingsIcon,
  LogOutIcon,
  DebugIcon,
  UserIcon,
} from 'Components/icons'
import { navigateTo } from 'Routes'
import * as hooks from 'Hooks'

import SidebarLink from './SidebarLink'
import SettingsPopup from './settings'
import UserInfoPopup from './user-info'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    flex: '0 0 96px',
    backgroundColor: theme.colors.sidebarBackground,
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(2),
  },
  userInfo: {
    border: `1px solid ${theme.colors.bat}`,
  },
  settings: {
    borderTop: `1px solid ${theme.colors.bat}`,
    borderBottom: `1px solid ${theme.colors.bat}`,
    margin: theme.spacing(0, 1),
    width: `calc(100% - ${theme.spacing(2)}px)`,
    display: 'flex',
    justifyContent: 'center',
  },
}))

const Sidebar = () => {
  const c = useStyles()
  const [currentPopup, setCurrentPopup] = useState(undefined)
  const t = hooks.usePolyglot()

  const closePopup = () => setCurrentPopup(undefined)
  let popup
  switch (currentPopup) {
    case 'settings':
      popup = <SettingsPopup onClose={closePopup} />
      break
    case 'user-info':
      popup = <UserInfoPopup onClose={closePopup} />
      break
    default:
      popup = undefined
  }

  return (
    <Grid container className={c.container}>
      <Grid item container spacing={6} alignItems="center" direction="column">
        <Grid item>
          <icons.IconLogo />
        </Grid>
        <Grid item>
          <SidebarLink
            className={`${c.userInfo} sidebar userInfo`}
            tooltip={t('sidebar.userInfo')}
            onClick={() => setCurrentPopup('user-info')}
          >
            <UserIcon fontSize="large" />
          </SidebarLink>
        </Grid>
      </Grid>
      <Box flexGrow={1} />
      <Grid item container spacing={2} alignItems="center" direction="column">
        <Grid item>
          <SidebarLink
            tooltip={t('sidebar.templates')}
            className={`sidebar templates`}
            onClick={() => navigateTo('/templates')}
          >
            <PlusIcon fontSize="large" />
          </SidebarLink>
        </Grid>
        <Grid item>
          <SidebarLink
            tooltip={t('sidebar.history')}
            className={`sidebar history`}
            onClick={() => navigateTo('/results')}
          >
            <HistoryIcon fontSize="large" />
          </SidebarLink>
        </Grid>
        <Grid item>
          <SidebarLink
            tooltip={t('sidebar.semafor')}
            className={`sidebar semafor`}
            onClick={() => navigateTo('/traffic-lights')}
          >
            <SemaforIcon fontSize="large" />
          </SidebarLink>
        </Grid>
      </Grid>
      <Box flexGrow={3} />
      <Grid item container spacing={2} alignItems="center" direction="column">
        <Grid item className={c.settings}>
          <SidebarLink
            tooltip={t('sidebar.settings')}
            className={`sidebar settings`}
            onClick={() => setCurrentPopup('settings')}
          >
            <SettingsIcon fontSize="large" />
          </SidebarLink>
        </Grid>
        <Grid item>
          <SidebarLink
            tooltip={t('sidebar.logout')}
            onClick={() => navigateTo('/logout')}
          >
            <LogOutIcon fontSize="large" />
          </SidebarLink>
        </Grid>
      </Grid>
      <Box flexGrow={0.5} />
      <Grid item container spacing={2} alignItems="center" direction="column">
        <Grid item>
          <SidebarLink
            tooltip={t('sidebar.feedback')}
            onClick={() =>
              window.location.replace(
                `mailto:feedback@pochopimto.cz?subject=${t(
                  'feedback.subject'
                )}&body=${t('feedback.body')}`
              )
            }
          >
            <DebugIcon fontSize="large" />
          </SidebarLink>
        </Grid>
      </Grid>
      <Box>{popup}</Box>
    </Grid>
  )
}

export default withRouter(Sidebar)
