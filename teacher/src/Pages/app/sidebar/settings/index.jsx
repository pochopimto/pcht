import React from 'react'
import { makeStyles } from '@material-ui/core'

import { buttons, popups } from 'Components'
import * as hooks from 'Hooks'
import l from 'Lib'

const useSettingsPopupStyles = makeStyles((theme) => ({
  popup: {
    minWidth: 400,
  },
  menu: {
    margin: '80px 0',
    display: 'flex',
    flexDirection: 'column',
  },
  link: {
    ...theme.h4,
    color: theme.colors.grey4,
    '&.enabled:hover': {
      color: theme.h4.color,
    },
    '&:not(:first-child)': {
      marginTop: 16,
    },
  },
}))

export const SettingsPopup = (props) => {
  const c = useSettingsPopupStyles()
  const t = hooks.usePolyglot()

  return (
    <popups.Popup className={c.popup}>
      <popups.Header>
        <buttons.ButtonClose size={24} onClick={props.onClose} />
      </popups.Header>
      <div className={c.menu}>
        {/*
          <buttons.Button className={c.link}>
            {l.firstUpper(t('sidebar.manage_subscription'))}
          </buttons.Button>
        */}
        <buttons.Button
          className={c.link}
          href={'mailto:info@pochopimto.cz'}
          newTab
        >
          {l.firstUpper(t('sidebar.support'))}
        </buttons.Button>
        <buttons.Button className={c.link}>
          {l.firstUpper(t('sidebar.version', { version: '2.0' }))}
        </buttons.Button>
      </div>
    </popups.Popup>
  )
}

export default SettingsPopup
