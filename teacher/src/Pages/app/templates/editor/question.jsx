import React from 'react'
import { makeStyles } from '@material-ui/core'

import { icons, buttons } from 'Components'
import * as hooks from 'Hooks'

const padding = [18, 18, 18, 12]
const useStyles = makeStyles(theme => ({
  question: {
    padding: padding.map(p => p + 'px').join(' '),
    borderBottom: `1px solid ${theme.colors.grey3}`,
    display: 'flex',
    width: `calc(100% - ${padding[0] + padding[2]}px)`,
  },
  body: {
    flex: '1 1 auto',
  },
  header: {
    marginTop: 6,
  },
  content: {
    marginTop: 15,
    display: 'flex',
    flexDirection: 'column',
  },
  questionText: {
    ...theme.p,
    overflowWrap: 'anywhere',
    overflow: 'hidden',
    flex: '0 1 auto',
  },
  actions: {
    marginLeft: 16,
    marginTop: -18 + 1,
    '& > *': {
      marginTop: 16,
    },
  },
}))

const Question = props => {
  const c = useStyles()
  const question = props.question
  const t = hooks.usePolyglot()

  return (
    <div className={c.question}>
      <div className={c.body}>
        <icons.BadgeAnswerType className={c.header} type={question.type} />
        <div className={c.content}>
          <div className={c.questionText}>{question.question}</div>
        </div>
      </div>
      <div className={c.actions}>
        {props.onEdit && (
          <buttons.ButtonEdit onClick={props.onEdit} size={24} />
        )}
        {props.onDelete && (
          <buttons.ButtonDelete onClick={props.onDelete} size={24} />
        )}
      </div>
    </div>
  )
}

export default Question
