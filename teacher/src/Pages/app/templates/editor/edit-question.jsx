import React, { useState, useRef, useEffect } from 'react'
import { makeStyles } from '@material-ui/core'
import useSmoothScroll from 'use-smooth-scroll'
import uuid from 'uuid'

import l from 'Lib'
import * as hooks from 'Hooks'
import { buttons, popups, inputs } from 'Components'
import { Form } from 'Components/forms'

const useStyles = makeStyles(theme => ({
  editQuestionPopup: {},
  actions: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginRight: -24,
    marginTop: -16,
    minWidth: '50vw',
  },
  cancelButton: {
    marginLeft: 17,
  },
  content: {
    marginTop: 31,
    display: 'flex',
    flexDirection: 'column',
  },
  typeChooser: {
    marginTop: 27,
    '& > span': {
      ...theme.hint,
    },
    '& > div': {
      marginTop: 13,
      marginLeft: -17,
      display: 'flex',
      justifyContent: 'flex-start',
      '& > *': {
        marginLeft: 24,
      },
    },
  },
  optionsContainer: {
    marginTop: 27,
    '& > span': {
      ...theme.hint,
    },
  },
  optionsContainerInactive: {
    display: 'none',
  },
  optionsList: {
    marginTop: 13,
    marginRight: -15,
    paddingRight: 15,
    display: 'flex',
    flexDirection: 'column',
    maxHeight: 'calc(100vh - 400px)',
    overflowY: 'auto',
  },
  option: {
    marginBottom: 26,
  },
}))

const EditQuestionPopup = props => {
  const c = useStyles()
  const t = hooks.usePolyglot()
  const optionListRef = useRef()
  const scrollTo = useSmoothScroll('y', optionListRef)
  const [question, setQuestion] = useState({
    question: '',
    type: 'text',
    nextOptionId: uuid(),
    options: [],
    ...props.question,
  })
  useEffect(() => {
    scrollTo(optionListRef.current.scrollHeight)
  }, [question.nextOptionId])
  const addOption = option => {
    setQuestion({
      ...question,
      options: [
        ...(question.options || []),
        {
          ...option,
          id: question.nextOptionId,
        },
      ],
      nextOptionId: uuid(),
    })
  }
  const editOption = option =>
    setQuestion({
      ...question,
      options: question.options.map(o => (o.id === option.id ? option : o)),
    })
  const deleteOption = option =>
    setQuestion({
      ...question,
      options: question.options.filter(o => o.id !== option.id),
    })

  const validName = question.question.match(/[^ ]/)
  const validQuestion =
    validName &&
    (question.type !== 'select' ||
      (question.options.length >= 2 &&
        question.options.filter(o => !o.text.match(/[^ ]/)).length === 0))

  return (
    <popups.Popup>
      <div className={c.editQuestionPopup}>
        <Form onSubmit={() => props.onSave(question)}>
          <div className={c.actions}>
            <buttons.TextButtonFilled
              disabled={!validQuestion}
              onClick={() => props.onSave(question)}
            >
              {l.firstUpper(t('editor.save'))}
            </buttons.TextButtonFilled>
            <buttons.ButtonClose
              className={c.cancelButton}
              onClick={props.onClose}
              size={24}
            />
          </div>
          <div className={c.content}>
            <inputs.Input
              placeholder={l.firstUpper(t('editor.question'))}
              value={question.question}
              onChange={val => setQuestion({ ...question, question: val })}
              label={l.firstUpper(t('editor.question'))}
              autoFocus
            />
            <div className={c.typeChooser}>
              <span>{l.firstUpper(t('editor.choose_answer_type'))}</span>
              <div>
                <buttons.ChipButton
                  active={question.type === 'select'}
                  type={'select'}
                  disabled={!validName}
                  onClick={() => setQuestion({ ...question, type: 'select' })}
                />
                <buttons.ChipButton
                  active={question.type === 'scale'}
                  type={'scale'}
                  disabled={!validName}
                  onClick={() => setQuestion({ ...question, type: 'scale' })}
                />
                <buttons.ChipButton
                  active={question.type === 'text'}
                  type={'text'}
                  disabled={!validName}
                  onClick={() => setQuestion({ ...question, type: 'text' })}
                />
              </div>
            </div>
            <div
              className={
                c.optionsContainer +
                ' ' +
                (question.type === 'select' ? '' : c.optionsContainerInactive)
              }
            >
              <span>{l.firstUpper(t('editor.add_options'))}</span>
              <div className={c.optionsList} ref={optionListRef}>
                {[
                  ...(question.options || []).map((o, i) => (
                    <inputs.Input
                      className={c.option}
                      key={o.id}
                      value={o.text}
                      label={String.fromCharCode('A'.charCodeAt(0) + i)}
                      onChange={val => editOption({ ...o, text: val })}
                      onDelete={() => deleteOption(o)}
                    />
                  )),
                  <inputs.Input
                    className={c.newOption}
                    key={question.nextOptionId}
                    value={''}
                    label={'+'}
                    onChange={val => addOption({ text: val })}
                    message={
                      question.options.length < 2
                        ? l.firstUpper(
                            t(
                              'editor.few_more_options_needed',
                              2 - question.options.length
                            )
                          )
                        : undefined
                    }
                    error={question.options.length < 2}
                  />,
                ]}
              </div>
            </div>
          </div>
        </Form>
      </div>
    </popups.Popup>
  )
}

export default EditQuestionPopup
