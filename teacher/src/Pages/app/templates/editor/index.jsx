import React, { useState, useRef, useEffect, useCallback } from 'react'
import { makeStyles } from '@material-ui/core'
import useSmoothScroll from 'use-smooth-scroll'
import uuid from 'uuid'
import {
  NavLink,
  Link,
  withRouter,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom'

import * as hooks from 'Hooks'
import l from 'Lib'

import Question from './question'
import EditQuestionPopup from './edit-question'

import { icons, buttons, inputs, popups } from 'Components'
import { Form } from 'Components/forms'

const useGetNamePopupStyles = makeStyles({
  popup: {
    width: 436,
		padding: '56px 32px 31px 32px',
	},
	form: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  input: {
    width: 'calc(100% - 64px)',
  },
  button: {
    marginTop: 47,
  },
  actions: {
    width: 436,
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginTop: -36,
    marginRight: -24,
  },
})

const GetNamePopup = props => {
  const c = useGetNamePopupStyles()
  const [name, setName] = useState(props.defaultValue)
  const t = hooks.usePolyglot()
  const onSubmit = useCallback(() => {
    props.onContinue(name)
  }, [props.onContinue, name])
  return (
    <popups.Popup className={c.popup}>
      <Form onSubmit={onSubmit} className={c.form}>
        <div className={c.actions}>
          <buttons.ButtonClose onClick={props.onClose} size={20} />
        </div>
        <inputs.Input
          value={name}
          label={l.firstUpper(t('editor.survey_name'))}
          className={c.input}
          onChange={setName}
          autoFocus
        />
        <buttons.TextButtonFilled
          className={c.button}
          level={1}
          onClick={onSubmit}
          disabled={name.length === 0}
        >
          {l.firstUpper(t('editor.continue'))}
        </buttons.TextButtonFilled>
      </Form>
    </popups.Popup>
  )
}

const useStyles = makeStyles(theme => ({
  container: {
    flex: '1 0 auto',
    display: 'flex',
    overflow: 'hidden',
  },
  header: {
    flex: '0 0 232px',
    borderRight: `1px solid ${theme.colors.grey3}`,
    padding: '72px 32px',
    display: 'flex',
    flexDirection: 'column',

    '& > *': {
      margin: '8px 0',
    },
  },
  body: {
    display: 'flex',
    flexDirection: 'column',
    flex: '1 0 auto',
    width: `calc(100% - ${296 + 38 + 70 * 2}px)`,
    marginRight: 38,
    padding: '32px 70px',
    alignItems: 'center',
  },
  addNewQuestion: {
    margin: '48px 12px',
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    flex: '0 0 auto',

    '& > div': {
      height: 88,
      width: 88,
      minWidth: 88,
      borderRadius: 20,
      border: `1px solid ${theme.colors.grey3}`,
      position: 'relative',

      '& > div': {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',

        '& g': {
          fill: theme.colors.barbie,
        },
      },
    },

    '& > span': {
      ...theme.h3,
      margin: '0 27px',
    },
  },
  questionList: {
    borderTop: `1px solid ${theme.colors.grey3}`,
    borderBottom: `1px solid ${theme.colors.grey3}`,
    display: 'flex',
    flex: '0 1 auto',
    flexDirection: 'column',
    overflowY: 'auto',
    '& > *:last-child': {
      borderBottom: 'none',
    },
    minWidth: 400,
  },
  footer: {
    marginBottom: 40,
    flex: '0 0 auto',
    display: 'flex',
    justifyContent: 'flex-end',
    width: '100%',
    '& > *': {
      marginLeft: 12,
      marginRight: 12,
    },
  },
  saveAsSurvey: {},
  hidden: {
    opacity: 0,
  },
}))

const Editor = ({ template, ...props }) => {
  const c = useStyles()
  const t = hooks.usePolyglot()
  const [survey, setSurvey] = useState({
    name: '',
    ...template,
    questions:
      template.questions instanceof Array
        ? template.questions.map(q => ({ ...q, id: uuid() }))
        : [],
  })

  const [forceSurveyName, setForceSurveyName] = useState(
    props.action === 'create'
  )
  const [focusedQuestion, __setFocusedQuestion] = useState(undefined)
  const [creatingQuestion, __setCreatingQuestion] = useState(false)

  const setFocusedQuestion = val => {
    if (val === undefined) __setFocusedQuestion(val)
    else if (!creatingQuestion) __setFocusedQuestion(val)
  }

  const setCreatingQuestion = val => {
    if (!val) __setCreatingQuestion(val)
    else if (focusedQuestion === undefined) __setCreatingQuestion(val)
  }

  const setName = name => setSurvey(s => ({ ...s, name }))
  const addQuestion = question =>
    setSurvey(s => ({
      ...s,
      questions: [
        ...s.questions,
        {
          ...question,
          id: uuid(),
        },
      ],
    }))

  const updateQuestion = question => {
    setSurvey({
      ...survey,
      questions: survey.questions.map(q =>
        q.id === question.id ? question : q
      ),
    })
  }

  const removeQuestion = question =>
    setSurvey({
      ...survey,
      questions: survey.questions.filter(q => q.id !== question.id),
    })

  const questionListRef = useRef()
  const scrollTo = useSmoothScroll('y', questionListRef)

  useEffect(() => {
    scrollTo(questionListRef.current.scrollHeight)
  }, [creatingQuestion, scrollTo])

  const createSurvey = hooks.useCreateSurvey()
  const createTemplate = hooks.useCreateTemplate()
  const updateTemplate = hooks.useUpdateTemplate()

  const handleSubmit = () => {
    switch (props.action) {
      case 'create':
        createSurvey(survey)
        break
      case 'edit':
        if (template.id === undefined) {
          createTemplate(survey)
        } else {
          updateTemplate({
            ...survey,
            id: template.id,
          })
        }
        props.history.goBack()
        break
      default:
        console.error('unknown editor action')
    }
  }

  const wasBeeingCreated = useRef({ current: false })
  const surveys = hooks.useSurveys()

  if (surveys.creating === true) wasBeeingCreated.current = true

  useEffect(() => {
    if (
      surveys.creating === false &&
      surveys.creatingError === undefined &&
      wasBeeingCreated.current === true
    ) {
      props.history.push('/results/' + surveys.lastCreatedId, {
        showHelp: true,
      })
    }
  })

  return (
    <div className={c.container}>
      <div className={c.header}>
        <inputs.Input
          value={survey.name}
          onChange={setName}
          label={l.firstUpper(t('editor.survey_name'))}
        />
        {props === 'create' && (
          <buttons.SaveAsTemplateButton
            className={c.saveAsSurvey}
            onClick={() => {
              createTemplate(survey)
            }}
          />
        )}
      </div>
      <div className={c.body}>
        <div
          className={c.addNewQuestion}
          onClick={() => setCreatingQuestion(true)}
        >
          <div>
            <icons.IconPlus />
          </div>
          <span>{l.firstUpper(t('editor.add_new_question'))}</span>
        </div>
        <div
          className={`${c.questionList} ${
            survey.questions.length > 0 ? '' : c.hidden
          }`}
          ref={questionListRef}
        >
          {survey.questions.map(q => (
            <Question
              key={q.id}
              question={q}
              onEdit={() => setFocusedQuestion(q.id)}
              onDelete={() => removeQuestion(q)}
            />
          ))}
        </div>
        <div style={{ flex: '1 1 auto' }} />
        <div className={c.footer}>
          <buttons.TextButtonBorder
            level={1}
            onClick={() => props.history.goBack()}
          >
            {l.firstUpper(t('editor.cancel'))}
          </buttons.TextButtonBorder>
          <buttons.TextButtonFilled
            level={1}
            onClick={handleSubmit}
            disabled={survey.questions.length === 0}
          >
            {(() => {
              switch (props.action) {
                case 'create':
                  return l.firstUpper(t('editor.start'))
                case 'edit':
                  return l.firstUpper(t('editor.save'))
                default:
                  console.error('uknown editor action')
              }
            })()}
          </buttons.TextButtonFilled>
        </div>
        {focusedQuestion && (
          <EditQuestionPopup
            question={survey.questions.find(q => q.id === focusedQuestion)}
            onClose={() => setFocusedQuestion(undefined)}
            onSave={q => {
              updateQuestion(q)
              setFocusedQuestion(undefined)
            }}
          />
        )}
        {creatingQuestion && (
          <EditQuestionPopup
            question={{}}
            onClose={() => setCreatingQuestion(false)}
            onSave={q => {
              addQuestion(q)
              setCreatingQuestion(false)
            }}
          />
        )}
        {forceSurveyName && (
          <GetNamePopup
            defaultValue={survey.name}
            onClose={() => props.history.goBack()}
            onContinue={name => {
              setName(name)
              setForceSurveyName(false)
            }}
          />
        )}
      </div>
    </div>
  )
}

export default withRouter(Editor)
