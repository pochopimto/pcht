import React from 'react'
import { makeStyles } from '@material-ui/core'

import { lists, buttons } from 'Components'
import { useDeleteTemplate } from 'Hooks'
import { navigateTo } from 'Routes'

type Props = {
  template: any // TODO ITemplate
  predefined?: boolean
}

const useStyles = makeStyles(theme => ({
  templateName: {
    ...theme.h4,
  },
  templateActionList: {
    display: 'flex',
  },
  templateActions: {
    margin: '0px 12px',
  },
}))

const Template = ({ template, predefined = false }: Props) => {
  const c = useStyles({})
  const deleteTemplate = useDeleteTemplate()

  const onDelete = (e: Event) => {
    e.stopPropagation()
    deleteTemplate(template.id)
  }

  const onEdit = (e: Event) => {
    e.stopPropagation()
    navigateTo(`templates/edit-template/id/${template.id}`)
  }

  const onStart = (e: Event) => {
    e.stopPropagation()
    if (predefined) {
      console.log(template)
      navigateTo(`templates/create-survey/predefined/${template.key}`)
    } else {
      navigateTo(`templates/create-survey/id/${template.id}`)
    }
  }

  return (
    <lists.ListItem onClick={onStart}>
      <div>
        <span className={c.templateName}>{template ? template.name : ''}</span>
      </div>
      <div className={c.templateActionList}>
        {!predefined && (
          <>
            <buttons.ButtonDelete
              className={c.templateActions}
              size={24}
              onClick={onDelete}
            />
            <buttons.ButtonEdit
              className={c.templateActions}
              size={24}
              onClick={onEdit}
            />
          </>
        )}
        <buttons.FancyLinkStart
          className={c.templateActions}
          size={24}
          onClick={onStart}
        />
      </div>
    </lists.ListItem>
  )
}

export default Template
