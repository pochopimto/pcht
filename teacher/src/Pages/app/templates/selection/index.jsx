import React from 'react'
import { withRouter } from 'react-router-dom'
import { Button, makeStyles, Typography } from '@material-ui/core'

import { PlusIcon } from 'Components/icons'
import { Loader } from 'Components/loaders'
import * as hooks from 'Hooks'
import l from 'Lib'
import { navigateTo } from 'Routes'

import Template from './Template'

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flex: '1 0 auto',
    flexDirection: 'column',
    padding: '87px 64px 32px 72px',
  },
  header: {
    display: 'flex',
  },
  templatesList: {
    flex: '0 1 auto',
    display: 'flex',
    flexDirection: 'column',
    marginTop: theme.spacing(4),
    overflow: 'auto',
  },
  noTemplates: {
    ...theme.h3,
    marginTop: 64,
  },
  heading: {
    marginTop: theme.spacing(6),
  },
  expanded: {
    flexGrow: 1,
  },
}))

const Selection = (props) => {
  const c = useStyles()
  const t = hooks.usePolyglot()
  const predefinedTemplates = hooks.usePredefinedTemplates()
  const templates = hooks.useTemplates()
  const template_ids = Object.keys(templates.data).sort(
    (a, b) => templates.data[b].date - templates.data[a].date
  )

  return (
    <div className={c.container}>
      <div className={c.header}>
        <Button
          size="large"
          variant="contained"
          color="secondary"
          endIcon={<PlusIcon />}
          onClick={() =>
            navigateTo(props.match.path + '/create-survey/predefined/blank')
          }
        >
          {t('templates.create_new_survey')}
        </Button>
      </div>
      {predefinedTemplates.length > 0 && (
        <>
          <Typography variant="h4" className={c.heading}>
            {t('templates.our_templates')}
          </Typography>
          <div className={c.templatesList}>
            {predefinedTemplates.map((t) => (
              <Template key={t.key} template={t} predefined />
            ))}
          </div>
        </>
      )}
      {!templates.upToDate || !templates.lastRetrieved ? (
        <div className={c.expanded}>
          <Loader />
        </div>
      ) : template_ids.length > 0 ? (
        <>
          <Typography variant="h4" className={c.heading}>
            {t('templates.your_templates')}
          </Typography>
          <div className={c.templatesList}>
            {template_ids.map((id) => (
              <Template key={id} template={templates.data[id]} />
            ))}
          </div>
        </>
      ) : (
        Object.keys(predefinedTemplates).length === 0 && (
          <h3 className={c.noTemplates}>
            {l.firstUpper(t('templates.no_templates'))}
          </h3>
        )
      )}
    </div>
  )
}

export default withRouter(Selection)
