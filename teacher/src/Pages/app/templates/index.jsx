import React from 'react'
import { withRouter, Switch, Route } from 'react-router-dom'

import Selection from './selection'
import CreateSurvey from './create-survey'
import EditTemplate from './edit-template'

const Templates = props => {
  return (
    <Switch>
      <Route exact path={props.match.path} component={Selection} />
      <Route
        path={props.match.path + '/create-survey'}
        component={CreateSurvey}
      />
      <Route
        path={props.match.path + '/edit-template'}
        component={EditTemplate}
      />
    </Switch>
  )
}

export default withRouter(Templates)
