import gen_Page from './editor-launcher'

const CreateSurvey = gen_Page('create')
CreateSurvey.displayName = 'CreateSurvey'

export default CreateSurvey
