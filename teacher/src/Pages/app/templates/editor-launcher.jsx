import React, { useState, useMemo } from 'react'
import { withRouter, Switch, Route } from 'react-router-dom'

import * as hooks from 'Hooks'
import l from 'Lib'
import Error from 'Pages/error'
import { Loader } from 'Components/loaders'

import Editor from './editor'
import { usePredefinedTemplates } from 'Hooks'

const fromId = action =>
  function FromId(props) {
    const template = hooks.useTemplate(props.match.params.id)
    const loading = template.retrieving || template.error || !template.upToDate
    const [loaded, setLoaded] = useState(false)
    if (!loaded && !loading) setLoaded(true)
    const template_data = useMemo(() => template.data, [template.data])

    if (!loaded) return <Loader />
    else return <Editor template={template_data} action={action} />
  }

const fromPredefined = action =>
  function FromPredefined(props) {
    const predefinedTemplates = usePredefinedTemplates()

    const templates = {
      blank: {
        name: '',
      },
      ...predefinedTemplates.reduce((obj, t) => ({ ...obj, [t.key]: t }), {}),
    }
    console.log('templates', templates)

    const template = useMemo(() => templates[props.match.params.id], [
      props.match.params.id,
      templates,
    ])

    if (templates[props.match.params.id] === undefined) {
      return <Error />
    } else {
      return <Editor template={template} action={action} />
    }
  }

const gen_Page = action =>
  withRouter(function EditorLauncher(props) {
    return (
      <Switch>
        <Route path={props.match.path + '/id/:id'} component={fromId(action)} />
        <Route
          path={props.match.path + '/predefined/:id'}
          component={fromPredefined(action)}
        />
      </Switch>
    )
  })

export default gen_Page
