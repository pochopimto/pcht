import gen_Page from './editor-launcher'

const EditTemplate = gen_Page('edit')
EditTemplate.displayName = 'EditTemplate'

export default EditTemplate
