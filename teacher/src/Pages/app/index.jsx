import React from 'react'
import { makeStyles } from '@material-ui/core'

import { withRouter, Switch, Route, Redirect } from 'react-router-dom'

import Sidebar from './sidebar'
import Templates from './templates'
import Results from './results'
import TrafficLights from './trafficLights'
import Test from 'Pages/test'
import Error from 'Pages/error'

import Intro from 'Components/Intro'

const useStyles = makeStyles({
  container: {
    display: 'flex',
    flexDirection: 'row',

    height: '100%',
    width: '100%',
  },
  contentContainer: {
    flex: '1 1 auto',
    display: 'flex',
  },
})

const App = () => {
  const c = useStyles()
  return (
    <div className={c.container}>
      <Sidebar />
      <div className={c.contentContainer}>
        <Switch>
          <Redirect from={'/'} exact to={'/templates'} />
          <Route path="/templates" component={Templates} />
          <Route path="/results" component={Results} />
          <Route path="/traffic-lights" component={TrafficLights} />
          <Route path="/test" component={Test} />
          <Route component={Error} />
        </Switch>
      </div>
      <Intro />
    </div>
  )
}

export default withRouter(App)
