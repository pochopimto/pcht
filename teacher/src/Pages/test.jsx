/* eslint-disable no-unused-vars */
import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Paper, Button, Container, IconButton } from '@material-ui/core'

import { buttons, icons } from 'Components'

const Test = () => {
  return (
    <Container maxWidth="xs">
      <h1> Test </h1>
      <Button
        color="default"
        containerElement={<Link to="/templates" />}
        variant="contained"
      >
        Default
      </Button>
      <Button color="primary" variant="contained">
        primary
      </Button>
      <Button color="secondary" variant="contained">
        secondary
      </Button>
    </Container>
  )
}

export default Test
