import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core'
import { withRouter, Redirect } from 'react-router-dom'

import Button from 'Components/Button'
import * as hooks from 'Hooks'
import l from 'Lib'
import { inputs, icons, buttons } from 'Components'
import { Form } from 'Components/forms'

const useStyles = makeStyles(theme => ({
  container: {
    boxSizing: 'border-box',
    padding: 64,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    overflowY: 'auto',
    height: '100%',
  },
  logoContainer: {
    display: 'flex',
    width: '100%',
  },
  heading: {
    marginTop: 23,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  heading1: {
    ...theme.h3,
  },
  heading2: {
    ...theme.h4,
    marginTop: 12,
    color: theme.colors.purple,
  },
  form: {
    marginTop: 72,
    maxWidth: 320,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  passwordInput: {
    marginTop: 12,
  },
  loginButton: {
    marginTop: 40,
    textTransform: 'none',
  },
  bottomStuff: {
    marginTop: 16,
    '& > div': {
      margin: '3px 0',
      '& > span': {
        marginRight: 6,
      },
    },
  },
}))

const Page = props => {
  const c = useStyles(props)
  const t = hooks.usePolyglot()
  const [loginApi, setLoginApi] = useState({
    done: false,
    pending: false,
    error: undefined,
  })

  let email_error, password_error
  let global_error

  if (loginApi.error) {
    if (loginApi.error.response !== undefined) {
      ;(loginApi.error.response.data.non_field_errors || []).forEach(e => {
        switch (e) {
          case 'Unable to log in with provided credentials.':
            global_error = l.firstUpper(t('login.bad_credentials'))
            break
          default:
            console.error('Error now known:', e)
        }
      })
    }
  }

  const login = hooks.useLogin(setLoginApi)
  const [email, __setEmail] = useState('')
  const setEmail = email => __setEmail(email.toLowerCase())

  const [password, setPassword] = useState('')

  const onSubmit = e => {
    e.preventDefault()
    login(email, password)
  }

  const { from } = props.location.state || { from: { pathname: '/' } }
  if (loginApi.done) return <Redirect to={from} />

  return (
    <div className={c.container}>
      <div className={c.logoContainer}>
        <icons.IconLogo />
      </div>
      <div className={c.heading}>
        <div className={c.heading1}>
          {l.firstUpper(t('login.welcome_back'))}
        </div>
        <div className={c.heading2}>{t('login.login-ing').toUpperCase()}</div>
      </div>
      <Form className={c.form} onSubmit={onSubmit}>
        <inputs.Input
          type="text"
          label={l.firstUpper(t('login.email'))}
          value={email}
          onChange={setEmail}
          message={email_error || global_error}
          error={email_error || global_error}
          autoFocus
          required
        />
        <inputs.Input
          className={c.passwordInput}
          type="password"
          label={l.firstUpper(t('login.password'))}
          value={password}
          onChange={setPassword}
          message={password_error || global_error}
          error={password_error || global_error}
          required
        />
        <Button
          className={c.loginButton}
          onClick={onSubmit}
          variant="contained"
          color="primary"
          size="large"
          loading={loginApi.pending}
        >
          {l.firstUpper(t('login.login'))}
        </Button>
      </Form>
      <div className={c.bottomStuff}>
        <div className={c.registerStuff}>
          <span>{l.firstUpper(t('login.you_dont_have_an_account'))}</span>
          <buttons.Link
            to={{ pathname: '/register', state: { from } }}
            color={1}
          >
            {l.firstUpper(t('login.register'))}
          </buttons.Link>
        </div>
        {/* TODO: <div className={c.recoveryStuff}>
          <span>
            { l.firstUpper(t('login.you_forgot')) }
          </span>
          <buttons.Link
            to={'/recover-password'}
            color={1}
          >
            { l.firstUpper(t('login.recover')) }
          </buttons.Link>
        </div> */}
        <div className={c.needHelp}>
          <span>{l.firstUpper(t('login.need_help'))}</span>
          <buttons.Link href={'mailto:info@pochopimto.cz'} color={1} newTab>
            {l.firstUpper(t('login.get_in_touch'))}
          </buttons.Link>
        </div>
      </div>
    </div>
  )
}

export default withRouter(Page)
