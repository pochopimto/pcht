import React, { useState } from 'react'
import { makeStyles, FormControl, FormGroup } from '@material-ui/core'
import { Redirect } from 'react-router-dom'
import ReactGA from 'react-ga'

import Button from 'Components/Button'
import * as hooks from 'Hooks'
import l from 'Lib'
import { icons, buttons, inputs } from 'Components'
import { Form } from 'Components/forms'
import { Checkbox } from 'Components/forms/form-control'
import { notify } from 'Components/notifications'

const useStyles = makeStyles(theme => ({
  container: {
    boxSizing: 'border-box',
    padding: 64,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    overflowY: 'auto',
    height: '100%',
  },
  logoContainer: {
    display: 'flex',
    width: '100%',
  },
  heading: {
    marginTop: 23,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  heading1: {
    ...theme.h3,
  },
  heading2: {
    ...theme.h4,
    marginTop: 12,
    color: theme.colors.barbie,
  },
  form: {
    marginTop: 60,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    maxWidth: 320 * 2 + 32,
  },
  registerButton: {
    marginTop: 40,
    maxWidth: 320,
    width: '100%',
    textTransform: 'none',
  },
  bottomStuff: {
    color: theme.colors.darkpurple,
    marginTop: 16,
  },
  inputs: {
    display: 'grid',
    gridTemplateColumns: 'repeat(auto-fill, 320px)',
    rowGap: '12px',
    columnGap: '32px',
    width: '100%',
    justifyContent: 'center',
    '& > *': {
      flex: '0 1 320px',
    },
  },
  tickBoxes: {
    color: theme.colors.darkpurple,
    marginTop: 126,
    display: 'flex',
    flexDirection: 'column',
  },
  textBottom: {
    maxWidth: 400,
    margin: '3px 0',
    '& > span': {
      marginRight: 6,
    },
  },
  tickLine: {
    display: 'flex',
    alignItems: 'center',
  },
  helperText: {
    marginLeft: 42,
  },
  fonrControl: {
    display: 'flex',
  },
}))

const Register = props => {
  const c = useStyles()

  const [email, __setEmail] = useState('')
  const setEmail = email => __setEmail(email.toLowerCase())

  const [password, setPassword] = useState('')
  const [schoolId, setSchoolId] = useState(undefined)
  const [name, setName] = useState('')
  const [termsAndConditions, setTermsAndConditions] = useState(false)
  const [sendNewsletter, setSendNewsletter] = useState(false)

  const t = hooks.usePolyglot()
  const schools = hooks.useSchools()

  const [registerApi, setRegisterApi] = useState({
    done: false,
    pending: false,
    error: undefined,
  })

  const clearError = field => {
    setRegisterApi(a => {
      ;(((a.error || {}).response || {}).data || {})[field] = undefined
      return a
    })
  }

  const addError = (field, msg) => {
    setRegisterApi(a => ({
      ...a,
      error: {
        ...(a.error || {}),
        [field]: [
          ...((((a.error || {}).response || {}).data || {})[field] || []),
          msg,
        ],
      },
    }))
  }

  const register = hooks.useRegister(setRegisterApi)

  let email_error, password_error, schoolId_error, name_error
  let global_error

  if (registerApi.error) {
    ;(registerApi.error.email || []).forEach(e => {
      switch (e) {
        case 'Email is not unique.':
          email_error = l.firstUpper(t('register.email_taken'))
          break
        case 'This field may not be blank.':
          email_error = t('register.email_required')
          break
        default:
          email_error = e
          console.error('Unknown error:', e)
      }
    })
    ;(registerApi.error.name || []).forEach(e => {
      switch (e) {
        case 'This field may not be blank.':
          name_error = l.firstUpper(t('register.name_required'))
          break
        default:
          name_error = e
          console.error('Unknown error:', e)
      }
    })
    ;(registerApi.error.school || []).forEach(e => {
      switch (e) {
        case 'This field may not be blank.':
          schoolId_error = l.firstUpper(t('register.school_required'))
          break
        default:
          schoolId_error = e
          console.error('Unknown error:', e)
      }
    })
    ;(registerApi.error.password || []).forEach(e => {
      switch (e) {
        case 'This field may not be blank.':
          password_error = t('register.password_required')
          break
        default:
          password_error = e
          console.error('Unknown error:', e)
      }
    })
    ;(registerApi.error.non_field_error || []).forEach(e => {
      switch (e) {
        default:
          notify(e)
      }
    })
  }

  const onSubmit = () => {
    if (schoolId === undefined) {
      addError('school', 'This field may not be blank.')
      return
    }
    if (!termsAndConditions) {
      notify(t('register.must_agree'), 'error')
      return
    }

    register({
      email,
      password,
      name,
      schoolId: schoolId === 'noSchool' ? undefined : schoolId,
      termsAndConditions,
      sendNewsletter,
    })
  }

  const { from } = props.location.state || { from: { pathname: '/' } }
  if (registerApi.done)
    return <Redirect to={{ pathname: '/login', state: { from } }} />

  return (
    <div className={c.container}>
      <div className={c.logoContainer}>
        <icons.IconLogo />
      </div>
      <div className={c.heading}>
        <div className={c.heading1}>{l.firstUpper(t('register.join_us'))}</div>
        <div className={c.heading2}>
          {t('register.registration').toUpperCase()}
        </div>
      </div>
      <Form className={c.form} onSubmit={onSubmit}>
        <div className={c.inputs}>
          <inputs.Select
            label={l.firstUpper(t('register.school_name'))}
            value={schoolId}
            options={Object.values(schools.data).map(s => ({
              id: s.id,
              label: s.name,
            }))}
            alwaysOptions={[
              {
                label: l.firstUpper(t('register.without_school')),
                id: 'noSchool',
              },
            ]}
            onChange={a => {
              setSchoolId(a)
              clearError('school')
            }}
            message={schoolId_error || global_error}
            error={Boolean(schoolId_error || global_error)}
            autoFocus
            required
          />
          <inputs.Input
            type="text"
            label={l.firstUpper(t('register.name'))}
            autocomplete={'name'}
            value={name}
            onChange={a => {
              setName(a)
              clearError('name')
            }}
            message={name_error || global_error}
            error={Boolean(name_error || global_error)}
            required
          />
          <inputs.Input
            type="text"
            label={l.firstUpper(t('register.email'))}
            autocomplete={'email'}
            value={email}
            onChange={a => {
              setEmail(a)
              clearError('email')
            }}
            message={email_error || global_error}
            error={Boolean(email_error || global_error)}
            required
          />
          <inputs.Input
            type="password"
            label={l.firstUpper(t('register.password'))}
            autocomplete={'password'}
            value={password}
            onChange={setPassword}
            message={password_error || global_error}
            error={Boolean(password_error || global_error)}
            required
          />
        </div>
        <div className={c.tickBoxes}>
          <FormControl className={c.formControl}>
            <FormGroup className={c.newsletter}>
              <div className={c.tickLine}>
                <Checkbox
                  className={c.checkbox}
                  value={sendNewsletter}
                  onChange={setSendNewsletter}
                  label={l.firstUpper(t('register.get_news'))}
                />
              </div>
            </FormGroup>
          </FormControl>
          <FormControl className={c.formControl} error={!termsAndConditions}>
            <FormGroup className={c.terms}>
              <div className={c.tickLine}>
                <Checkbox
                  className={c.checkbox}
                  checked={termsAndConditions}
                  onChange={setTermsAndConditions}
                  label={
                    <>
                      {l.firstUpper(t('register.agree_to'))}{' '}
                      <buttons.Link
                        href={
                          'https://www.pochopimto.cz/static/landing-page/gdpr.pdf'
                        }
                      >
                        {l.firstUpper(t('register.terms_and_conditions'))}
                      </buttons.Link>
                      {' *'}
                    </>
                  }
                  error={
                    termsAndConditions
                      ? []
                      : [`* ${t('register.this_is_required')}`]
                  }
                />
              </div>
            </FormGroup>
          </FormControl>
        </div>
        <Button
          className={c.registerButton}
          onClick={e => {
            e.preventDefault()
            onSubmit()
            ReactGA.event({
              category: 'onBoarding',
              action: 'Register clicked',
              label: 'Register',
            })
          }}
          variant="contained"
          color="secondary"
          size="large"
          loading={registerApi.pending}
        >
          {l.firstUpper(t('register.register'))}
        </Button>
      </Form>
      <div className={c.bottomStuff}>
        <div className={`${c.loginStuff} ${c.textBottom}`}>
          <span>{l.firstUpper(t('register.you_have_an_account'))}</span>
          <buttons.Link to={{ pathname: '/login', state: { from } }} color={0}>
            {l.firstUpper(t('register.login'))}
          </buttons.Link>
        </div>
      </div>
    </div>
  )
}

export default Register
