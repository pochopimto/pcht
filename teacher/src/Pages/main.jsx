import React from 'react'
import { makeStyles } from '@material-ui/core'
import { withRouter, Switch, Route, Redirect } from 'react-router-dom'
import { useSnackbar } from 'notistack'

import * as hooks from 'Hooks'
import { notificationProvider } from 'Components/notifications'

import Error from './error'
import Login from './login'
import Register from './register'
import AppPage from './app'

const useStyles = makeStyles(theme => ({
  container: {
    height: '100%',
    width: '100%',
    fontFamily: theme.fontFamily,
    '& a': {
      textDecoration: 'none',
    },
  },
}))

const Main = () => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar()
  notificationProvider.enqueueSnackbar = enqueueSnackbar
  notificationProvider.closeSnackbar = closeSnackbar

  const c = useStyles()
  const auth = hooks.useAuth()
  const logout = hooks.useLogout()
  return (
    <div className={c.container}>
      <Switch>
        <Route path={'/login'} component={Login} />
        <Route path={'/register'} component={Register} />
        <Route
          path="/logout"
          render={() => {
            logout()
            return <Redirect to={'/'} />
          }}
        />
        <Route
          render={() =>
            auth.loggedIn ? (
              <AppPage />
            ) : (
              <Route
                render={props => (
                  <Redirect
                    to={{
                      pathname: '/login',
                      state: { from: props.location },
                    }}
                  />
                )}
              />
            )
          }
        />
        <Route component={Error} />
      </Switch>
    </div>
  )
}

export default withRouter(Main)
