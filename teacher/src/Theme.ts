import { Theme } from '@material-ui/core/styles/createMuiTheme' // eslint-disable-line no-unused-vars

declare module '@material-ui/core/styles/createMuiTheme' {
  interface Theme {
    /* These are obsolete */
    fontFamily: typeof theme.fontFamily
    colors: typeof theme.colors
    h1: typeof theme.h1
    h2: typeof theme.h2
    h3: typeof theme.h3
    h4: typeof theme.h4
    h5: typeof theme.h5
    p: typeof theme.p
    hint: typeof theme.hint
    transition: (prop: string) => string
    /* End of obsolote properties */

    /* Use this instead */
    pcht: {}
  }

  interface ThemeOptions {
    fontFamily?: typeof theme.fontFamily
    colors?: typeof theme.colors
    h1?: typeof theme.h1
    h2?: typeof theme.h2
    h3?: typeof theme.h3
    h4?: typeof theme.h4
    h5?: typeof theme.h5
    p?: typeof theme.p
    hint?: typeof theme.hint
    transition?: (prop: string) => string
  }
}

export const theme = {
  breakpoints: {
    keys: ['xs', 'sm', 'md', 'lg', 'xl'],
    values: { xs: 0, sm: 600, md: 960, lg: 1280, xl: 1920 },
  },
  direction: 'ltr',
  mixins: {
    toolbar: {
      minHeight: 56,
      '@media (min-width:0px) and (orientation: landscape)': { minHeight: 48 },
      '@media (min-width:600px)': { minHeight: 64 },
    },
  },
  overrides: {},
  palette: {
    common: { black: '#000', white: '#fff' },
    type: 'light',
    primary: {
      //light: '#F5ECFF',
      main: '#A452FF',
      //dark: '#A452FF',
      contrastText: '#fff',
    },
    secondary: {
      //light: '#FFE0F3',
      main: '#FF0099',
      //dark: '#FF0099',
      contrastText: '#fff',
    },
    error: {
      light: '#e57373',
      main: '#f44336',
      dark: '#d32f2f',
      contrastText: '#fff',
    },
    grey: {
      '50': '#fafafa',
      '100': '#f5f5f5',
      '200': '#eeeeee',
      '300': '#e0e0e0',
      '400': '#bdbdbd',
      '500': '#9e9e9e',
      '600': '#757575',
      '700': '#616161',
      '800': '#424242',
      '900': '#212121',
      A100: '#d5d5d5',
      A200: '#aaaaaa',
      A400: '#303030',
      A700: '#616161',
    },
    contrastThreshold: 3,
    tonalOffset: 0.2,
    text: {
      primary: 'rgba(0, 0, 0, 0.87)',
      secondary: 'rgba(0, 0, 0, 0.54)',
      disabled: 'rgba(0, 0, 0, 0.38)',
      hint: 'rgba(0, 0, 0, 0.38)',
    },
    divider: 'rgba(0, 0, 0, 0.12)',
    background: { paper: '#fff', default: '#fafafa' },
    action: {
      active: 'rgba(0, 0, 0, 0.54)',
      hover: 'rgba(0, 0, 0, 0.08)',
      hoverOpacity: 0.08,
      selected: 'rgba(0, 0, 0, 0.14)',
      disabled: 'rgba(0, 0, 0, 0.26)',
      disabledBackground: 'rgba(0, 0, 0, 0.12)',
    },
  },
  props: {},
  shadows: [
    'none',
    '0px 2px 1px -1px rgba(0,0,0,0.2),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 3px 0px rgba(0,0,0,0.12)',
    '0px 3px 1px -2px rgba(0,0,0,0.2),0px 2px 2px 0px rgba(0,0,0,0.14),0px 1px 5px 0px rgba(0,0,0,0.12)',
    '0px 3px 3px -2px rgba(0,0,0,0.2),0px 3px 4px 0px rgba(0,0,0,0.14),0px 1px 8px 0px rgba(0,0,0,0.12)',
    '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)',
    '0px 3px 5px -1px rgba(0,0,0,0.2),0px 5px 8px 0px rgba(0,0,0,0.14),0px 1px 14px 0px rgba(0,0,0,0.12)',
    '0px 3px 5px -1px rgba(0,0,0,0.2),0px 6px 10px 0px rgba(0,0,0,0.14),0px 1px 18px 0px rgba(0,0,0,0.12)',
    '0px 4px 5px -2px rgba(0,0,0,0.2),0px 7px 10px 1px rgba(0,0,0,0.14),0px 2px 16px 1px rgba(0,0,0,0.12)',
    '0px 5px 5px -3px rgba(0,0,0,0.2),0px 8px 10px 1px rgba(0,0,0,0.14),0px 3px 14px 2px rgba(0,0,0,0.12)',
    '0px 5px 6px -3px rgba(0,0,0,0.2),0px 9px 12px 1px rgba(0,0,0,0.14),0px 3px 16px 2px rgba(0,0,0,0.12)',
    '0px 6px 6px -3px rgba(0,0,0,0.2),0px 10px 14px 1px rgba(0,0,0,0.14),0px 4px 18px 3px rgba(0,0,0,0.12)',
    '0px 6px 7px -4px rgba(0,0,0,0.2),0px 11px 15px 1px rgba(0,0,0,0.14),0px 4px 20px 3px rgba(0,0,0,0.12)',
    '0px 7px 8px -4px rgba(0,0,0,0.2),0px 12px 17px 2px rgba(0,0,0,0.14),0px 5px 22px 4px rgba(0,0,0,0.12)',
    '0px 7px 8px -4px rgba(0,0,0,0.2),0px 13px 19px 2px rgba(0,0,0,0.14),0px 5px 24px 4px rgba(0,0,0,0.12)',
    '0px 7px 9px -4px rgba(0,0,0,0.2),0px 14px 21px 2px rgba(0,0,0,0.14),0px 5px 26px 4px rgba(0,0,0,0.12)',
    '0px 8px 9px -5px rgba(0,0,0,0.2),0px 15px 22px 2px rgba(0,0,0,0.14),0px 6px 28px 5px rgba(0,0,0,0.12)',
    '0px 8px 10px -5px rgba(0,0,0,0.2),0px 16px 24px 2px rgba(0,0,0,0.14),0px 6px 30px 5px rgba(0,0,0,0.12)',
    '0px 8px 11px -5px rgba(0,0,0,0.2),0px 17px 26px 2px rgba(0,0,0,0.14),0px 6px 32px 5px rgba(0,0,0,0.12)',
    '0px 9px 11px -5px rgba(0,0,0,0.2),0px 18px 28px 2px rgba(0,0,0,0.14),0px 7px 34px 6px rgba(0,0,0,0.12)',
    '0px 9px 12px -6px rgba(0,0,0,0.2),0px 19px 29px 2px rgba(0,0,0,0.14),0px 7px 36px 6px rgba(0,0,0,0.12)',
    '0px 10px 13px -6px rgba(0,0,0,0.2),0px 20px 31px 3px rgba(0,0,0,0.14),0px 8px 38px 7px rgba(0,0,0,0.12)',
    '0px 10px 13px -6px rgba(0,0,0,0.2),0px 21px 33px 3px rgba(0,0,0,0.14),0px 8px 40px 7px rgba(0,0,0,0.12)',
    '0px 10px 14px -6px rgba(0,0,0,0.2),0px 22px 35px 3px rgba(0,0,0,0.14),0px 8px 42px 7px rgba(0,0,0,0.12)',
    '0px 11px 14px -7px rgba(0,0,0,0.2),0px 23px 36px 3px rgba(0,0,0,0.14),0px 9px 44px 8px rgba(0,0,0,0.12)',
    '0px 11px 15px -7px rgba(0,0,0,0.2),0px 24px 38px 3px rgba(0,0,0,0.14),0px 9px 46px 8px rgba(0,0,0,0.12)',
  ],
  typography: {
    htmlFontSize: 16,
    fontFamily: "'Montserrat',sans-serif",
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 700,
    h1: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 300,
      fontSize: '6rem',
      lineHeight: 1,
      color: '#1E0059',
    },
    h2: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 300,
      fontSize: '3.75rem',
      lineHeight: 1,
      color: '#1E0059',
    },
    h3: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 400,
      fontSize: '3rem',
      lineHeight: 1.04,
      color: '#1E0059',
    },
    h4: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 400,
      fontSize: '2.125rem',
      lineHeight: 1.17,
      color: '#1E0059',
    },
    h5: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 400,
      fontSize: '1.5rem',
      lineHeight: 1.33,
      color: '#1E0059',
    },
    h6: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 500,
      fontSize: '1.25rem',
      lineHeight: 1.6,
      color: '#1E0059',
    },
    subtitle1: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 400,
      fontSize: '1rem',
      lineHeight: 1.75,
      color: '#1E0059',
    },
    subtitle2: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 500,
      fontSize: '0.875rem',
      lineHeight: 1.57,
      color: '#1E0059',
    },
    body1: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 400,
      fontSize: '1rem',
      lineHeight: 1.5,
      color: '#1E0059',
    },
    body2: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 400,
      fontSize: '0.875rem',
      lineHeight: 1.43,
    },
    button: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 500,
      fontSize: '0.875rem',
      lineHeight: 1.75,
      textTransform: 'uppercase',
    },
    caption: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 400,
      fontSize: '0.75rem',
      lineHeight: 1.66,
    },
    overline: {
      fontFamily: "'Montserrat',sans-serif",
      fontWeight: 400,
      fontSize: '0.75rem',
      lineHeight: 2.66,
      textTransform: 'uppercase',
    },
  },
  shape: { borderRadius: 6 },
  transitions: {
    easing: {
      easeInOut: 'cubic-bezier(0.4, 0, 0.2, 1)',
      easeOut: 'cubic-bezier(0.0, 0, 0.2, 1)',
      easeIn: 'cubic-bezier(0.4, 0, 1, 1)',
      sharp: 'cubic-bezier(0.4, 0, 0.6, 1)',
    },
    duration: {
      shortest: 150,
      shorter: 200,
      short: 250,
      standard: 300,
      complex: 375,
      enteringScreen: 225,
      leavingScreen: 195,
    },
  },
  zIndex: {
    mobileStepper: 1000,
    speedDial: 1050,
    appBar: 1100,
    drawer: 1200,
    modal: 1300,
    snackbar: 1400,
    tooltip: 1500,
  },
  fontFamily: "'Montserrat', sans-serif",
  colors: {
    barbie: '#FF0099',
    purple: '#A452FF',
    yellow: '#FFA300',
    bat: '#1F2532',
    lightbat: '#2D3444',
    lightyellow: '#FFF4E0',
    lightbarbie: '#FFE0F3',
    lightpurple: '#F5ECFF',
    darkpurple: '#1E0059',
    grey1: '#F1F1F1',
    grey2: '#E8E8E8',
    grey3: '#D2D2D2',
    grey4: '#939393',
    background: '#FAFAFA',
    white: '#FFFFFF',
    sidebarBackground: '#2D3444',
    error: '#FF001F',
    outline: {
      normal: '#D2D2D2',
      hover: '#939393',
      focused: '#A452FF',
      error: '#FF001F',
    },
  },
  h1: { fontWeight: 700, fontSize: 40, color: '#1E0059' },
  h2: { fontWeight: 700, fontSize: 30, color: '#1E0059' },
  h3: { fontWeight: 600, fontSize: 25, color: '#1E0059' },
  h4: { fontWeight: 600, fontSize: 18, color: '#1E0059' },
  h5: { fontWeight: 500, fontSize: 16, color: '#1E0059' },
  p: { fontWeight: 'normal', fontSize: 14, color: '#1E0059' },
  hint: { fontWeight: 'normal', fontSize: 13, color: '#939393' },
  transition: (prop: string) =>
    `${prop} ${theme.transitions.duration.standard} ${theme.transitions.easing.easeInOut}`,
}

export default (theme as unknown) as Theme
