const gen_type = (section, action) => ({
  start: `${section}/${action}/start`,
  ok: `${section}/${action}/ok`,
  bad: `${section}/${action}/bad`,
  retire: `${section}/${action}/retire`,
})

const gen_section = (section, actions) => {
  let ob = {}
  for (const action of actions) {
    ob[action] = gen_type(section, action)
  }
  return ob
}

const gen_types = sections => {
  let ob = {}
  for (const section in sections) {
    ob[section] = gen_section(section, sections[section])
  }
  return ob
}

const types = gen_types({
  auth: ['login', 'logout', 'register'],
  api: [
    'getCurrentUser',
    'getSurveys',
    'createSurvey',
    'deleteSurvey',
    'getTemplates',
    'createTemplate',
    'deleteTemplate',
    'updateTemplate',
    'getSchools',
    'createSchool',
    'deleteTextAnswer',
  ],
  locale: ['changeLocale'],
})

export default types
