import types from './types'

import { api } from '../Config'

export const login = (dispatcher, setApi) => (email, password) => {
  dispatcher({
    type: types.auth.login.start,
  })
  setApi(a => ({
    ...a,
    pending: true,
  }))

  api
    .post('/api/token', {
      username: email,
      password,
    })
    .then(response => {
      dispatcher({
        type: types.auth.login.ok,
        data: {
          token: response.data.token,
          email,
        },
      })
      setApi(a => ({
        ...a,
        pending: false,
        done: true,
        error: undefined,
      }))
    })
    .catch(error => {
      dispatcher({
        type: types.auth.login.bad,
        data: {
          error,
        },
      })
      setApi(a => ({
        ...a,
        pending: false,
        done: false,
        error,
      }))
    })
}

export const logout = dispatcher => () => {
  dispatcher({
    type: types.auth.logout.start,
  })

  dispatcher({
    type: types.auth.logout.ok,
  })
}
