import types from './types'
import * as auth from './auth'
import * as api from './api'

const actions = {
  types,
  auth,
  api,
}

export default actions
