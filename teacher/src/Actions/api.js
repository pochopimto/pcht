import { api } from 'Config'

import types from './types'

export const getCurrentUser = dispatcher => () => {
  dispatcher({
    type: types.api.getCurrentUser.start,
  })

  return api
    .get('/api/current_teacher', {})
    .then(response => {
      dispatcher({
        type: types.api.getCurrentUser.ok,
        data: response.data,
      })
    })
    .catch(error => {
      dispatcher({
        type: types.api.getCurrentUser.bad,
        data: {
          error,
        },
      })
      console.log({ error })
      throw error
    })
}

export const retireCurrentUser = dispatcher => () => {
  dispatcher({
    type: types.api.getCurrentUser.retire,
  })
}

export const getSurveys = dispatcher => () => {
  dispatcher({
    type: types.api.getSurveys.start,
  })

  return api
    .get('/api/my_surveys', {})
    .then(response => {
      dispatcher({
        type: types.api.getSurveys.ok,
        data: response.data,
      })
    })
    .catch(error => {
      dispatcher({
        type: types.api.getSurveys.bad,
        data: {
          error,
        },
      })
      console.log({ error })
      throw error
    })
}

export const retireSurveys = dispatcher => () => {
  dispatcher({
    type: types.api.getSurveys.retire,
  })
}

export const createSurvey = dispatcher => survey => {
  dispatcher({
    type: types.api.createSurvey.start,
    data: {
      survey,
    },
  })

  return api
    .post('/api/surveys', {
      teacher: survey.teacherId,
      questions: survey.questions,
      name: survey.name,
    })
    .then(response => {
      dispatcher({
        type: types.api.createSurvey.ok,
        data: {
          surveyId: response.data.id,
        },
      })
    })
    .catch(error => {
      dispatcher({
        type: types.api.createSurvey.bad,
        data: {
          error,
        },
      })
      console.log({ error })
      throw error
    })
}

export const deleteSurvey = dispatcher => surveyId => {
  dispatcher({
    type: types.api.deleteSurvey.start,
    data: {
      surveyId,
    },
  })

  return api
    .delete('/api/my_surveys/' + surveyId, {})
    .then(response => {
      dispatcher({
        type: types.api.deleteSurvey.ok,
        data: {
          surveyId,
        },
      })
    })
    .catch(error => {
      dispatcher({
        type: types.api.deleteSurvey.bad,
        data: {
          error,
          surveyId,
        },
      })
      console.log({ error })
      throw error
    })
}

export const deleteTextAnswer = dispatcher => answerId => {
  dispatcher({
    type: types.api.deleteTextAnswer.start,
    data: {
      answerId,
    },
  })

  return api
    .delete('/api/delete_answer/' + answerId, {})
    .then(response => {
      dispatcher({
        type: types.api.deleteTextAnswer.ok,
        data: {
          answerId,
        },
      })
    })
    .catch(error => {
      dispatcher({
        type: types.api.deleteTextAnswer.bad,
        data: {
          error,
          answerId,
        },
      })
      throw error
    })
}

export const getTemplates = dispatcher => () => {
  dispatcher({
    type: types.api.getTemplates.start,
  })

  return api
    .get('/api/my_templates', {})
    .then(response => {
      dispatcher({
        type: types.api.getTemplates.ok,
        data: response.data,
      })
    })
    .catch(error => {
      dispatcher({
        type: types.api.getTemplates.bad,
        data: {
          error,
        },
      })
      console.log({ error })
      throw error
    })
}

export const retireTemplates = dispatcher => () => {
  dispatcher({
    type: types.api.getTemplates.retire,
  })
}

export const createTemplate = dispatcher => template => {
  dispatcher({
    type: types.api.createTemplate.start,
  })

  return api
    .post('/api/my_templates', {
      teacher: template.teacherId,
      questions: template.questions,
      name: template.name,
    })
    .then(response => {
      dispatcher({
        type: types.api.createTemplate.ok,
        data: {
          templateId: response.data.id,
        },
      })
    })
    .catch(error => {
      dispatcher({
        type: types.api.createTemplate.bad,
        data: {
          error,
        },
      })
      console.log({ error })
      throw error
    })
}

export const deleteTemplate = dispatcher => templateId => {
  dispatcher({
    type: types.api.deleteTemplate.start,
    data: {
      templateId,
    },
  })

  return api
    .delete('/api/my_templates/' + templateId, {})
    .then(response => {
      dispatcher({
        type: types.api.deleteTemplate.ok,
        data: {
          templateId,
        },
      })
    })
    .catch(error => {
      dispatcher({
        type: types.api.deleteTemplate.bad,
        data: {
          error,
          templateId,
        },
      })
      console.log({ error })
      throw error
    })
}

export const updateTemplate = dispatcher => template => {
  dispatcher({
    type: types.api.createTemplate.start,
  })

  return api
    .put('/api/my_templates/' + template.id, {
      teacher: template.teacherId,
      questions: template.questions,
      name: template.name,
    })
    .then(response => {
      dispatcher({
        type: types.api.updateTemplate.ok,
        data: {
          templateId: response.data.id,
        },
      })
    })
    .catch(error => {
      dispatcher({
        type: types.api.updateTemplate.bad,
        data: {
          error,
        },
      })
      console.log({ error })
      throw error
    })
}

export const getSchools = dispatcher => () => {
  dispatcher({
    type: types.api.getSchools.start,
  })

  return api
    .get('/api/schools', {})
    .then(response => {
      dispatcher({
        type: types.api.getSchools.ok,
        data: response.data,
      })
    })
    .catch(error => {
      dispatcher({
        type: types.api.getSchools.bad,
        data: {
          error,
        },
      })
      console.log({ error })
      throw error
    })
}

export const createSchool = dispatcher => school => {
  dispatcher({
    type: types.api.createSchool.start,
    data: {
      school,
    },
  })

  return api
    .post('/api/schools', {
      name: school.name,
      address: school.address,
    })
    .then(response => {
      dispatcher({
        type: types.api.createSchool.ok,
        data: {
          schoolId: response.data.id,
          school,
        },
      })
    })
    .catch(error => {
      dispatcher({
        type: types.api.createSchool.bad,
        data: {
          error,
        },
      })
      console.log({ error })
      throw error
    })
}

export const retireSchools = dispatcher => () => {
  dispatcher({
    type: types.api.getSchools.retire,
  })
}
