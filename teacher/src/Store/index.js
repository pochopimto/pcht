import { createStore, applyMiddleware } from 'redux'
import { persistStore, persistReducer, createTransform } from 'redux-persist'
import { createMigrate } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { composeWithDevTools } from 'redux-devtools-extension'

import rootReducer from 'Reducers'
import { DEBUG, add_token, remove_token } from 'Config'

// Set to true if you want to debug redux-persist migration
const MIGRATION_DEBUG = false

// Mapping between redux-persist versions
const migrations = {
  0: () => ({}),
  1: state => ({
    ...state,
    api: {
      ...state.api,
      schools: {
        data: {},
        retrieving: false,
        lastRetrieved: undefined,
        upToDate: false,
        error: undefined,
        creating: false,
        creatingError: undefined,
        lastCreatedId: undefined,
      },
    },
  }),
}

const StopAllRequests = state => {
  return Object.keys(state).reduce((obj, key) => {
    obj[key] = state[key]
    if (obj[key].retrieving === true) obj[key].retrieving = false
    if (obj[key].creating === true) obj[key].creating = false
    return obj
  }, {})
}

const FinishAllPendingRequestsTransform = createTransform(
  inboundState => inboundState,
  outboundState => StopAllRequests(outboundState),
  { whitelist: 'api' }
)

const persistConfig = {
  key: 'root',
  version: 1,
  migrate: createMigrate(migrations, { debug: DEBUG && MIGRATION_DEBUG }),
  storage,
  transforms: [FinishAllPendingRequestsTransform],
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = createStore(
  persistedReducer,
  composeWithDevTools(applyMiddleware())
)

// This thing watches redux state for token changes and applies them to `api`
const change_token = () => {
  const state = store.getState()
  const token = state.auth.token
  if (token !== undefined) {
    add_token(token)
  } else {
    remove_token(token)
  }
}

store.subscribe(change_token)

export const persistor = persistStore(store)

export default store
