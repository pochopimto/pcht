import { useEffect, useState, useDebugValue } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import dayjs from 'dayjs'

import actions from 'Actions'
import { api } from 'Config'
import { REQUEST_RETRY_DELAY, TIMEOUTS } from 'Config'
import { notify } from 'Components/notifications'
import { usePolyglot, useRerender } from 'Hooks'

export const useData = (
  getData,
  fetchData,
  retireData,
  update_timeout = REQUEST_RETRY_DELAY,
  defaultValues = {}
) => {
  const dispatch = useDispatch()
  const data = useSelector(getData) || {}
  const currentTime = dayjs().valueOf()
  const logout = useLogout()
  const t = usePolyglot()
  const rerender = useRerender()
  const refresh_time =
    data.error || !data.lastRetrieved || !data.upToDate
      ? REQUEST_RETRY_DELAY
      : update_timeout
  const isOutDated =
    !data.lastRetrieved || data.lastRetrieved <= currentTime - refresh_time

  useEffect(() => {
    if (data.error && data.error.response) {
      if (data.error.response.status === 401) {
        logout()
      }
    }

    if (!data.retrieving) {
      if (!isOutDated) {
        const time = data.lastRetrieved + refresh_time - currentTime
        const pid = setTimeout(rerender, time)
        return () => clearTimeout(pid)
      } else {
        fetchData(dispatch).catch(error => {
          if (error.response) {
            notify(t('general_error'), 'error')
          } else if (error.isAxiosError) {
            // notify(t('notifications.no_internet_connection'), 'error')
          } else {
            notify(t('general_error'), 'error')
          }
        })
      }
    }
  }, [data, isOutDated])

  const out = {
    ...data,
    data: {
      ...defaultValues,
      ...data.data,
    },
  }
  useDebugValue(out)
  return out
}

export const useCurrentUser = () => {
  const currentUser = useData(
    state => state.api.currentUser,
    dispatch => actions.api.getCurrentUser(dispatch)(),
    dispatch => actions.api.retireCurrentUser(dispatch)(),
    TIMEOUTS.currentUser
  )
  return currentUser
}

export const useSurveys = () => {
  return useData(
    state => state.api.surveys,
    dispatch => actions.api.getSurveys(dispatch)(),
    dispatch => actions.api.retireSurveys(dispatch)(),
    TIMEOUTS.surveys
  )
}

export const useSurvey = id => {
  const survey = useSurveys()
  return {
    ...survey,
    data: {
      id: '',
      name: '',
      questions: [],
      answers: [],
      ...(survey.data && survey.data[id]),
    },
  }
}

export const useTemplates = () => {
  return useData(
    state => state.api.templates,
    dispatch => actions.api.getTemplates(dispatch)(),
    dispatch => actions.api.retireTemplates(dispatch)(),
    TIMEOUTS.templates
  )
}

export const useTemplate = id => {
  const template = useTemplates()
  return {
    ...template,
    data: {
      id: '',
      name: '',
      questions: [],
      ...(template.data && template.data[id]),
    },
  }
}

export const useCreateSurvey = () => {
  const dispatch = useDispatch()
  const createSurvey = actions.api.createSurvey(dispatch)
  const currentUser = useCurrentUser()
  return survey => {
    createSurvey({
      teacherId: currentUser.data.id,
      name: survey.name,
      questions: survey.questions.map(q => {
        return {
          question: q.question,
          type: q.type,
          ...(q.type === 'select' && {
            options: q.options.map(o => ({ text: o.text })),
          }),
        }
      }),
    })
  }
}

export const useDeleteSurvey = () => {
  const dispatch = useDispatch()
  const deleteSurvey = actions.api.deleteSurvey(dispatch)
  return surveyId => {
    deleteSurvey(surveyId)
  }
}

export const useCreateTemplate = () => {
  const dispatch = useDispatch()
  const createTemplate = actions.api.createTemplate(dispatch)
  const retireTemplate = actions.api.retireTemplates(dispatch)
  const currentUser = useCurrentUser()

  return async template => {
    await createTemplate({
      teacherId: currentUser.data.id,
      name: template.name,
      questions: template.questions.map(q => {
        return {
          question: q.question,
          type: q.type,
          ...(q.type === 'select' && {
            options: q.options.map(o => ({ text: o.text })),
          }),
        }
      }),
    })

    retireTemplate()
  }
}

export const useDeleteTemplate = () => {
  const dispatch = useDispatch()
  const deleteTemplate = actions.api.deleteTemplate(dispatch)
  return templateId => {
    deleteTemplate(templateId)
  }
}

export const useUpdateTemplate = () => {
  const dispatch = useDispatch()
  const updateTemplate = actions.api.updateTemplate(dispatch)
  const currentUser = useCurrentUser()
  return template => {
    updateTemplate({
      id: template.id,
      teacherId: currentUser.data.id,
      name: template.name,
      questions: template.questions.map(q => {
        return {
          question: q.question,
          type: q.type,
          ...(q.type === 'select' && {
            options: q.options.map(o => ({ text: o.text })),
          }),
        }
      }),
    })
  }
}

export const useSchools = () => {
  return useData(
    state => state.api.schools,
    dispatch => actions.api.getSchools(dispatch)(),
    dispatch => actions.api.retireSchools(dispatch)(),
    TIMEOUTS.schools
  )
}

export const useCreateSchool = () => {
  const dispatch = useDispatch()
  const createSchool = actions.api.createSchool(dispatch)
  return school => {
    createSchool({
      name: school.name,
      address: school.address,
    })
  }
}

export const useAuth = () => {
  const auth = useSelector(s => s.auth)
  useDebugValue(auth)
  return auth
}

export const useAction = (action, setApi) => {
  const dispatch = useDispatch()
  return action(dispatch, setApi)
}

export const useLogin = (setApi = () => {}) =>
  useAction(actions.auth.login, setApi)
export const useLogout = (setApi = () => {}) =>
  useAction(actions.auth.logout, setApi)

export const useRegister = (setApi = () => {}) => {
  const t = usePolyglot()
  const register = ({
    email,
    password,
    name,
    schoolId,
    termsAndConditions,
    sendNewsletter,
  }) => {
    setApi(a => ({
      ...a,
      pending: true,
      error: undefined,
    }))

    return api
      .post('/api/teachers', {
        email,
        password,
        name,
        school: schoolId,
        agreed_to_terms: termsAndConditions,
        subscribed_to_newsletter: sendNewsletter,
      })
      .then(() => {
        setApi(a => ({
          ...a,
          pending: false,
          done: true,
          error: undefined,
        }))
        notify(t('register.success'), 'success')
      })
      .catch(error => {
        if (error.response) {
          setApi(a => ({
            ...a,
            pending: false,
            done: false,
            error: error.response.data,
          }))
        }
        console.log({ error })
      })
  }

  return register
}

export const useCreateTrafficLights = () => {
  const currentUser = useCurrentUser()
  const [apiState, setApiState] = useState({
    loading: false,
    error: undefined,
    data: undefined,
    lastTry: 0,
  })
  const rerender = useRerender()

  const ready =
    apiState.lastTry < dayjs().valueOf() - (REQUEST_RETRY_DELAY - 10)

  if (currentUser.data !== undefined && !apiState.loading && ready) {
    setApiState(a => ({
      ...a,
      loading: true,
    }))
    api
      .post('/api/traffic-lights', {
        teacher: currentUser.data.id,
      })
      .then(response => {
        setApiState(a => ({
          ...a,
          loading: false,
          error: undefined,
          data: response.data,
          lastTry: dayjs().valueOf(),
        }))
      })
      .catch(error => {
        setApiState(a => ({
          ...a,
          loading: false,
          lastTry: dayjs().valueOf(),
          error,
        }))
        setTimeout(() => rerender(), REQUEST_RETRY_DELAY)
        console.log({ error })
      })
  }

  return apiState.data
}

export const useTrafficLights = id => {
  const [apiState, setApiState] = useState({
    loading: false,
    error: undefined,
    data: undefined,
    lastTry: 0,
  })

  const rerender = useRerender()

  const ready = apiState.lastTry < dayjs().valueOf() - TIMEOUTS.trafficLights

  if (!apiState.loading && !apiState.upToDate && ready) {
    setApiState(a => ({
      ...a,
      loading: true,
    }))
    api
      .get(`/api/traffic-lights/${id}`)
      .then(response => {
        setApiState(a => ({
          ...a,
          loading: false,
          error: undefined,
          data: response.data,
          upToDate: true,
          lastTry: dayjs().valueOf(),
        }))
        setTimeout(() => {
          setApiState(a => ({
            ...a,
            upToDate: false,
          }))
        }, TIMEOUTS.trafficLights)
      })
      .catch(error => {
        setApiState(a => ({
          ...a,
          loading: false,
          error,
          lastTry: dayjs().valueOf(),
        }))
        setTimeout(() => rerender(), TIMEOUTS.trafficLights)
        console.log({ error })
      })
  }

  return apiState.data
}

export { useTextAnswerDelete } from './answer'
