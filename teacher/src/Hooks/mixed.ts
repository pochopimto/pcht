import { useMemo, useCallback, useState } from 'react'
import Polyglot from 'node-polyglot'

import languages from 'Data/languages.json'
import predefined from 'Data/templates.json'
import { DEBUG } from 'Config'

const getLanguageCode = () => {
  if (DEBUG) return 'cs'

  const userLang = navigator.language || (navigator as any).userLanguage
  switch (userLang.substring(0, 2)) {
    case 'cs':
      return 'cs'
    default:
      return 'en'
  }
}

const polyglotLanguages = Object.keys(languages).reduce((obj, loc) => {
  obj[loc] = new Polyglot({ phrases: languages[loc], locale: loc })
  return obj
}, {})

export const usePolyglot = () => {
  const languageMap = useMemo(() => polyglotLanguages[getLanguageCode()], [
    getLanguageCode(),
  ])
  return useCallback((...args) => languageMap.t(...args), [languageMap])
}

export const usePredefinedTemplates = () => {
  const enabled = predefined.enabled
  const currentLangugage = predefined.templates[getLanguageCode()]
  const enabledTemplates = enabled
    .filter(t => t in currentLangugage)
    .map(t => ({ ...currentLangugage[t], key: t }))

  return enabledTemplates
}

export const useRerender = () => {
  const [, setState] = useState(true)
  const rerender = useCallback(() => setState(s => !s), [])
  return rerender
}
