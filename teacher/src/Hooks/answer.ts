import { useState } from 'react'
import { useDispatch } from 'react-redux'

import actions from 'Actions'

type ActionState = 'doing' | 'done' | 'error'

export const useTextAnswerDelete = () => {
  const [requestStates, setRequestStates] = useState<{
    [id: number]: ActionState
  }>({})

  const updateRequestStates = (newState: typeof requestStates) => {
    setRequestStates(oldState => ({ ...oldState, ...newState }))
  }

  const dispatch = useDispatch()
  const deleteTextAnswer = actions.api.deleteTextAnswer(dispatch)

  return [
    (textAnswerId: number) => {
      updateRequestStates({ [textAnswerId]: 'doing' })
      deleteTextAnswer(textAnswerId)
        .then(() => updateRequestStates({ [textAnswerId]: 'done' }))
        .catch(() => updateRequestStates({ [textAnswerId]: 'error' }))
    },
    Object.keys(requestStates)
      .map(id => parseInt(id))
      .filter(id => requestStates[id] === 'doing'),
    Object.keys(requestStates)
      .map(id => parseInt(id))
      .filter(id => requestStates[id] === 'done'),
    Object.keys(requestStates)
      .map(id => parseInt(id))
      .filter(id => requestStates[id] === 'error'),
  ]
}
