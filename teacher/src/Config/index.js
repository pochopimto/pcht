import axios from 'axios'

export const DEBUG = process.env.NODE_ENV !== 'production'

const LOCAL_SERVER = false

export const REQUEST_RETRY_DELAY = 5000
export const TIMEOUTS = {
  surveys: 5000,
  trafficLights: 5000,
  currentUser: 60000,
  templates: 60000,
  schools: 60000,
}

let api_params = {
  timeout: 4000,
}

if (DEBUG) {
  api_params['baseURL'] = LOCAL_SERVER
    ? 'http://localhost:8000'
    : 'https://www.pochopimto.cz'
}

export const add_token = (token) => {
  api.defaults.headers.common['Authorization'] = `Token ${token}`
}

export const remove_token = (token) => {
  api.defaults.headers.common['Authorization'] = undefined
}

export const api = axios.create(api_params)
