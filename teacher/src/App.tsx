import React from 'react'
import { Router } from 'react-router-dom'
import { ThemeProvider } from '@material-ui/styles'
import { createMuiTheme } from '@material-ui/core'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { SnackbarProvider } from 'notistack'

import store, { persistor } from 'Store'
import { history } from 'Routes'
import Main from 'Pages/main'
import Theme from 'Theme'

const theme = createMuiTheme(Theme)

const App = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ThemeProvider theme={theme}>
        <Router history={history}>
          <SnackbarProvider maxSnack={2}>
            <Main />
          </SnackbarProvider>
        </Router>
      </ThemeProvider>
    </PersistGate>
  </Provider>
)

export default App
