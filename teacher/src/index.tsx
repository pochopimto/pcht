import React from 'react'
import ReactDOM from 'react-dom'
import * as Sentry from '@sentry/browser'
import ReactGA from 'react-ga'

import App from 'App'
import * as serviceWorker from './serviceWorker'
import { DEBUG } from 'Config'

function initializeReactGA() {
  ReactGA.initialize('UA-153619307-1')
  ReactGA.pageview('/teacher')
}

if (!DEBUG) {
  Sentry.init({
    dsn: 'https://0be2282c1f9f4ba999c8947d494c8d86@sentry.io/1823356',
  })
}

serviceWorker.register()

const render = (Component: React.ComponentType) => {
  ReactDOM.render(<Component />, document.getElementById('root'))
}

initializeReactGA()
render(App)

/*
if ((module as any).hot) {
  ;(module as any).hot.accept('./App', () => {
    const NextApp = require('./App').default
    render(NextApp)
  })
}*/
