import { combineReducers } from 'redux'
import dayjs from 'dayjs'

import actions from 'Actions'

let authInitialState = {
  email: undefined,
  token: undefined,
  loggedIn: false,
  logoutPending: false,
  error: undefined,
}

const auth = (state = authInitialState, action) => {
  switch (action.type) {
    case actions.types.auth.login.start:
      return {
        ...state,
      }
    case actions.types.auth.login.ok:
      return {
        ...state,
        loggedIn: true,
        token: action.data.token,
        email: action.data.email,
      }
    case actions.types.auth.login.bad:
      return {
        ...state,
        loggedIn: false,
        token: undefined,
        email: undefined,
      }
    case actions.types.auth.logout.start:
      return {
        ...state,
        logoutPending: true,
      }
    case actions.types.auth.logout.ok:
      return {
        ...state,
        loggedIn: false,
        email: undefined,
        token: undefined,
        logoutPending: false,
      }
    default:
      return state
  }
}

const apiInitialVars = {
  data: {},
  retrieving: false,
  lastRetrieved: undefined,
  upToDate: false,
  error: undefined,
}
const apiInitialWriteVars = {
  creating: false,
  creatingError: undefined,
  lastCreatedId: undefined,
}

const apiInitialState = {
  surveys: {
    ...apiInitialVars,
    ...apiInitialWriteVars,
  },
  templates: {
    ...apiInitialVars,
    ...apiInitialWriteVars,
  },
  currentUser: {
    ...apiInitialVars,
    ...apiInitialWriteVars,
  },
  schools: {
    ...apiInitialVars,
    ...apiInitialWriteVars,
  },
}

const api = (state = apiInitialState, action) => {
  switch (action.type) {
    case actions.types.auth.logout.ok:
      return apiInitialState
    case actions.types.api.getCurrentUser.start:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          retrieving: true,
        },
      }
    case actions.types.api.getCurrentUser.ok:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          data: {
            ...action.data,
            schoolId: action.data.school,
            surveyIds: action.data.surveys,
          },
          retrieving: false,
          lastRetrieved: dayjs().valueOf(),
          upToDate: true,
          error: undefined,
        },
      }
    case actions.types.api.getCurrentUser.bad:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          retrieving: false,
          lastRetrieved: dayjs().valueOf(),
          error: action.data.error,
        },
      }
    case actions.types.api.getCurrentUser.retire:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          upToDate: false,
        },
      }
    case actions.types.api.getSurveys.start:
      return {
        ...state,
        surveys: {
          ...state.surveys,
          retrieving: true,
        },
      }
    case actions.types.api.getSurveys.ok:
      return {
        ...state,
        surveys: {
          ...state.surveys,
          data: action.data.reduce((obj, survey) => {
            obj[survey.id] = {
              id: survey.id,
              teacherId: survey.teacher,
              questions: survey.questions,
              name: survey.name,
              date: dayjs(survey.date).valueOf(),
              answers: survey.answers,
            }
            return obj
          }, {}),
          retrieving: false,
          lastRetrieved: dayjs().valueOf(),
          upToDate: true,
          error: undefined,
        },
      }
    case actions.types.api.getSurveys.bad:
      return {
        ...state,
        surveys: {
          ...state.surveys,
          retrieving: false,
          lastRetrieved: dayjs().valueOf(),
          error: action.data.error,
        },
      }
    case actions.types.api.getSurveys.retire:
      return {
        ...state,
        surveys: {
          ...state.surveys,
          upToDate: false,
        },
      }
    case actions.types.api.createSurvey.start:
      return {
        ...state,
        surveys: {
          ...state.surveys,
          creating: true,
        },
      }
    case actions.types.api.createSurvey.ok:
      return {
        ...state,
        surveys: {
          ...state.surveys,
          upToDate: false,
          creatingError: undefined,
          creating: false,
          lastCreatedId: action.data.surveyId,
        },
      }
    case actions.types.api.deleteSurvey.start:
      return {
        ...state,
        surveys: {
          ...state.surveys,
          data: Object.filter(
            state.surveys.data,
            s => s.id !== action.data.surveyId
          ),
        },
      }
    case actions.types.api.deleteSurvey.ok:
    case actions.types.api.deleteSurvey.bad:
      return {
        ...state,
        surveys: {
          ...state.surveys,
          upToDate: false,
        },
      }
    case actions.types.api.createSurvey.bad:
      return {
        ...state,
        surveys: {
          ...state.surveys,
          creatingError: action.data.error,
          creating: false,
        },
      }
    case actions.types.api.getTemplates.start:
      return {
        ...state,
        templates: {
          ...state.templates,
          retrieving: true,
        },
      }
    case actions.types.api.getTemplates.ok:
      return {
        ...state,
        templates: {
          ...state.templates,
          data: action.data.reduce((obj, template) => {
            obj[template.id] = {
              id: template.id,
              teacherId: template.teacher,
              questions: template.questions,
              name: template.name,
              date: dayjs(template.date).valueOf(),
            }
            return obj
          }, {}),
          retrieving: false,
          lastRetrieved: dayjs().valueOf(),
          upToDate: true,
          error: undefined,
        },
      }
    case actions.types.api.getTemplates.bad:
      return {
        ...state,
        templates: {
          ...state.templates,
          retrieving: false,
          lastRetrieved: dayjs().valueOf(),
          error: action.data.error,
        },
      }
    case actions.types.api.getTemplates.retire:
      return {
        ...state,
        templates: {
          ...state.templates,
          upToDate: false,
        },
      }
    case actions.types.api.deleteTemplate.start:
      return {
        ...state,
        templates: {
          ...state.templates,
          data: Object.filter(
            state.templates.data,
            s => s.id !== action.data.templateId
          ),
        },
      }
    case actions.types.api.createTemplate.ok:
    case actions.types.api.updateTemplate.ok:
    case actions.types.api.deleteTemplate.ok:
    case actions.types.api.deleteTemplate.bad:
      return {
        ...state,
        templates: {
          ...state.templates,
          upToDate: false,
        },
      }
    case actions.types.api.getSchools.start:
      return {
        ...state,
        schools: {
          ...state.schools,
          retrieving: true,
        },
      }
    case actions.types.api.getSchools.ok:
      return {
        ...state,
        schools: {
          ...state.schools,
          data: action.data.reduce((obj, school) => {
            obj[school.id] = {
              id: school.id,
              name: school.name,
              address: school.address,
            }
            return obj
          }, {}),
          retrieving: false,
          lastRetrieved: dayjs().valueOf(),
          upToDate: true,
          error: undefined,
        },
      }
    case actions.types.api.getSchools.bad:
      return {
        ...state,
        schools: {
          ...state.schools,
          retrieving: false,
          lastRetrieved: dayjs().valueOf(),
          error: action.data.error,
        },
      }
    case actions.types.api.getSchools.retire:
      return {
        ...state,
        schools: {
          ...state.schools,
          upToDate: false,
        },
      }
    case actions.types.api.createSchool.start:
      return {
        ...state,
        schools: {
          ...state.schools,
          creating: true,
        },
      }
    case actions.types.api.createSchool.ok:
      return {
        ...state,
        schools: {
          ...state.schools,
          data: {
            ...state.schools.data,
            [action.data.schoolId]: action.data.school,
          },
          upToDate: false,
          creatingError: undefined,
          creating: false,
          lastCreatedId: action.data.schoolId,
        },
      }

    default:
      return state
  }
}

const rootReducer = combineReducers({
  auth,
  api,
})

export default rootReducer
