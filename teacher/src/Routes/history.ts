import { createBrowserHistory } from 'history'

const history = createBrowserHistory({
  basename: 'teacher',
})

export default history
