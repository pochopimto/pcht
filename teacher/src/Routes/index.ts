import history from './history'

export { default as history } from './history'

export const navigateTo = (url: string) => history.push(url)

export default {
  history,
  navigateTo,
}
