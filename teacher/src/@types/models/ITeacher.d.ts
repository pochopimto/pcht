declare type ITeacher = {
  email: string
  password?: string
  name: string
  school: number
  principal: boolean
  agreed_to_terms: boolean
  subscribed_to_newsletter: boolean
  completed_intro: boolean
}
