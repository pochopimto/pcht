declare type INotificationType =
  | 'default'
  | 'error'
  | 'warning'
  | 'info'
  | 'success'

declare type INotification = {
  id: IUUID
  message: string
  type: INotificationType
  actions?: ((key: IUUID) => JSX.Element)[]
}
