const deepCompare = (a, b) => {
  if (a===b) return true;
  if (isNaN(a) && isNaN(b) && typeof a === 'number' && typeof b === 'number') return true;
  if ((typeof a === 'function' && typeof b === 'function') ||
      (a instanceof Date && b instanceof Date) ||
      (a instanceof RegExp && b instanceof RegExp) ||
      (a instanceof String && b instanceof String) ||
      (a instanceof Number && b instanceof Number)) {
      return a.toString() === b.toString();
  }
  if (!(a instanceof Object && b instanceof Object)) return false;
  for (let p in a) {
    if (b.hasOwnProperty(p) !== a.hasOwnProperty(p)) return false;
  }
  for (let p in b) {
    if (b.hasOwnProperty(p) !== a.hasOwnProperty(p)) return false;
    if (!deepCompare(a,b)) return false;
  }
  return true;
};

const firstUpper = text => text[0].toUpperCase() + text.slice(1).toLowerCase();
const startCase = text => firstUpper(text).replace("/ ./g", c => c.toUpperCase());

const getColor = (theme, colorId) => (
  [
    theme.colors.barbie,
    theme.colors.purple,
    theme.colors.yellow,
    theme.colors.white,
  ][colorId]
)

Object.filter = (obj, predicate) => 
    Object.keys(obj)
          .filter(key => predicate(obj[key]))
          .reduce((res, key) => (res[key] = obj[key], res), {});

export default {
  deepCompare,
  firstUpper,
  startCase,
  getColor,
};
