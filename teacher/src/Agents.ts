import { api } from 'Config'

// #############################################################################################################
//
//   Agents
//   - every endpoint should have its own agent it will be responsible for all request made to that endpoint
//   - agents should be pure - they will use only the data they will get in parameters
//
// #############################################################################################################

const currentTeacher = {
  get: () => api.get('/api/current_teacher') as Promise<ITeacher>,
  patch: (data: Partial<ITeacher>) =>
    api.patch('/api/current_teacher', data) as Promise<ITeacher>,
}

export default {
  currentTeacher,
}
