# Pcht

Awesome Pochopimto app.

# How to setup

- create python virtual environment
- run `pip install -r requirements.txt`
- run `npm i` in both react apps
- run `cp .env.sample .env` and fill in real values

# How to start server

- for Django run `python manage.py runserver`
- for react apps run `npm run start` in `student/` or `teacher/`
