from django.shortcuts import render
from django.http import HttpResponse, FileResponse
from django.conf import settings
from django.views.generic import View
import logging
import os


def getStaticFileView(file):
    file_path = os.path.join(settings.BASE_DIR, file)

    class FrontendFileView(View):
        """
        Serves {file}.
        """

        def get(self, request):
            try:
                with open(file_path) as f:
                    return HttpResponse(f.read())
            except FileNotFoundError:
                logging.exception("Production build of {file}".format(file=file))
                return HttpResponse("Updating ...", status=404)

    return FrontendFileView

