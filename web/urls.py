from django.conf.urls import include
from django.urls import path, re_path
from django.views import generic
from .views import getStaticFileView
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    re_path(r"^$", generic.RedirectView.as_view(url="/landing-page", permanent=False)),
    path(
        "zak/<slug:code>",
        generic.RedirectView.as_view(url="/student/%(code)s", permanent=False),
    ),
    re_path(r"^zak/", generic.RedirectView.as_view(url="/student", permanent=False)),
    re_path(r"^ucitel/", generic.RedirectView.as_view(url="/teacher", permanent=False)),
    re_path(r"^student/", getStaticFileView("student/build/index.html").as_view()),
    re_path(r"^teacher/", getStaticFileView("teacher/build/index.html").as_view()),
    re_path(r"^landing-page/", getStaticFileView("landing-page/index.html").as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
