import Polyglot from "node-polyglot";
import languages from "./data/language.json";

const getLanguageCode = () => {
  const userLang = navigator.language || navigator.userLanguage;
  switch (userLang.substring(0, 2)) {
    case "cs":
      return "cs";
    default:
      return "en";
  }
};

const polyglotLanguages = Object.keys(languages).reduce((obj, loc) => {
  obj[loc] = new Polyglot({ phrases: languages[loc], locale: loc });
  return obj;
}, {});

export const usePolyglot = () => {
  return (...args) => polyglotLanguages[getLanguageCode()].t(...args);
};
