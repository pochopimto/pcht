import React from 'react'
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import { ThemeProvider } from '@material-ui/styles'
import { createMuiTheme } from '@material-ui/core/styles'

import AppComponent from './components/App'

const theme = createMuiTheme({
  backgroundColor: '#1f2532',
  inputBackgroundColor: '#2d3444',
  pinkColor: '#ff0099',
  anonymousHeight: 18,
  anonymousWidth: 117,
  logoHeight: 26,
  logoWidth: 58,
  inputHeight: 56,
  inputWidth: 184,
  submitHeight: 56,
  submitWidth: 56,

  anonymousMargin: 27,
  logoMargin: 67,

  headerHeight: 72,

  text: {
    fontFamily: 'Monstserrat, sans-serif',
  },
})

const App = props => {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter basename="student">
        <Switch>
          <Route path="/" exact={true} component={AppComponent} />
          <Route path="/:code" exact={true} component={AppComponent} />
          <Redirect to="/" />
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  )
}

export default App
