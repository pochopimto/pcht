import React from 'react'
import ReactDOM from 'react-dom'
import * as Sentry from '@sentry/browser'

import App from './App'
import * as serviceWorker from './serviceWorker'
import { DEBUG } from './config'

if (!DEBUG) {
  Sentry.init({
    dsn: 'https://0be2282c1f9f4ba999c8947d494c8d86@sentry.io/1823356',
  })
}

ReactDOM.render(<App />, document.getElementById('root'))

serviceWorker.register()
