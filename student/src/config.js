import axios from 'axios'

export const DEBUG = process.env.NODE_ENV !== 'production'

export const REQUEST_RETRY_DELAY = 2000

let api_params = {
  timeout: 2000,
}

if (DEBUG) {
  api_params['baseURL'] = 'http://localhost:8000'
}

export const api = axios.create(api_params)
