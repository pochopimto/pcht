import { REQUEST_RETRY_DELAY } from "./config";

export function get_time() {
    return (new Date()).getTime();
}

export function get_user_inicials(name) {
    let out = "";
    for (let i = 0; i < name.length; ++i) {
        if (i === 0 || name[i-1] === ' ') {
            out += name[i];
        }
    }
    return out;
}

export function make_loaded(obj, update_func) {
    if (get_time() - REQUEST_RETRY_DELAY > (obj.time === undefined? 0 : obj.time)) {
        update_func();
    }
}

export function loaded(obj) {
    return obj && obj.obj && !obj.error;
}

export function make_id(length) {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

export const copy = (data) => {
    return JSON.parse(JSON.stringify(data));
};
