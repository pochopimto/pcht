import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import { makeStyles, withStyles, Slider } from '@material-ui/core'
import Cookies from 'js-cookie'

import { Anonymous, Arrow, Tick } from './mixed'

import { REQUEST_RETRY_DELAY, api } from '../config'
import Loader from './Loader'

import * as hooks from '../hooks'

const usePositionStyles = makeStyles(theme => ({
  position: {
    transition: 'left .25s ease-out, transform .25s ease-out',
    position: 'absolute',
    left: p => {
      switch (p.position) {
        case 'prev':
          return '0'
        case 'current':
          return '50%'
        case 'next':
          return '100%'

        default:
          console.error(`Unexpected position ${p.position}`)
          return '50%'
      }
    },
    transform: p => {
      switch (p.position) {
        case 'prev':
          return 'translateX(-100%)'
        case 'current':
          return 'translateX(-50%)'
        case 'next':
          return 'translateX(0)'

        default:
          console.error(`Unexpected position ${p.position}`)
          return 'translateX(-50%)'
      }
    },
  },
}))

const useQuestionStyles = makeStyles(theme => ({
  question: {
    minHeight: '100%',
    width: '100%',
    maxWidth: 500,
  },
  questionText: {
    ...theme.text,
    fontSize: '24px',
    fontWeight: '500',
    letterSpacing: '.6px',
    textAlign: 'center',
    color: 'white',
    margin: '24px 0 64px 0',
  },
}))

const Question = ({ question, children, position, ...props }) => {
  const c = {
    ...usePositionStyles({ position }),
    ...useQuestionStyles(),
  }

  return (
    <div className={`${c.question} ${c.position} ${props.className || ''}`}>
      <div className={c.questionText}>{question}</div>
      {children}
    </div>
  )
}

const useQuestionCommonStyles = makeStyles(theme => ({
  defaultForm: {
    margin: '0 8px',
    width: 'calc(100% - 16px)',

    '& button': {
      width: '100%',
      height: '40px',
      background: '#a452ff',
      padding: '0',
      opacity: '0',
      transition: 'all .25s ease-in-out',
      cursor: 'pointer',

      '& svg': {
        height: '32px',
        fill: 'white',
        float: 'right',
        marginRight: '16px',
      },
    },
  },
}))

const useQuestionTextStyles = makeStyles(theme => ({
  textarea: {
    ...theme.text,
    display: 'block',
    margin: '0',
    boxSizing: 'border-box',
    width: '100%',
    height: '176px',
    padding: '24px',
    overflow: 'auto',
    borderRadius: '12px',
    background: '#2d3444',
    fontSize: '20px',
    fontWeight: '500',
    color: 'white',
    transition: 'all .25s ease-in-out',
    resize: 'none',

    '&::placeholder': {
      fontWeight: '300',
    },
    '&:focus': {
      borderBottomLeftRadius: '0',
      borderBottomRightRadius: '0',

      '& ~ button': {
        opacity: '1',
      },
    },
  },
}))

const QuestionText = ({ question, save, className = '', ...props }) => {
  const c = {
    ...useQuestionCommonStyles(),
    ...useQuestionTextStyles(),
  }

  const t = hooks.usePolyglot()
  const [text, setText] = useState('')

  return (
    <Question
      question={question.question}
      className={`${className}`}
      {...props}
    >
      <form
        onSubmit={e => {
          e.preventDefault()
          save({ text })
        }}
        className={c.defaultForm}
      >
        <textarea
          className={c.textarea}
          placeholder={t('start_typing')}
          val={text}
          onChange={e => setText(e.target.value)}
        />
        <button type="submit">
          <Arrow />
        </button>
      </form>
    </Question>
  )
}

const PchtSlider = withStyles({
  root: {
    height: 8,
    marginLeft: 24,
    marginRight: 24,
  },
  thumb: {
    height: 18,
    width: 18,
    backgroundColor: '#FF0099',
    marginLeft: -9,
    transform: 'ccale(1)',
    transition: 'transform .35s ease-in-out',
    '&:focus,&:hover,&$active': {
      boxShadow: 'inherit',
      transform: 'scale(2)',
      transition: 'transform .25s ease-in-out',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 8,
    borderRadius: 4,
    color: '#FF0099',
  },
  rail: {
    height: 8,
    borderRadius: 4,
    color: '#1F2532',
  },
})(Slider)

const useQuestionScaleStyles = makeStyles(theme => ({
  slider: {
    height: '80px',
    background: '#2d3444',
    width: '100%',
    borderRadius: '12px',
    display: 'flex',
    alignItems: 'center',
  },

  sliderActive: {
    borderBottomLeftRadius: '0',
    borderBottomRightRadius: '0',

    '& ~ button': {
      opacity: '1',
    },
  },
}))

const QuestionScale = ({ question, save, className = '', ...props }) => {
  const c = {
    ...useQuestionCommonStyles(),
    ...useQuestionScaleStyles(),
  }
  const [value, setValue] = useState(5)
  const [touched, setTouchedHard] = useState(false)
  const setTouched = () => !touched && setTouchedHard(true)
  return (
    <Question
      question={question.question}
      className={className + ' scale'}
      {...props}
    >
      <form
        onSubmit={e => {
          e.preventDefault()
          save({ number: value })
        }}
        className={c.defaultForm}
      >
        <div className={`${c.slider} ${touched ? c.sliderActive : ''}`}>
          <PchtSlider
            min={0}
            max={10}
            value={value}
            onChange={(e, v) => {
              setValue(v)
              setTouched()
            }}
          />
        </div>
        <button type="submit">
          <Arrow />
        </button>
      </form>
    </Question>
  )
}

const useQuestionSelectStyles = makeStyles(theme => {
  const margin = 44
  return {
    container: {
      width: 'calc(100% - #{2*$margin})',
      margin: `0 ${margin}`,
    },

    option: {
      ...theme.text,
      width: '100%',
      background: '#2d3444',
      borderRadius: '12px',
      display: 'flex',
      alignItems: 'center',
      fontSize: '16px',
      fontWeight: '600',
      color: 'white',
      marginBottom: '32px',
      cursor: 'pointer',

      '&:last-child': {
        margin: '0',
      },
    },

    chip: {
      ...theme.text,
      height: '32px',
      width: '32px',
      margin: '12px',
      lineHeight: '32px',
      textAlign: 'center',
      borderRadius: '16px',
      background: '#1f2532',
      color: '#2d3444',
      fontSize: '16px',
    },
  }
})

const QuestionSelect = ({ question, save, className = '', ...props }) => {
  const c = {
    ...useQuestionCommonStyles(),
    ...useQuestionSelectStyles(),
  }
  return (
    <Question
      question={question.question}
      className={className + ' select'}
      {...props}
    >
      <div className={c.container}>
        {question.options.map((o, i) => (
          <div
            className={c.option}
            key={o.id}
            onClick={e => {
              save({ option_id: o.id })
            }}
          >
            <div className={c.chip}>
              {String.fromCharCode('a'.charCodeAt(0) + i)}
            </div>
            {o.text}
          </div>
        ))}
      </div>
    </Question>
  )
}

const generateAnswers = (answers, questions) =>
  questions.map((q, i) => {
    if (answers[i] !== undefined) return answers[i]
    else
      switch (q.type) {
        case 'text':
          return { text: '' }
        case 'scale':
          return { number: 0 }
        case 'select':
          return { option: undefined }
        default:
          console.error(`I do not know this type: '${q.type}'`)
          return {}
      }
  })

const headerHeight = 72

const useSurveyStyles = makeStyles(theme => ({
  survey: {
    height: '100%',
    width: '100%',
  },
  header: {
    height: headerHeight,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    position: 'relative',
  },
  progress: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: 8,
    backgroundImage: 'linear-gradient(93deg, #ff0099, #a452ff)',
    transition: 'all .5s ease-out',
    width: p =>
      `${
        p.survey
          ? Math.min(
              ((p.questionI + 1) * 100) / (p.survey.questions.length + 1),
              100
            )
          : 0
      }%`,
    transform: p => (p.last ? 'translateY(-100%)' : 'translateY(0)'),
  },

  space: {
    flex: '1 1 44px',
  },

  anonymous: {
    flex: '0 0 auto',
  },

  progressRatio: {
    ...theme.text,
    fontSize: 20,
    fontWeight: 500,
    letterSpacing: -0.5,
    color: '#ffffff',
    paddingRight: 24,
    flex: '1 0 44px',
    textAlign: 'right',
    transition: 'all .25s ease-out',
  },

  progressRatioHidden: {
    transform: 'translateY(-50px)',
  },
  questions: {
    overflowX: 'hidden',
    minHeight: `calc(100% - ${theme.headerHeight}px)`,
    position: 'relative',
  },
  lastPage: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    minHeight: '100%',
  },
  lastPageCircle: {
    height: '198px',
    width: '198px',
    borderRadius: '99px',
    backgroundImage: 'linear-gradient(148deg, #f09, #a452ff)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '& svg': {
      color: 'white',
      height: '63px',
      width: '97px',
    },
  },
  space1: {
    flexGrow: '1',
  },
  space2: {
    flexGrow: '2',
  },
}))

const useLoadSurvey = (setApiState, onWrongCode = () => undefined) => code => {
  const t = hooks.usePolyglot()

  api
    .get('/api/surveys/' + code)
    .then(response => {
      setApiState({
        loading: false,
        error: undefined,
        data: response.data,
      })
    })
    .catch(error => {
      if (error.response) {
        if (error.response.status === 404) {
          setApiState({
            loading: false,
            error: {
              error,
              message: [t('error.wrong_code'), t('error.please_try_again')],
            },
            data: undefined,
          })
          onWrongCode()
        } else {
          setApiState({
            loading: false,
            error: {
              error,
            },
            data: undefined,
          })
        }
      } else {
        setApiState({
          loading: false,
          error: {
            error,
            message: [
              t('error.connection_error'),
              t('error.please_try_again_later'),
            ],
          },
          data: undefined,
        })
      }
    })
}

const useSubmitAnswers = survey => answers => {
  if (survey === undefined) return false
  const id = survey.id
  return api
    .post('/api/answers', {
      survey: id,
      answers,
    })
    .then(response => {
      Cookies.set(`survey_${id}`, true)
    })
    .catch(error => console.error({ error }))
}

const Survey = ({ history, setError, code, ...props }) => {
  const t = hooks.usePolyglot()

  const [apiState, setApiState] = React.useState({
    loading: false,
    error: undefined,
    data: undefined,
  })

  const [questionI, setQuestionI] = React.useState(0)
  const [answers, setAnswers] = React.useState(
    apiState.data === undefined
      ? []
      : generateAnswers([], apiState.data.questions)
  )
  const [lastQuery, setLastQuery] = React.useState(0)

  const last = apiState.data && questionI === apiState.data.questions.length

  const loadSurvey = useLoadSurvey(setApiState, () => {
    history.replace('/')
    setError([t('error.survey_not_found'), t('error.please_try_again')])
  })
  const submitAnswers = useSubmitAnswers(apiState.data)

  const c = {
    ...usePositionStyles({ position: last ? 'current' : 'next' }),
    ...useSurveyStyles({
      survey: apiState.data,
      questionI,
      last,
    }),
  }

  React.useEffect(() => {
    if (apiState.data === undefined && !apiState.loading) {
      const currentTime = new Date().getTime()
      const timeout = lastQuery + REQUEST_RETRY_DELAY - currentTime
      const callback = () => {
        loadSurvey(code.toLowerCase())
        setLastQuery(new Date().getTime())
      }
      if (timeout > 0) {
        const pid = setTimeout(callback, timeout)
        return () => clearTimeout(pid)
      } else {
        callback()
      }
    } else if (
      apiState.data &&
      apiState.data.questions.length !== answers.length
    ) {
      setAnswers(a => generateAnswers(a, apiState.data.questions))
    }
  }, [
    apiState.data,
    apiState.loading,
    code,
    (apiState.data || {}).questions,
    answers,
    loadSurvey,
  ])

  React.useEffect(() => {
    if (
      apiState.data !== undefined &&
      apiState.data.questions.length === questionI
    ) {
      submitAnswers(answers)
    }
  }, [
    apiState.data,
    (apiState.data || {}).questions,
    questionI,
    answers,
    submitAnswers,
  ])

  if (apiState.data !== undefined) {
    return (
      <div className={c.survey}>
        <div className={c.header}>
          <div className={`${c.progress} ${last ? c.progressHidden : ''}`} />
          <div className={c.space} />
          <Anonymous className={c.anonymous} />
          <div
            className={`${c.progressRatio} ${
              last ? c.progressRatioHidden : ''
            }`}
          >
            {Math.min(questionI + 1, apiState.data.questions.length)}/
            {apiState.data.questions.length}
          </div>
        </div>
        <div className={c.questions}>
          {[
            ...apiState.data.questions.map((q, i) => {
              const position = (() => {
                if (last || questionI > i) {
                  return 'prev'
                } else if (questionI < i) {
                  return 'next'
                } else {
                  return 'current'
                }
              })()

              const save = newState => {
                setQuestionI(i => i + 1)
                setAnswers(a => {
                  let ans = JSON.parse(JSON.stringify(a))
                  ans[i] = newState
                  return ans
                })
              }

              switch (q.type) {
                case 'text':
                  return (
                    <QuestionText
                      key={q.id}
                      question={q}
                      save={save}
                      position={position}
                    />
                  )
                case 'scale':
                  return (
                    <QuestionScale
                      key={q.id}
                      question={q}
                      save={save}
                      position={position}
                    />
                  )
                case 'select':
                  return (
                    <QuestionSelect
                      key={q.id}
                      question={q}
                      save={save}
                      position={position}
                    />
                  )
                default:
                  console.error(`Unknown question type ${q.type}`)
                  return <div className={'Question'}>Error</div>
              }
            }),
            <div key="last" className={`${c.lastPage} ${c.position}`}>
              <div className={c.space1} />
              <div className={c.lastPageCircle}>
                <Tick />
              </div>
              <div className={c.space2} />
            </div>,
          ]}
        </div>
      </div>
    )
  } else {
    return <Loader />
  }
}

export default withRouter(Survey)
