import React from 'react'
import { makeStyles } from '@material-ui/styles'
import { CircularProgress } from '@material-ui/core'

const useStyles = makeStyles({
  loader: {
    height: '100%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
})

const Loader = ({ className = '' }) => {
  const c = useStyles()

  return (
    <div className={`${c.loader} ${className}`}>
      <CircularProgress />
    </div>
  )
}

export default Loader
