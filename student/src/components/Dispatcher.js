import React from 'react'
import Survey from './Survey'
import TrafficLights from './TrafficLights'

export const Dispatcher = ({ match, setError, ...props }) => {
  const code = match.params.code
  if (code[0] === '0') {
    return <TrafficLights code={code} setError={setError}/>
  } else {
    return <Survey code={code} setError={setError}/>
  }
}

export default Dispatcher