import React from 'react';
import { makeStyles } from '@material-ui/core'

import { Logo, Anonymous, Arrow, Wrong } from './mixed';

import * as hooks from "../hooks"

const maxLength = 6;

const useStyles = makeStyles(theme => ({
  codePrompt: {
    height: '100%',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  spaceBefore: {
    flex: '0 0 25px',
  },
  spaceAfter: {
    flex: '3 0 25px',
  },

  form: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    flex: '1 1 auto',
    minHeight: 65 + theme.logoHeight + theme.anonymousHeight + theme.inputHeight + theme.submitHeight + 'px',
  },

  space: {
    minHeight: '15px',
  },
  spaceLogo: {
    flex: '0 1 67px',
  },
  spaceInput: {
    flex: '1 1 118px',
    minHeight: '100px',
  },
  spaceButton: {
    flex: '0 1 24px',
  },

  logo: {
    flex: '0 0 auto',
  },

  anonymous: {
    flex: '0 0 auto',
  },

  input: {
    ...theme.text,
    height: theme.inputHeight,
    width: theme.inputWidth,
    padding: '24px 16px',
    boxSizing: 'border-box',
    borderRadius: 12,
    backgroundColor: theme.inputBackgroundColor,
    fontSize: 16,
    fontWeight: 500,
    color: '#ffffff',
    flex: '0 0 auto',
  },

  button: {
    ...theme.text,
    cursor: 'pointer',
    height: theme.submitHeight,
    width: theme.submitWidth,
    borderRadius: (theme.submitHeight + theme.submitWidth)/4,
    backgroundColor: '#F09',
    flex: '0 0 auto',
    opacity: '0',
    transition: 'all .25s ease-out',
    "& svg": {
      height: 32,
      width: 32,
      fill: '#ffffff',
    },
  },
  buttonVisible: {
    opacity: 1,
  },
  buttonDisabled: {
    backgroundColor: '#cecece',
    pointerEvents: 'none',
    cursor: 'default',
  },
  error: {
    boxSizing: 'border-box',
    position: 'absolute',
    left: "50%",
    opacity: p => p.error ? 1 : 0,
    transition: 'all .25s ease-in-out',
    transform: p => p.error? 'translate(-50%, 0)' : 'translate(-50%, -100%)',
    width: 'calc(100% - 48px)',
    maxWidth: 264,
    height: 136,
    padding: 24,
    backgroundColor: theme.inputBackgroundColor,
    borderRadius: 12,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    "& svg": {
      height: 32,
      width: 32,
      color: theme.pinkColor,
    },
  },
  errorText: {
    ...theme.text,
    fontSize: 16,
    fontWeight: 600,
    color: '#ffffff',
    lineHeight: '24px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }
}))

export const CodePrompt = ({ error, ...props }) => {

  const [ code, setCode ] = React.useState('')

  const c = useStyles({error: error !== undefined})
  const t = hooks.usePolyglot();

  const isValid = () => {
    return code.length === maxLength
  }

  return (
    <div className={c.codePrompt}>
      <div className={c.spaceBefore}/>
      <form onSubmit={e => {
        e.preventDefault()
        if (isValid()) {
          props.history.push('/' + code.toLowerCase())
        }
      }}
        className={c.form}
      >
        <div className={c.error}>
          <Wrong/>
          <div className={c.errorText}>
            {
              (error || []).map((m, i) => <span key={i}>{m}</span>)
            }
          </div>
        </div>
        <Anonymous className={c.anonymous}/>
        <div className={`${c.space} ${c.spaceLogo}`}/>
        <Logo className={c.logo}/>
        <div className={`${c.space} ${c.spaceInput}`}/>
        <input
          className={c.input}
          value={code}
          onChange={e=>e.target.value.length <= 6 && setCode(e.target.value.toUpperCase())}
          placeholder={t('enter_code')}
        />
        <div className={`${c.space} ${c.spaceButton}`}/>
        <button
          className={`${c.button} ${code ? c.buttonVisible : ''} ${!isValid() ? c.buttonDisabled : ''}`}
          type={'submit'}
        >
              <Arrow/>
          </button>
          <div className={c.space}/>
      </form>
      <div className={c.spaceAfter}/>
    </div>
  )
}

export default CodePrompt
