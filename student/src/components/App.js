import React from 'react'
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import { makeStyles } from '@material-ui/core'

import CodePrompt from './CodePrompt'
import Dispatcher from './Dispatcher'
const useStyles = makeStyles(theme => ({
  app: {
    overflow: 'auto',
    background: theme.backgroundColor,
    color: '#d2d2d2',
  },
}))

const App = props => {
  const c = useStyles()
  const [error, setError] = React.useState(undefined)

  return (
    <div className={c.app}>
      <Switch>
        <Route
          path={'/'}
          exact
          render={props => <CodePrompt {...props} error={error} />}
        />
        <Route
          path={'/:code'}
          exact
          render={props => <Dispatcher {...props} setError={setError} />}
        />
        <Redirect to={'/'} />
      </Switch>
    </div>
  )
}

export default withRouter(App)
