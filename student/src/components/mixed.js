import React, { Component } from "react";
import { Switch, Route, Redirect, withRouter } from "react-router-dom";
import { makeStyles } from "@material-ui/core";

const useLogoStyles = makeStyles(theme => ({
  logo: {
    width: theme.logoWidth,
    height: theme.logoHeight,
    display: "block"
  }
}));

export const Logo = props => {
  const c = useLogoStyles();
  return (
    <svg
      className={`${c.logo} ${props.className || ""}`}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 52 26"
    >
      <g fillRule="nonzero" fill={"#96F"}>
        <path d="M20.125 25.7c-.856 0-1.55-.712-1.55-1.59 0-.879.694-1.591 1.55-1.591h19.32a9.446 9.446 0 0 0 6.695-2.784 9.468 9.468 0 0 0 2.756-6.68c0-2.503-.99-4.906-2.756-6.68a9.446 9.446 0 0 0-6.695-2.784H12.772c-.856 0-1.55-.712-1.55-1.591s.694-1.591 1.55-1.591h26.673c3.323 0 6.511 1.327 8.865 3.692a12.69 12.69 0 0 1 3.688 8.954 12.69 12.69 0 0 1-3.688 8.953 12.508 12.508 0 0 1-8.865 3.693h-19.32zM2 25.7c-.857 0-1.551-.713-1.551-1.592 0-.878.694-1.59 1.551-1.59h12.102c.857 0 1.551.712 1.551 1.59 0 .879-.694 1.591-1.551 1.591H2z" />
      </g>
    </svg>
  );
};

const useAnonymousStyles = makeStyles(theme => ({
  anonymous: {
    color: "#d2d2d2",
    width: theme.anonymousWidth,
    display: "flex",
    alignContent: "center",
    userSelect: "none",

    "& svg": {
      height: theme.anonymousHeight,
      width: 21,
      marginRight: 18,
      display: "inline-block"
    },

    lineHeight: theme.anonymousHeight + "px",
    fontFamily: "Montserrat, sans-serif",
    fontSize: 16,
    fontWeight: 500,
    fontStyle: "normal",
    fontStretch: "normal",
    letterSpacing: 0.4
  }
}));

export const Anonymous = props => {
  const c = useAnonymousStyles();
  return (
    <div className={`${c.anonymous} ${props.className || ""}`}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="32"
        height="32"
        viewBox="0 0 32 32"
      >
        <g fill="currentColor" fillRule="evenodd">
          <path d="M5.59 13.187c.656-5.166 5.066-9.16 10.41-9.16s9.754 3.994 10.41 9.16h2.217a1.333 1.333 0 1 1 0 2.666H3.373a1.333 1.333 0 1 1 0-2.666h2.218zm2.696 0h15.428a7.829 7.829 0 0 0-15.428 0zM22.053 28a4.6 4.6 0 1 1 4.614-4.613A4.587 4.587 0 0 1 22.053 28zm0-6.52A1.933 1.933 0 1 0 24 23.387a1.92 1.92 0 0 0-1.947-1.934v.027zM9.947 28a4.6 4.6 0 1 1-.027-9.2 4.6 4.6 0 0 1 .027 9.2zm0-6.52a1.933 1.933 0 1 0 1.92 1.907 1.933 1.933 0 0 0-1.92-1.934v.027z" />
        </g>
      </svg>
      <div>incognito</div>
    </div>
  );
};

export const Arrow = props => (
  <svg viewBox={`0 0 32 32`}>
    <path d="M22.947 17.333H5.84a1.333 1.333 0 1 1 0-2.666h17.107L19.56 11.28a1.333 1.333 0 1 1 1.893-1.88l5.654 5.653a1.333 1.333 0 0 1 0 1.894l-5.654 5.72c-.262.225-.601.34-.946.32a1.333 1.333 0 0 1-.947-.387 1.333 1.333 0 0 1 0-1.88l3.387-3.387z" />
  </svg>
);

export const Tick = props => (
  <svg viewBox={`0 0 98 69`}>
    <path
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      fillRule="evenodd"
      stroke="currentColor"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth="15"
      d="M8 29.78L38.397 61 90 8"
    />
  </svg>
);

export const Wrong = props => {
  return (
    <svg viewBox="0 0 30 30" className={props.className}>
      <path
        d="M20.7866667,11.2133333 L11.2133333,20.7866667"
        stroke="currentColor"
        strokeWidth="1.69333333"
        strokeLinecap="round"
        strokeLinejoin="round"
      ></path>
      <path
        d="M20.7866667,20.7866667 L11.2133333,11.2133333"
        stroke="currentColor"
        strokeWidth="1.69333333"
        strokeLinecap="round"
        strokeLinejoin="round"
      ></path>
      <circle
        cx="16"
        cy="16"
        r="11"
        fill="none"
        stroke="currentColor"
        strokeWidth="1.69333333"
      />
    </svg>
  );
};
