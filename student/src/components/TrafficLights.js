import React from "react";

import { makeStyles } from "@material-ui/core";
import Cookies from "js-cookie";
import { withRouter } from "react-router-dom";

import { api } from "../config";
import { Anonymous } from "./mixed";
import Loader from "./Loader";

import * as hooks from "../hooks"

const useOptionStyles = makeStyles(theme => ({
  option: {
    borderRadius: 12,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "20px 24px",
    "&:not(:last-child)": {
      marginBottom: 24
    },
    opacity: p => (p.selected ? 0.35 : 1),
    transition: "opacity .25s ease-in-out",
    cursor: "pointer",
    userSelect: "none"
  },
  text: {
    ...theme.text,
    fontWeight: 600,
    marginRight: 24,
    lineHeight: "24px"
  },
  selectedText: {
    ...theme.text,
    fontWeight: 600,
    opacity: p => (p.selected ? 1 : 0),
    transition: "opacity .25s ease-in-out"
  }
}));

const Option = ({
  className = "",
  text = "",
  selected = false,
  onClick,
  ...props
}) => {
  const c = useOptionStyles({ selected });
  const t = hooks.usePolyglot();

  return (
    <div className={`${c.option} ${className}`} onClick={onClick}>
      <span className={c.text}>{text}</span>
      <span className={c.selectedText}>{t('traffic_light.selected')}</span>
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  trafficLights: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    minHeight: "100%"
  },
  anonymous: {
    marginTop: 24
  },
  upperSpace: {
    flex: "1 0 0"
  },
  lowerSpace: {
    flex: "2 0 0"
  },
  options: {
    flex: "0 0 auto"
  },
  ok: {
    backgroundColor: "#a452ff"
  },
  mid: {
    backgroundColor: "#ffa300"
  },
  bad: {
    backgroundColor: "#ff0099"
  }
}));

const useLoadTrafficLights = (
  setApiState,
  onWrongCode = () => undefined
) => code => {

  const t = hooks.usePolyglot();

  setApiState(s => ({
    ...s,
    loading: true,
    error: undefined
  }));
  api
    .get("/api/traffic-lights/" + code)
    .then(response => {
      setApiState({
        loading: false,
        error: undefined,
        data: response.data
      });
    })
    .catch(error => {
      if (error.response) {
        if (error.response.status === 404) {
          setApiState({
            loading: false,
            error: {
              error,
              message: [ 
                t('error.wrong_code'),
                t('error.please_try_again'),
              ]
            },
            data: undefined
          });
          onWrongCode();
        } else {
          setApiState({
            loading: false,
            error: {
              error
            },
            data: undefined
          });
        }
      } else {
        setApiState({
          loading: false,
          error: {
            error,
            message: [
              t('error.connection_error'),
              t('error.please_try_again_later'),
            ]
          },
          data: undefined
        });
      }
    });
};

const useUpdateState = (setApiState, trafficLights) => state => {
  if (trafficLights === undefined) return false;
  const id = trafficLights.id;
  const myEntry = Cookies.get(`trafficLights_${id}`);

  if (myEntry === undefined) {
    setApiState(as => ({
      loading: true,
      error: undefined,
      data: as.data
    }));
    return api
      .post("/api/traffic-lights-entries", {
        trafficLights: id,
        state
      })
      .then(response => {
        Cookies.set(`trafficLights_${id}`, response.data.id);
        setApiState({
          loading: false,
          error: undefined,
          data: response.data
        });
      })
      .catch(error => {
        console.error({ error });
        setApiState(as => ({
          loading: false,
          error: {
            error
          },
          data: as.data
        }));
      });
  } else {
    setApiState(as => ({
      loading: true,
      error: undefined,
      data: as.data
    }));
    return api
      .patch(`/api/traffic-lights-entries/${myEntry}`, {
        state
      })
      .then(response => {
        setApiState({
          loading: false,
          error: undefined,
          data: response.data
        });
      })
      .catch(error => {
        console.error({ error });
        setApiState(as => ({
          loading: false,
          error: {
            error
          },
          data: as.data
        }));
      });
  }
};

const TrafficLights = ({ history, code, setError, ...props }) => {
  const [loadingApiState, setLoadingApiState] = React.useState({
    loading: false,
    error: undefined,
    data: undefined
  });

  const [entryApiState, setEntryApiState] = React.useState({
    loading: false,
    error: undefined,
    data: undefined
  });

  const [desiredState, setDesiredState] = React.useState("ok");

  const c = useStyles({ state: desiredState });
  const t = hooks.usePolyglot();

  const loadTrafficLights = useLoadTrafficLights(setLoadingApiState, () => {
    history.replace("/");
    setError([
      t('error.this_traffic_light_does_not_exist'),
      t('error.please_try_again')
    ]);
  });

  const updateState = useUpdateState(setEntryApiState, loadingApiState.data);

  React.useEffect(() => {
    if (loadingApiState.loading) return;
    if (loadingApiState.data === undefined) {
      loadTrafficLights(code);
    }
  }, [loadingApiState, code]);

  React.useEffect(() => {
    if (entryApiState.loading) return;
    if (
      entryApiState.data === undefined ||
      entryApiState.data.state !== desiredState
    ) {
      updateState(desiredState);
    }
  }, [entryApiState, desiredState, loadingApiState.data]);

  if (loadingApiState.loading || loadingApiState.data === undefined) {
    return <Loader />;
  }

  return (
    <div className={c.trafficLights}>
      <Anonymous className={c.anonymous} />
      <div className={c.upperSpace} />
      <div className={c.options}>
        <Option
          className={c.ok}
          text={t('traffic_light.ok')}
          selected={desiredState === "ok"}
          onClick={() => setDesiredState("ok")}
        />
        <Option
          className={c.mid}
          text={t('traffic_light.mid')}
          selected={desiredState === "mid"}
          onClick={() => setDesiredState("mid")}
        />
        <Option
          className={c.bad}
          text={t('traffic_light.bad')}
          selected={desiredState === "bad"}
          onClick={() => setDesiredState("bad")}
        />
      </div>
      <div className={c.lowerSpace} />
    </div>
  );
};

export default withRouter(TrafficLights);
