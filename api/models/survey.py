from django.db import models, IntegrityError
import random
import string


def get_code():
    ret = []
    ret.extend(random.sample(string.ascii_lowercase, 2))
    ret.extend(random.sample(string.digits, 2))
    ret.extend(random.sample(string.ascii_lowercase, 2))

    return "".join(ret)


class Survey(models.Model):
    teacher = models.ForeignKey(
        "Teacher", on_delete=models.CASCADE, related_name="surveys"
    )

    name = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now_add=True)

    question_set = models.OneToOneField(
        "QuestionSet", models.CASCADE, related_name="survey"
    )
    id = models.SlugField(
        primary_key=True, max_length=6, editable=False, unique=True, blank=True
    )

    def save(self, *args, **kwargs):
        if not self.id:
            self.id = get_code()
        correct = False
        failiures = 0

        while not correct:
            try:
                super().save(*args, **kwargs)
            except IntegrityError:
                failiures += 1
                if failiures > 5:
                    raise
                else:
                    self.id = get_code()
            else:
                correct = True

