from django.db.models.signals import post_save, post_delete
from django_rest_passwordreset.signals import reset_password_token_created
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings
from django.urls import reverse
from django.core.mail import send_mail

from api.models import Teacher, Survey

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def post_save_user(sender, instance=None, created=False, **kwargs):
    if created:
        Teacher.objects.create(
            user=instance, name=instance.username,
        )
        Token.objects.create(user=instance)


@receiver(post_delete, sender=Teacher)
def post_delete_teacher(sender, instance=None, **kwargs):
    instance.user.delete()


@receiver(post_delete, sender=Survey)
def post_delete_survey(sender, instance=None, **kwargs):
    instance.question_set.delete()


@receiver(reset_password_token_created)
def password_reset_token_created(sender, reset_password_token, *args, **kwargs):
    """
    Handles password reset tokens
    When a token is created, an e-mail needs to be sent to the user
    :param sender:
    :param reset_password_token:
    :param args:
    :param kwargs:
    :return:
    """

    context = {
        "current_user": reset_password_token.user,
        "username": reset_password_token.user.username,
        "email": reset_password_token.user.email,
        "name": reset_password_token.user.teacher.name,
        "reset_password_token": reset_password_token.key,
    }

    send_mail(
        "Obnova hesla",
        """Ahoj

tady ti posíláme token pro obnovu tvého hesla:
{reset_password_token}
Tento odkaz je platný 24 hodin, potom si budeš muset vyžádat nový.

Hodně štěstí při sebezdokonalování a děkujeme za používání našeho produktu.

Tým Pochopímto
""".format(
            **context
        ),
        "noreply@pochopimto.cz",
        [context["email"]],
        fail_silently=False,
    )
