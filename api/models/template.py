from django.db import models, IntegrityError
import random
import string


class Template(models.Model):
    teacher = models.ForeignKey(
        "Teacher", on_delete=models.CASCADE, related_name="templates"
    )

    name = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now_add=True)

    question_set = models.OneToOneField(
        "QuestionSet", models.CASCADE, related_name="template"
    )

