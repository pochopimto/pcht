"""
    Permit to import everything from api.models without knowing the details.
"""


import sys

from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from .answer import AnswerSet, Answer
from .question import QuestionSet, Question, Option
from .teacher import Teacher, School
from .survey import Survey
from .template import Template
from .trafficLights import TrafficLights, TrafficLightsEntry
import api.models.receivers


__all__ = ['AnswerSet', 'Answer', 'QuestionSet', 'Question', 'Option', 'Teacher', 'School', 'Survey', 'Template', 'User', 'Token', 'TrafficLights', 'TrafficLightsEntry']


