from django.db import models
from django.contrib.auth.models import User


class Teacher(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    name = models.CharField(max_length=100)
    school = models.ForeignKey(
        "School",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        default=None,
        related_name="teachers",
    )
    principal = models.BooleanField(default=False)
    agreed_to_terms = models.BooleanField(default=False)
    subscribed_to_newsletter = models.BooleanField(default=False)
    completed_intro = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.name} <{self.user.email}>"


class School(models.Model):
    name = models.TextField()
    address = models.TextField()

    def __str__(self):
        return f"{self.name}"

