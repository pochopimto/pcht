from django.db import models
from django.core.exceptions import ValidationError


class QuestionSet(models.Model):
    pass


class Question(models.Model):
    SELECT = "select"
    SCALE = "scale"
    TEXT = "text"

    QUESTION_TYPES = (
        (SELECT, "Select Option"),
        (SCALE, "Linear scale"),
        (TEXT, "Text"),
    )

    question = models.TextField()
    order = models.IntegerField()
    type = models.CharField(choices=QUESTION_TYPES, max_length=10, default=TEXT,)
    question_set = models.ForeignKey(
        "QuestionSet", on_delete=models.CASCADE, related_name="questions"
    )

    class Meta:
        ordering = ["question_set", "order"]


class Option(models.Model):
    question = models.ForeignKey("Question", models.CASCADE, related_name="options")

    text = models.CharField(default="Option", max_length=100)
    order = models.IntegerField()

    class Meta:
        ordering = ("question", "order")

