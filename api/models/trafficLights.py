from django.db import models, IntegrityError
import random
import string


def get_code():
    ret = []
    ret.extend(["0", "0"])
    ret.extend(random.sample(string.ascii_lowercase, 2))
    ret.extend(random.sample(string.digits, 2))

    return "".join(ret)


class TrafficLights(models.Model):
    teacher = models.ForeignKey(
        "Teacher", on_delete=models.CASCADE, related_name="trafficLight"
    )
    date = models.DateTimeField(auto_now_add=True)
    id = models.SlugField(
        primary_key=True, max_length=6, editable=False, unique=True, blank=True
    )

    def save(self, *args, **kwargs):
        if not self.id:
            self.id = get_code()
        correct = False
        failiures = 0

        while not correct:
            try:
                super().save(*args, **kwargs)
            except IntegrityError:
                failiures += 1
                if failiures > 5:
                    raise
                else:
                    self.id = get_code()
            else:
                correct = True


class TrafficLightsEntry(models.Model):
    OK = "ok"
    MIDDLE = "mid"
    BAD = "bad"

    ENTRY_STATES = (
        (OK, "I understand"),
        (MIDDLE, "I'm not sure"),
        (BAD, "I'm lost"),
    )

    trafficLights = models.ForeignKey(
        TrafficLights, on_delete=models.CASCADE, related_name="entries"
    )
    date = models.DateTimeField(auto_now_add=True)
    state = models.CharField(choices=ENTRY_STATES, max_length=5, default=MIDDLE,)
