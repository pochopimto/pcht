from django.db import models
from django.core.exceptions import ValidationError

from .question import Question


class AnswerSet(models.Model):
    date = models.DateTimeField(auto_now_add=True)

    survey = models.ForeignKey("Survey", models.CASCADE, related_name="answer_sets")


class Answer(models.Model):
    answer_set = models.ForeignKey("AnswerSet", models.CASCADE, related_name="answers")
    question = models.ForeignKey(Question, models.CASCADE, related_name="answers")

    text = models.TextField(null=True, default=None, blank=True)
    number = models.IntegerField(null=True, default=None)
    option = models.ForeignKey(
        "Option", models.SET_NULL, null=True, default=None, related_name="answers"
    )

    class Meta:
        ordering = ("answer_set", "question__order")

    def validate(self):
        if self.answer_set.survey != self.question.question_set.survey:
            raise ValidationError(
                "This Answer belongs under different survey that question."
            )
        must_be_null = []
        must_be_filled = []
        if self.question.type in [Question.SELECT]:
            must_be_null += ["text", "number"]
            if self.option and self.question != self.option.question:
                raise ValidationError(
                    "This Answer is related to an option in different question."
                )
        elif self.option.question.type in [Question.SCALE]:
            must_be_null += ["text", "option"]
            must_be_filled += ["number"]
        elif self.option.question.type in [Question.TEXT]:
            must_be_null += ["number", "option"]
            must_be_filled += ["text"]

        for param in must_be_null:
            if self.__getattribute__(param) is not None:
                raise ValidationError(
                    "Param {param} needs to be empty because question has type {type}".format(
                        param=param, type=self.question.type
                    )
                )

