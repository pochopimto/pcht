from django.contrib.auth.models import User
from rest_framework import (
    generics,
    permissions,
    views,
    response,
    exceptions,
    status,
    pagination,
    viewsets,
)
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from .serializers import *
from .models import *
from .permissions import IsUser, IsTeacher, IsTeacherOrReadOnly
from django.views.generic import View
from django.http import HttpResponse, FileResponse
from django.conf import settings
import logging
import os


class SchoolList(generics.ListAPIView):

    queryset = School.objects.all()
    serializer_class = SchoolSerializer


class SchoolDetails(generics.RetrieveAPIView):

    queryset = School.objects.all()
    serializer_class = SchoolSerializer


class TeacherList(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)

    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer


class TeacherDetails(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsUser,)

    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer


class TeacherCurrentDetails(generics.RetrieveUpdateAPIView):

    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TeacherSerializer

    def get_object(self):
        return Teacher.objects.get(user=self.request.user)


class SurveyList(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)

    queryset = Survey.objects.all()
    serializer_class = SurveySerializer


class MySurveyWithAnswersList(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = SurveyWithAnswersSerializer

    def get_queryset(self):
        user = self.request.user
        return Survey.objects.filter(teacher__user=user)


class MySurveyWithAnswersDetails(generics.RetrieveDestroyAPIView):
    permission_classes = (IsTeacher,)

    serializer_class = SurveyWithAnswersSerializer

    def get_queryset(self):
        user = self.request.user
        return Survey.objects.filter(teacher__user=user)


class SurveyDetails(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    queryset = Survey.objects.all()
    serializer_class = SurveySerializer


class MyTemplateList(generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = TemplateSerializer

    def get_queryset(self):
        user = self.request.user
        return Template.objects.filter(teacher__user=user)


class MyTemplateDetails(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    serializer_class = TemplateSerializer

    def get_queryset(self):
        user = self.request.user
        return Template.objects.filter(teacher__user=user)


class QuestionDetails(generics.RetrieveAPIView):

    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class AnswerSetList(generics.CreateAPIView):

    queryset = AnswerSet.objects.all()
    serializer_class = AnswerSetSerializer


class AnswerSetDetails(generics.RetrieveAPIView):

    queryset = AnswerSet.objects.all()
    serializer_class = AnswerSetSerializer


class AnswerSetDestroy(generics.DestroyAPIView):
    """
        Deletes AnswerSet (AnswerSet is answer for one survey and consists of more Answers).
    """
    class CustomAnswerSetPermissionClass(permissions.BasePermission):
        def has_object_permission(self, request, view, obj):
            return request.user == obj.survey.teacher.user

    permission_classes = (CustomAnswerSetPermissionClass,)

    queryset = AnswerSet.objects.all()
    serializer_class = AnswerSetSerializer


class AnswerDestroy(generics.DestroyAPIView):
    """
        Deletes Answer (Answer is answer for one question).
    """
    class CustomAnswerPermissionClass(permissions.BasePermission):
        def has_object_permission(self, request, view, obj):
            return request.user == obj.answer_set.survey.teacher.user

    permission_classes = (CustomAnswerPermissionClass,)

    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer


class TrafficLightsList(generics.CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    queryset = TrafficLights.objects.all()
    serializer_class = TrafficLightsSerializer


class TrafficLightsDetails(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsTeacherOrReadOnly,)

    queryset = TrafficLights.objects.all()
    serializer_class = TrafficLightsSerializer


class TrafficLightsEntriesList(generics.CreateAPIView):

    queryset = TrafficLightsEntry.objects.all()
    serializer_class = TrafficLightsEntrySerializer


class TrafficLightsEntriesDetails(generics.RetrieveUpdateAPIView):

    queryset = TrafficLightsEntry.objects.all()
    serializer_class = TrafficLightsEntrySerializer
