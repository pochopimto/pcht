from django.test import TestCase, Client
from rest_framework.test import APIRequestFactory, APITestCase, APIClient
from rest_framework import status

from api.models import *
from django.contrib.auth.models import User

from pprint import pprint


class SchoolTestCase(APITestCase):
    def setUp(self):
        self.schools_data = [
            {
                "name": "Gymnázium Bobči Bobčíka na Bobenově",
                "address": "Bobenova 12, Bobenov, 123 45, Bobia",
            },
            {"name": "ZŠ Bobenkov", "address": "Bobanov",},
        ]
        self.schools = {}
        for i, school_data in enumerate(self.schools_data):
            school = School.objects.create(**school_data)
            self.schools_data[i]["id"] = school.id
            self.schools_data[i]["teachers"] = []
            self.schools[school.id] = self.schools_data[i]

    def test_schools_have_created(self):
        for id in self.schools:
            school = self.schools[id]
            obj = School.objects.get(id=school["id"])
            self.assertEqual(obj.name, school["name"])
            self.assertEqual(obj.address, school["address"])

    def test_schools_list(self):
        response = self.client.get("/api/schools")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for school in response.json():
            id = school["id"]
            for attr in school:
                self.assertEqual(school[attr], self.schools[id][attr])


class TeacherTestCase(APITestCase):
    def setUp(self):
        school = School.objects.create(name="School", address="Somewhere")

        user = User.objects.create(username="bob@bobinko.com", email="bob@bobinko.com")
        user.set_password("bob")
        user.save()
        teacher = Teacher.objects.get(user=user)
        teacher.name = "Bob Bobinko"
        teacher.school = school
        teacher.save()

    def test_database_has_created(self):
        self.assertEqual(Teacher.objects.count(), 1)
        t = Teacher.objects.get()

        self.assertEqual(t.school.name, "School")
        self.assertEqual(t.school.address, "Somewhere")

        self.assertEqual(t.user.username, "bob@bobinko.com")
        self.assertEqual(t.user.email, "bob@bobinko.com")

        self.assertEqual(t.name, "Bob Bobinko")
        self.assertEqual(t.principal, False)

    def test_teachers_create(self):
        teacher_data = {
            "name": "Peter Parker",
            "email": "peter@parker.com",
            "password": "spidey",
            "school": School.objects.all().first().id,
        }
        response = self.client.post("/api/teachers", teacher_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        teacher = Teacher.objects.get(id=response.json()["id"])
        self.assertEqual(teacher.user.email, teacher_data["email"])
        self.assertEqual(teacher.user.username, teacher_data["email"])
        self.assertEqual(teacher.name, teacher_data["name"])
        self.assertEqual(teacher.school.id, teacher_data["school"])

    #    def test_teacher_modify(self):
    #        new_teacher_data = {
    #            'name': 'Teacher Name',
    #            'email': 'spidey@animal.com',
    #            'password': 'none',
    #        }
    #
    #        response = self.client.put('/api/teachers/1', new_teacher_data, format='json')
    #        self.assertEqual(response.status_code, status.HTTP_200_OK)
    #
    #        teacher = Teacher.objects.get(id=1)
    #        self.assertEqual(teacher.name, new_teacher_data['name'])
    #        self.assertEqual(teacher.user.email, new_teacher_data['email'])
    #
    #        response = self.client.patch('/api/teachers/1', {'name': 'New Name'}, format='json')
    #        self.assertEqual(response.status_code, status.HTTP_200_OK)
    #
    #        teacher = Teacher.objects.get(id=1)
    #        self.assertEqual(teacher.name, 'New Name')

    #     def test_teacher_destroy(self):
    #         number_of_teachers = Teacher.objects.count()
    #         response = self.client.delete('/api/teachers/1')
    #
    #         self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
    #         self.assertEqual(Teacher.objects.count(), number_of_teachers-1)

    def test_login(self):
        user = User.objects.get()
        password = "bob"
        user.save()

        response = self.client.post(
            "/api/token",
            {"username": user.username, "password": password},
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class SurveyTestCase(APITestCase):
    def setUp(self):
        self.password = "bob"
        user = User.objects.create(username="bob@bob.com", email="bob@bob.com",)
        user.set_password(self.password)

        teacher = Teacher.objects.get(user=user)
        teacher.name = "Bob Bobinko"
        teacher.save()

        question_set = QuestionSet.objects.create()
        survey = Survey.objects.create(
            teacher=teacher, name="Awesomeness", question_set=question_set,
        )

        questions = [
            Question.objects.create(
                question="How awesome you are?",
                order=1,
                type=Question.SCALE,
                question_set=question_set,
            ),
            Question.objects.create(
                question="Would you like to add anything?",
                order=2,
                type=Question.TEXT,
                question_set=question_set,
            ),
            Question.objects.create(
                question="Were you satisfied?",
                order=3,
                type=Question.SELECT,
                question_set=question_set,
            ),
        ]
        options = [
            Option.objects.create(question=questions[2], text="yes", order=1),
            Option.objects.create(question=questions[2], text="no", order=2),
        ]

        self.user = user
        self.teacher = teacher
        self.question_set = question_set
        self.survey = survey
        self.questions = questions
        self.options = options

    #
    #     def test_get(self):
    #         response = self.client.get('/api/surveys')
    #
    #         self.assertEqual(response.status_code, status.HTTP_200_OK)
    #
    #         data = response.json()[0]
    #
    #         self.assertEqual(data['id'], self.survey.id)
    #         self.assertEqual(data['teacher'], self.teacher.id)
    #         self.assertEqual(data['name'], self.survey.name)
    #         self.assertEqual(
    #             tuple('options' in data['questions'][i] for i in range(len(data['questions']))),
    #             tuple(data['questions'][i]['type'] == Question.SELECT for i in range(len(data['questions'])))
    #         )
    #         for i, question in enumerate(self.questions):
    #             self.assertEqual(data['questions'][i]['id'], question.id)
    #             self.assertEqual(data['questions'][i]['id'], question.id)
    #             if question.type == Question.SELECT:
    #                 for j, option in enumerate(self.options):
    #                     #self.assertEqual(data['questions'][i]['options'][j]['order'], option.order)
    #                     self.assertEqual(data['questions'][i]['options'][j]['text'], option.text)
    #                     #self.assertEqual(data['questions'][i]['options'][j]['question'], option.question_id)
    #
    #     def test_delete(self):
    #         client = APIClient()
    #         client.credentials(HTTP_AUTHORIZATION='Token ' + Token.objects.get(user=self.user).key)
    #         response = client.delete('/api/my_surveys/%s' % self.survey.id)
    #         self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
    #
    #         self.assertEqual(User.objects.count(), 1)
    #         self.assertEqual(Teacher.objects.count(), 1)
    #         self.assertEqual(QuestionSet.objects.count(), 0)
    #         self.assertEqual(Question.objects.count(), 0)
    #         self.assertEqual(Option.objects.count(), 0)

    def test_creation(self):
        data = {
            "teacher": self.teacher.id,
            "name": "test_creation",
            "questions": [
                {
                    "question": "How are you?",
                    "type": "select",
                    "options": [{"text": "yes"}, {"text": "no"}],
                },
                {"question": "Anything to add?", "type": "text",},
                {"question": "How would you rate our app?", "type": "scale"},
            ],
        }
        response = self.client.post("/api/surveys", data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        returned = response.json()
        survey = Survey.objects.get(name="test_creation")
        self.assertEqual(survey.name, data["name"])
        self.assertEqual(survey.name, returned["name"])
        self.assertEqual(survey.teacher_id, data["teacher"])
        self.assertEqual(survey.teacher_id, returned["teacher"])

        self.assertEqual(
            Question.objects.filter(question_set=survey.question_set).count(),
            len(data["questions"]),
        )
        for i, question in enumerate(
            Question.objects.filter(question_set=survey.question_set)
        ):
            self.assertEqual(question.question, data["questions"][i]["question"])
            self.assertEqual(question.question, returned["questions"][i]["question"])
            self.assertEqual(question.type, data["questions"][i]["type"])
            self.assertEqual(question.type, returned["questions"][i]["type"])

            if question.type == Question.SELECT:
                self.assertEqual(
                    Option.objects.filter(question=question).count(),
                    len(data["questions"][i]["options"]),
                )
                for j, option in enumerate(Option.objects.filter(question=question)):
                    self.assertEqual(
                        option.text, data["questions"][i]["options"][j]["text"]
                    )
                    self.assertEqual(
                        option.text, returned["questions"][i]["options"][j]["text"]
                    )


class AnswersTestCase(APITestCase):
    def setUp(self):
        user = User.objects.create(username="bob@bob.com", email="bob@bob.com",)
        user.set_password("bob")

        teacher = Teacher.objects.get(user=user)
        teacher.name = "Bob Bobinko"
        teacher.save()

        question_set = QuestionSet.objects.create()
        survey = Survey.objects.create(
            teacher=teacher, name="Awesomeness", question_set=question_set,
        )

        questions = [
            Question.objects.create(
                question="How awesome you are?",
                order=1,
                type=Question.SCALE,
                question_set=question_set,
            ),
            Question.objects.create(
                question="Would you like to add anything?",
                order=2,
                type=Question.TEXT,
                question_set=question_set,
            ),
            Question.objects.create(
                question="Were you satisfied?",
                order=3,
                type=Question.SELECT,
                question_set=question_set,
            ),
        ]
        options = [
            Option.objects.create(question=questions[2], text="yes", order=1),
            Option.objects.create(question=questions[2], text="no", order=2),
        ]

        answer_sets = [
            AnswerSet.objects.create(survey=survey),
            AnswerSet.objects.create(survey=survey),
            AnswerSet.objects.create(survey=survey),
        ]

        Answer.objects.create(
            answer_set=answer_sets[0], question=questions[0], number=1,
        )
        Answer.objects.create(
            answer_set=answer_sets[0], question=questions[1], text="I'm fine.",
        )
        Answer.objects.create(
            answer_set=answer_sets[0], question=questions[2], option=options[0],
        )

        Answer.objects.create(
            answer_set=answer_sets[1], question=questions[0], number=10,
        )
        Answer.objects.create(
            answer_set=answer_sets[1], question=questions[1], text="No thanks.",
        )
        Answer.objects.create(
            answer_set=answer_sets[1], question=questions[2], option=options[0],
        )

        Answer.objects.create(
            answer_set=answer_sets[2], question=questions[0], number=5,
        )
        Answer.objects.create(
            answer_set=answer_sets[2],
            question=questions[1],
            text="Yeah, you are awesome.",
        )
        Answer.objects.create(
            answer_set=answer_sets[2], question=questions[2], option=options[1],
        )

        self.user = user
        self.teacher = teacher
        self.question_set = question_set
        self.survey = survey
        self.questions = questions
        self.options = options
        self.answer_sets = answer_sets

    def test_database_created(self):
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(Teacher.objects.count(), 1)
        self.assertEqual(QuestionSet.objects.count(), 1)
        self.assertEqual(Survey.objects.count(), 1)
        self.assertEqual(Question.objects.count(), 3)
        self.assertEqual(Option.objects.count(), 2)
        self.assertEqual(AnswerSet.objects.count(), 3)
        self.assertEqual(Answer.objects.count(), 9)

    def test_creation(self):
        data = {
            "survey": self.survey.id,
            "answers": [
                {"number": -1},
                {"text": "Yay!!"},
                {"option_id": self.options[1].id},
            ],
        }
        n_answers = Answer.objects.count()
        n_answer_sets = AnswerSet.objects.count()
        response = self.client.post("/api/answers", data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(AnswerSet.objects.count(), n_answer_sets + 1)
        self.assertEqual(Answer.objects.count(), n_answers + len(data["answers"]))

        returned_data = response.json()
        answer_set = AnswerSet.objects.get(id=returned_data["id"])
        self.assertEqual(answer_set.id, returned_data["id"])
        self.assertEqual(answer_set.survey.id, data["survey"])
        self.assertEqual(
            Answer.objects.filter(answer_set=answer_set).count(), len(data["answers"])
        )
        for i, answer in enumerate(Answer.objects.filter(answer_set=answer_set)):
            self.assertEqual(returned_data["answers"][i]["id"], answer.id)
            if "number" in data["answers"][i]:
                self.assertEqual(returned_data["answers"][i]["number"], answer.number)
                self.assertEqual(data["answers"][i]["number"], answer.number)
            else:
                self.assertTrue("number" not in returned_data["answers"][i])
                self.assertIsNone(answer.number)

            if "text" in data["answers"][i]:
                self.assertEqual(returned_data["answers"][i]["text"], answer.text)
                self.assertEqual(data["answers"][i]["text"], answer.text)
            else:
                self.assertTrue("text" not in returned_data["answers"][i])
                self.assertIsNone(answer.text)

            if "option_id" in data["answers"][i]:
                self.assertEqual(
                    returned_data["answers"][i]["option"]["id"], answer.option.id
                )
                self.assertEqual(data["answers"][i]["option_id"], answer.option.id)
                self.assertEqual(
                    returned_data["answers"][i]["option"]["text"], answer.option.text
                )
                self.assertEqual(len(returned_data["answers"][i]["option"].keys()), 2)
            else:
                self.assertTrue("option" not in returned_data["answers"][i])
                self.assertIsNone(answer.option)

    def test_creation_validation(self):
        datasets = [
            {
                "survey": self.survey.id,
                "answers": [{"text": "Yay!!"}, {"option_id": self.options[1].id}],
            },
            {
                "survey": self.survey.id,
                "answers": [
                    {"number": -1},
                    {"text": "Yay!!", "number": -1},
                    {"option_id": self.options[1].id},
                ],
            },
            {
                "survey": self.survey.id,
                "answers": [
                    {"number": -1},
                    {"text": "Yay!!"},
                    {"option": self.options[1].id},
                ],
            },
            {
                "survey": self.survey.id,
                "answers": [{"number": -1}, {"text": "Yay!!"}, {"option_id": -1}],
            },
        ]

        for data in datasets:
            n_answers = Answer.objects.count()
            n_answer_sets = AnswerSet.objects.count()
            response = self.client.post("/api/answers", data, format="json")
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(n_answers, Answer.objects.count())
            self.assertEqual(n_answer_sets, AnswerSet.objects.count())

    def test_delete(self):
        n_answer_sets = AnswerSet.objects.count()
        n_answers = Answer.objects.count()
        response = self.client.delete("/api/answers/%d" % self.answer_sets[0].id)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(AnswerSet.objects.count(), n_answer_sets)
        self.assertEqual(Answer.objects.count(), n_answers)

        client = APIClient()
        client.credentials(
            HTTP_AUTHORIZATION="Token " + Token.objects.get(user=self.user).key
        )
        response = client.delete("/api/delete_answer/%d" % self.answer_sets[0].answers.first().id)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(AnswerSet.objects.count(), n_answer_sets)
        self.assertEqual(Answer.objects.count(), n_answers - 1)

    def test_delete_fail(self):
        n_answer_sets = AnswerSet.objects.count()
        n_answers = Answer.objects.count()

        password = "pass"
        new_user = User.objects.create(
            username="new@user.com", email="new@user.com", password=password
        )

        client = APIClient()
        client.credentials(
            HTTP_AUTHORIZATION="Token " + Token.objects.get(user=new_user).key
        )
        response = client.delete("/api/delete_answer/%d" % self.answer_sets[0].answers.first().id)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(AnswerSet.objects.count(), n_answer_sets)
        self.assertEqual(Answer.objects.count(), n_answers)

