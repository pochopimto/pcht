from django.conf.urls import url, include
from django.views import generic
from rest_framework.documentation import include_docs_urls
from rest_framework.schemas import get_schema_view
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.authtoken import views

from . import views as v


urlpatterns = [
    url(r"^$", generic.RedirectView.as_view(url="docs/", permanent=False)),
    url(r"^docs/", include_docs_urls(title="pochopím.to API")),
    url(r"^token$", views.obtain_auth_token),
    url(
        r"^password_reset/",
        include("django_rest_passwordreset.urls", namespace="password_reset"),
    ),
    url(r"^schools$", v.SchoolList.as_view()),
    url(r"^schools/(?P<pk>[0-9]+)$", v.SchoolDetails.as_view()),
    url(r"^teachers$", v.TeacherList.as_view()),
    url(r"^teachers/(?P<pk>[0-9]+)$", v.TeacherDetails.as_view()),
    url(r"^current_teacher$", v.TeacherCurrentDetails.as_view()),
    url(r"^surveys$", v.SurveyList.as_view()),
    url(r"^my_surveys$", v.MySurveyWithAnswersList.as_view()),
    url(r"^my_surveys/(?P<pk>[a-z0-9]+)$", v.MySurveyWithAnswersDetails.as_view()),
    url(r"^surveys/(?P<pk>[a-z0-9]+)$", v.SurveyDetails.as_view()),
    url(r"^my_templates$", v.MyTemplateList.as_view()),
    url(r"^my_templates/(?P<pk>[a-z0-9]+)$", v.MyTemplateDetails.as_view()),
    url(r"^questions/(?P<pk>[0-9]+)$", v.QuestionDetails.as_view()),
    url(r"^answers$", v.AnswerSetList.as_view()),
    url(r"^answers/(?P<pk>[0-9]+)$", v.AnswerSetDetails.as_view()),
    url(r"^delete_answer/(?P<pk>[0-9]+)$", v.AnswerDestroy.as_view()),
    url(r"^traffic-lights$", v.TrafficLightsList.as_view()),
    url(r"^traffic-lights/(?P<pk>[a-z0-9]+)$", v.TrafficLightsDetails.as_view()),
    url(r"^traffic-lights-entries$", v.TrafficLightsEntriesList.as_view()),
    url(
        r"^traffic-lights-entries/(?P<pk>[0-9]+)$",
        v.TrafficLightsEntriesDetails.as_view(),
    ),
]
