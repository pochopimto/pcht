from rest_framework import permissions


class PchtBasePermission(permissions.BasePermission):
    def __add__(self, o):
        self2 = self

        class Output(PchtBasePermission):
            def has_permission(self, *args):
                return self2.has_permission(*args) and o.has_permission(*args)

            def has_object_permission(self, *args):
                return self2.has_object_permission(*args) and o.has_object_permission(
                    *args
                )

        return Output


class IsUser(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return bool(
            request.user and request.user.is_authenticated and request.user == obj.user
        )


class IsTeacher(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return bool(
            request.user
            and request.user.is_authenticated
            and request.user == obj.teacher.user
        )


class IsTeacherOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return bool(
            request.method in permissions.SAFE_METHODS
            or (
                request.user
                and request.user.is_authenticated
                and request.user == obj.teacher.user
            )
        )

