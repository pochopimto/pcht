"""
    Permit to import everything from api.serializers without knowing the details.
"""


import sys

from .question import QuestionSerializer
from .teacher import TeacherSerializer, SchoolSerializer
from .answer import AnswerSetSerializer, AnswerSerializer
from .survey import SurveySerializer, SurveyWithAnswersSerializer
from .template import TemplateSerializer
from .trafficLights import TrafficLightsSerializer, TrafficLightsEntrySerializer


__all__ = [
    "QuestionSerializer",
    "TeacherSerializer",
    "SchoolSerializer",
    "SurveySerializer",
    "SurveyWithAnswersSerializer",
    "TemplateSerializer",
    "AnswerSetSerializer",
    "AnswerSerializer",
    "TrafficLightsSerializer",
    "TrafficLightsEntrySerializer",
]
