from rest_framework import serializers, validators
from api.models import *
from . import *
from pprint import pprint


class TemplateSerializer(serializers.ModelSerializer):
    """
    Actions: List, Read, Delete, Validation
    """

    questions = survey.HelperQuestionSerializer(
        many=True, source="question_set.questions", required=False,
    )

    class Meta:
        model = Template
        fields = ("id", "teacher", "questions", "name", "date")
        read_only_fields = ("id", "questions", "date")

    def create(self, validated_data):
        try:
            questions = validated_data.pop("question_set").get("questions")
        except KeyError:
            questions = []

        question_set = QuestionSet.objects.create()
        template = Template.objects.create(question_set=question_set, **validated_data)

        for i, question in enumerate(questions):
            data = {
                "question_set": question_set.id,
                "order": i + 1,
                **question,
            }
            question_serializer = QuestionSerializer(data=data)
            question_serializer.is_valid(raise_exception=True)
            question_obj = question_serializer.save()

        return template

    def update(self, instance, validated_data):
        try:
            questions = validated_data.pop("question_set").get("questions")
        except KeyError:
            questions = []

        instance.name = validated_data.get("name", instance.name)
        instance.teacher = validated_data.get("teacher", instance.teacher.id)

        for question in Question.objects.filter(question_set=instance.question_set):
            question.delete()

        for i, question in enumerate(questions):
            data = {
                "question_set": instance.question_set.id,
                "order": i + 1,
                **question,
            }
            question_serializer = QuestionSerializer(data=data)
            question_serializer.is_valid(raise_exception=True)
            question_obj = question_serializer.save()

        instance.save()

        return instance

    def validate_questions(self, value):
        if not value:
            raise serializers.ValidationError(
                "Every template has to have at least one question."
            )
        return value

