from rest_framework import serializers, validators
from api.models import *
from . import *
from pprint import pprint


class HelperOptionSerializer(serializers.ModelSerializer):
    """
    Actions: List, Read, Delete, Validation
    """

    class Meta:
        model = Option
        fields = ("id", "text")
        read_only_fields = ("id",)


class HelperQuestionSerializer(serializers.ModelSerializer):
    """
    Actions: List, Read, Delete, Validation
    """

    options = HelperOptionSerializer(many=True, required=False, default=[])

    def validate(self, data):
        if data["type"] == Question.SELECT:
            if "options" not in data or len(data["options"]) < 2:
                raise serializers.ValidationError(
                    "This type of question has to have at least 2 options field."
                )
        else:
            if "options" in data and data["options"]:
                raise serializers.ValidationError(
                    "This type of question cannot have options."
                )

        return data

    class Meta:
        model = Question
        fields = ("id", "options", "question", "type")
        read_only_fields = ("id",)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if "options" in representation and representation["type"] != Question.SELECT:
            del representation["options"]
        return representation


class SurveySerializer(serializers.ModelSerializer):
    """
    Actions: List, Read, Delete, Validation
    """

    questions = HelperQuestionSerializer(
        many=True, source="question_set.questions", required=False,
    )

    class Meta:
        model = Survey
        fields = ("id", "teacher", "questions", "name", "date")
        read_only_fields = ("id", "questions", "date")

    def create(self, validated_data):
        try:
            questions = validated_data.pop("question_set").get("questions")
        except KeyError:
            questions = []

        question_set = QuestionSet.objects.create()
        survey = Survey.objects.create(question_set=question_set, **validated_data)

        for i, question in enumerate(questions):
            data = {
                "question_set": question_set.id,
                "order": i + 1,
                **question,
            }
            question_serializer = QuestionSerializer(data=data)
            question_serializer.is_valid(raise_exception=True)
            question_obj = question_serializer.save()

        return survey

    def validate_questions(self, value):
        if not value:
            raise serializers.ValidationError(
                "Every survey has to have at least one question."
            )
        return value


class SurveyWithAnswersSerializer(serializers.ModelSerializer):

    answers = AnswerSetSerializer(many=True, source="answer_sets", required=False)

    questions = HelperQuestionSerializer(
        many=True, source="question_set.questions", required=False,
    )

    class Meta:
        model = Survey
        fields = ("id", "teacher", "questions", "name", "date", "answers")
        read_only_fields = ("id", "questions", "date")
