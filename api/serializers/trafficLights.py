from rest_framework import serializers, validators
from api.models import Teacher, TrafficLights, TrafficLightsEntry
from pprint import pprint


class TrafficLightsEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = TrafficLightsEntry
        fields = ("id", "state", "trafficLights")
        read_only_fields = ("id",)


class TrafficLightsSerializer(serializers.ModelSerializer):

    entries = TrafficLightsEntrySerializer(many=True, required=False, read_only=True,)

    teacher = serializers.PrimaryKeyRelatedField(queryset=Teacher.objects.all())

    class Meta:
        model = TrafficLights
        fields = ("id", "teacher", "entries", "date")
        read_only_fields = ("id", "date")
        depth = 1

