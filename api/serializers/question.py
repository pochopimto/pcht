from rest_framework import serializers, validators
from api.models import *


class HelperOptionsSerializer(serializers.ModelSerializer):
    """
    Actions: Validation
    """

    class Meta:
        model = Option
        fields = ("id", "text")
        read_only_fields = ("id",)


class QuestionSerializer(serializers.ModelSerializer):
    """
    Actions: Create
    """

    options = HelperOptionsSerializer(many=True)
    question_set = serializers.PrimaryKeyRelatedField(
        queryset=QuestionSet.objects.all(),
    )

    def create(self, validated_data):
        try:
            options = validated_data.pop("options")
        except KeyError:
            options = []
        question = Question.objects.create(**validated_data)

        for i, option_data in enumerate(options):
            option = Option.objects.create(
                question=question, text=option_data["text"], order=i + 1,
            )

        return question

    class Meta:
        model = Question
        fields = ("id", "options", "question", "type", "question_set", "order")
        read_only_fields = ("id",)
        depth = 1

