from rest_framework import serializers, validators
from api.models import *


class AnswerSerializer(serializers.ModelSerializer):

    question_id = serializers.PrimaryKeyRelatedField(
        source="question", queryset=Question.objects.all()
    )
    option_id = serializers.PrimaryKeyRelatedField(
        source="option", queryset=Option.objects.all(), required=False
    )

    class Meta:
        model = Answer
        fields = ("id", "question_id", "answer_set", "text", "number", "option_id")
        read_only_fields = ("id",)

    def validate(self, data):
        possible_fields = ("text", "number", "option")
        needed = {
            Question.SELECT: ("option",),
            Question.TEXT: ("text",),
            Question.SCALE: ("number",),
        }[data["question"].type]

        for field in possible_fields:
            if field in data and field not in needed:
                raise serializers.ValidationError(
                    "Field {field_name} is not needed.".format(field_name=field)
                )
            if field not in data and field in needed:
                raise serializers.ValidationError(
                    "Field {field_name} is required.".format(field_name=field)
                )
        return data


class HelperOptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Option
        fields = ("id", "text")
        read_only_fields = ("id",)


class APIAnswerSerializer(serializers.ModelSerializer):

    option_id = serializers.PrimaryKeyRelatedField(
        queryset=Option.objects.all(), source="option", write_only=True, required=False
    )
    option = HelperOptionSerializer(read_only=True)

    class Meta:
        model = Answer
        fields = ("id", "text", "number", "option", "option_id")
        read_only_fields = ("id",)
        depth = 1

    def to_representation(self, instance):
        data = super().to_representation(instance)
        possible_fields = ("text", "number", "option")
        needed = {
            Question.SELECT: ("option",),
            Question.TEXT: ("text",),
            Question.SCALE: ("number",),
        }[instance.question.type]

        for field in possible_fields:
            if field in data and field not in needed:
                del data[field]
        return data


class AnswerSetSerializer(serializers.ModelSerializer):
    """
    Actions: Create, Read, Destroy
    """

    answers = APIAnswerSerializer(many=True)

    def create(self, validated_data):
        try:
            answers = validated_data.pop("answers")
        except KeyError:
            answers = []
        answer_set = AnswerSet.objects.create(**validated_data)

        for i, answer_data in enumerate(answers):
            if "option" in answer_data:
                answer_data["option_id"] = answer_data["option"].id
                del answer_data["option"]
            answer_serializer = AnswerSerializer(
                data={
                    "answer_set": answer_set.id,
                    "question_id": Question.objects.get(
                        question_set=answer_set.survey.question_set, order=i + 1
                    ).id,
                    **answer_data,
                }
            )
            if not answer_serializer.is_valid():
                answer_set.delete()
                answer_serializer.is_valid(raise_exception=True)
            answer = answer_serializer.save()

        return answer_set

    class Meta:
        model = AnswerSet
        fields = ("id", "date", "survey", "answers")
        read_only_fields = ("id", "date")
