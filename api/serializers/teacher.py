from rest_framework import serializers, validators
from api.models import *


class SchoolSerializer(serializers.ModelSerializer):
    teachers = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = School
        fields = ("id", "name", "address", "teachers")
        read_only_fields = ("id", "teachers")


class TeacherSerializer(serializers.ModelSerializer):
    email = serializers.CharField(required=True, source="user.email")

    password = serializers.CharField(write_only=True, source="user.password")

    school = serializers.PrimaryKeyRelatedField(
        queryset=School.objects.all(), default=None
    )

    def create(self, validated_data):
        user_data = validated_data["user"]

        user = User.objects.create_user(
            email=user_data["email"], username=user_data["email"],
        )
        user.set_password(user_data["password"])
        user.save()

        teacher = Teacher.objects.get(user=user)

        return self.update(teacher, validated_data)

    def update(self, instance, validated_data):
        try:
            user_data = validated_data.pop("user")

            if "password" in user_data:
                instance.user.set_password(user_data.pop("password"))
            instance.user.email = user_data.get("email", instance.user.email)
            instance.user.username = user_data.get("email", instance.user.email)
            instance.user.save()
        except KeyError:
            pass

        instance.name = validated_data.get("name", instance.name)
        instance.school = validated_data.get("school", instance.school)
        instance.principal = validated_data.get("principal", instance.principal)
        instance.agreed_to_terms = validated_data.get(
            "agreed_to_terms", instance.agreed_to_terms
        )
        instance.completed_intro = validated_data.get(
            "completed_intro", instance.completed_intro
        )
        instance.subscribed_to_newsletter = validated_data.get(
            "subscribed_to_newsletter", instance.subscribed_to_newsletter
        )
        instance.save()

        return instance

    def validate_email(self, value):
        if User.objects.filter(email=value):
            raise serializers.ValidationError("Email is not unique.")
        else:
            return value

    class Meta:
        model = Teacher
        fields = (
            "id",
            "email",
            "password",
            "name",
            "surveys",
            "school",
            "principal",
            "agreed_to_terms",
            "subscribed_to_newsletter",
            "completed_intro"
        )
        read_only_fields = ("id", "surveys")

