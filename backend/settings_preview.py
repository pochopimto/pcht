from .base_settings import *
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

sentry_sdk.init(
    dsn="https://231c84e0fd37426e8b97600b2ef0f11e@sentry.io/1823394",
    integrations=[DjangoIntegration()],
)

ALLOWED_HOSTS = ["preview.pochopimto.cz"]

# Database
DATABASES = {
    "default": {
        "ENGINE": 'django.db.backends.postgresql_psycopg2',
        "NAME": 'previewpcht',
        'USER': 'previewpchtuser',
        "PASSWORD": env("DATABASE_PASSWORD"),
        "HOST": "kaifis-1388.postgres.pythonanywhere-services.com",
        "PORT": 11388,
    }
}

# Email
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"

EMAIL_HOST = "debugmail.io"
EMAIL_PORT = 25
EMAIL_HOST_USER = "kaifer741@gmail.com"
EMAIL_HOST_PASSWORD = "ac3f5240-fcb2-11e9-8b3d-33a4610fee81"
EMAIL_USE_TSL = False
EMAIL_USE_SSL = True
EMAIL_TIMEOUT = 60 * 10
