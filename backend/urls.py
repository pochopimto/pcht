from django.contrib import admin
from django.urls import path
from django.views.generic.base import RedirectView
from django.conf.urls import include, url

urlpatterns = [
    url(r"^admin/", admin.site.urls),
    url(r"^api/", include("api.urls")),
    url(r"^", include("web.urls")),
]
