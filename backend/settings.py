from .base_settings import *

ALLOWED_HOSTS = ["*"]

# Database
DATABASES = {
    "default": {
        # "ENGINE": "django.db.backends.sqlite3",
        # "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
        "ENGINE": 'django.db.backends.postgresql_psycopg2',
        "NAME": 'pcht',
        'USER': 'pchtuser',
        "PASSWORD": 'password',
        'HOST': 'localhost',
        'PORT': 5432,

    }
}

# Email
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
